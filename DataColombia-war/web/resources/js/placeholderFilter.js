/* 
 * Función javascript que se encarga de cargar el placeholder
 * en los input creados por prime para los filtros 
 * Creado por Daniel Iregui 
 * Septiembre 16 de 2016
 */
$.fn.cargarPlaceholder = function(){
    var hijos = $('#form_principal\\:tabvAmbito\\:panelInfoReg_content');
    var inputs = hijos.find("input");
    $(inputs).each(function(){
        console.log($(this));
        $(this).attr("placeholder","Buscar...");
    });
};

    


