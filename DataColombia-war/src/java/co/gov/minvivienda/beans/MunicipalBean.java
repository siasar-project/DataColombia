/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.minvivienda.beans;

import co.gov.minvivienda.controllers.MenuBean;
import co.gov.minvivienda.controllers.UtilBean;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import siasar.persistence.dto.MunicipalReportDTO;
import siasar.persistence.dto.PaisDTO;
import siasar.persistence.entities.TbDivisaoAdmin;
import siasar.persistence.service.GestionAdmin;
import org.primefaces.event.ItemSelectEvent;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.PieChartModel;


/**
 *
 * @author danielo
 */
@ManagedBean
@ViewScoped
public class MunicipalBean implements Serializable {
    
    private GestionAdmin gestionAdmin;
    @ManagedProperty("#{menuBean}")
    private MenuBean menuBean;
    private List<PaisDTO> countriesList;
    private List<TbDivisaoAdmin> departamentosList;
    private List<TbDivisaoAdmin> municipiosList;
    private List<MunicipalReportDTO> municipalInfoList;
    private boolean booleanPaises;
    private boolean booleanConsulta;
    private boolean resultadosMunicipal;
    private boolean paginado;
    private String country;
    private BigInteger secondLevelSeq;
    private BigInteger thirdLevelSeq;
    private TbDivisaoAdmin divisionAdministrativa;
    private BarChartModel barModel;
    private PieChartModel pieModel1;
    private PieChartModel pieModel2;
    

    public MunicipalBean() {
        try {
            gestionAdmin = new GestionAdmin();
            countriesList = gestionAdmin.getCountries();
            country = "";
            booleanConsulta = false;
            resultadosMunicipal = false;
            paginado = false;
            if(countriesList.size() > 0)
                booleanPaises = true;
        } catch (Exception e) {
            UtilBean.getInstance().showError(e.getMessage());
        }
        
    }
    
   
    public void onCountryChange() {

        if (country != null && !country.equals("")) 
            departamentosList = gestionAdmin.getLevel(country,BigInteger.ZERO);
        else 
            departamentosList = new ArrayList<TbDivisaoAdmin>();
        resultadosMunicipal = false;
    }
    
    public void onSecondLevelChange() {

        if (secondLevelSeq != null && !secondLevelSeq.equals(BigInteger.ZERO)) 
            municipiosList = gestionAdmin.getLevel(country,secondLevelSeq);
        else 
            municipiosList = new ArrayList<TbDivisaoAdmin>();
        resultadosMunicipal = false;
    }
    
    public void onThirdLevelChange() {

        if(municipiosList.size() > 0){
            booleanConsulta = true;
        }
        resultadosMunicipal = false;
    }
    
    public void loadNavigation(String value) {
        menuBean.setMenu(value);
    }
    
    public void displayReport(){

        municipalInfoList = new ArrayList<>();
        municipalInfoList = gestionAdmin.
                getMunicipalInfo(secondLevelSeq.toString(),
                        thirdLevelSeq.toString());
        resultadosMunicipal = municipalInfoList.size() > 0;
        paginado = municipalInfoList.size() > 10;
        if(resultadosMunicipal){
            createPieModel1(municipalInfoList);
        }
        
    }
    
    private void createPieModel1(List<MunicipalReportDTO> municipalInfoList) {
        pieModel1 = new PieChartModel();    
        for(MunicipalReportDTO obj : municipalInfoList){
            pieModel1.set(obj.getNombreClasePrestador(), obj.getVereda());
        }
        pieModel1.setTitle("Número de veredas por clase de prestador");
        pieModel1.setLegendPosition("e");
        
    }
    
    public void itemSelect(ItemSelectEvent event) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Item selected",
                        "Item Index: " + event.getItemIndex() + ", Series Index:" + event.getSeriesIndex());
         
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    
    public void iniciarRevision(){
        System.out.println("Entró aca a revisión");
    }

    public List<PaisDTO> getCountriesList() {
        return countriesList;
    }

    public void setCountriesList(List<PaisDTO> countriesList) {
        this.countriesList = countriesList;
    }

    

    public boolean isBooleanPaises() {
        return booleanPaises;
    }

    public void setBooleanPaises(boolean booleanPaises) {
        this.booleanPaises = booleanPaises;
    }

    public GestionAdmin getGestionAdmin() {
        return gestionAdmin;
    }

    public void setGestionAdmin(GestionAdmin gestionAdmin) {
        this.gestionAdmin = gestionAdmin;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<TbDivisaoAdmin> getDepartamentosList() {
        return departamentosList;
    }

    public void setDepartamentosList(List<TbDivisaoAdmin> departamentosList) {
        this.departamentosList = departamentosList;
    }

    public TbDivisaoAdmin getDivisionAdministrativa() {
        return divisionAdministrativa;
    }

    public void setDivisionAdministrativa(TbDivisaoAdmin divisionAdministrativa) {
        this.divisionAdministrativa = divisionAdministrativa;
    }

    public BigInteger getSecondLevelSeq() {
        return secondLevelSeq;
    }

    public void setSecondLevelSeq(BigInteger secondLevelSeq) {
        this.secondLevelSeq = secondLevelSeq;
    }

    public BigInteger getThirdLevelSeq() {
        return thirdLevelSeq;
    }

    public void setThirdLevelSeq(BigInteger thirdLevelSeq) {
        this.thirdLevelSeq = thirdLevelSeq;
    }

    public List<TbDivisaoAdmin> getMunicipiosList() {
        return municipiosList;
    }

    public void setMunicipiosList(List<TbDivisaoAdmin> municipiosList) {
        this.municipiosList = municipiosList;
    }

    public boolean isBooleanConsulta() {
        return booleanConsulta;
    }

    public void setBooleanConsulta(boolean booleanConsulta) {
        this.booleanConsulta = booleanConsulta;
    }

    public MenuBean getMenuBean() {
        return menuBean;
    }

    public void setMenuBean(MenuBean menuBean) {
        this.menuBean = menuBean;
    }

    public List<MunicipalReportDTO> getMunicipalInfoList() {
        return municipalInfoList;
    }

    public void setMunicipalInfoList(List<MunicipalReportDTO> municipalInfoList) {
        this.municipalInfoList = municipalInfoList;
    }

    public boolean isResultadosMunicipal() {
        return resultadosMunicipal;
    }

    public void setResultadosMunicipal(boolean resultadosMunicipal) {
        this.resultadosMunicipal = resultadosMunicipal;
    }

    public BarChartModel getBarModel() {
        return barModel;
    }

    public void setBarModel(BarChartModel barModel) {
        this.barModel = barModel;
    }

    public PieChartModel getPieModel1() {
        return pieModel1;
    }

    public void setPieModel1(PieChartModel pieModel1) {
        this.pieModel1 = pieModel1;
    }

    public boolean isPaginado() {
        return paginado;
    }

    public void setPaginado(boolean paginado) {
        this.paginado = paginado;
    }

    public PieChartModel getPieModel2() {
        return pieModel2;
    }

    public void setPieModel2(PieChartModel pieModel2) {
        this.pieModel2 = pieModel2;
    }
}
