/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.minvivienda.controllers;

import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author Daniel Iregui Mayorga
 */
public class ApplicationProperties {
    
    public static ApplicationProperties properties = new ApplicationProperties();
    
    private Properties prop;

    private ApplicationProperties() {
        cargar();
    }
    
    public void cargar(){
    
         try {
             System.out.println("Linea " +  this.getClass().getClassLoader());
            InputStream in = this.getClass().getClassLoader().getResourceAsStream("co/gov/cargue/bundles/messages.properties");
            
            prop = new Properties();

            prop.load(in);

            
        } catch (Exception e) {
            
            e.printStackTrace();
            //TODO manejar la excepción
        }
    }
         
    public String getPropiedad(String propiedad){
    
        return prop.getProperty(propiedad);
    
    }
    
}
