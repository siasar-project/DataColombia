/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.minvivienda.controllers;

import java.util.MissingResourceException;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author opuentes
 */
public class UtilBean {

    private static UtilBean instance = new UtilBean();

    /**
     *
     * @return
     */
    public static UtilBean getInstance() {
        return instance;
    }

    private UtilBean() {
    }
    
    /**
     *
     * @param parametro
     * @return
     */
    public String getParametroURL(String parametro){
        try{
            return ((HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true)).getAttribute(parametro).toString();
        }catch(Exception ex){
            System.out.println("Error UtilBean->getParametroURL "+ex);
            return null;
        }
    }
    
    /**
     *
     * @return
     */
    public String getUsuarioSesion(){
        //return ((HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true)).getAttribute("login").toString();
        return (String)FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
    }
    
    /**
     *
     * @param usuario
     * @return
     */
    public boolean isAdmin(String usuario){
        return FacesContext.getCurrentInstance().getExternalContext().isUserInRole("ADMINISTRADOR");
    }
    

    /**
     *
     * @param usuario
     * @return
     */
    public boolean isAuditor(String usuario){
        return FacesContext.getCurrentInstance().getExternalContext().isUserInRole("AUDITOR");
    }
    
    
    /**
     *
     * @param usuario
     * @return
     */
    public boolean isEmpresa(String usuario){
        return FacesContext.getCurrentInstance().getExternalContext().isUserInRole("EMPRESA");
    }
    
    /**
     *
     * @param msg
     */
    public void showError(String msg) {
        try {
            FacesMessage fmsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, "");
        FacesContext.getCurrentInstance().addMessage(null, fmsg);
        } catch (Exception e) {
            System.out.println("Exception " + e.getMessage());
        }
        
    }

    /**
     *
     * @param msg
     */
    public void showInfo(String msg) {
        FacesMessage fmsg = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "");
        FacesContext.getCurrentInstance().addMessage(null, fmsg);
    }

    /**
     *
     * @param msg
     */
    public void showWarning(String msg) {
        FacesMessage fmsg = new FacesMessage(FacesMessage.SEVERITY_WARN, msg, "");
        FacesContext.getCurrentInstance().addMessage(null, fmsg);
    }
    
    /**
     *
     */
    public void removerMensaje(){
        if (FacesContext.getCurrentInstance().getMessages() != null) {
            FacesContext.getCurrentInstance().getMessages().remove();
        }
    }
    
    /**
     *
     * @param msgResume
     * @param msgDetail
     * @param idMsg
     * @param severity
     */
    public static void mensajeCustom(String msgResume, String msgDetail, String idMsg, int severity) {
        FacesMessage fmsg = null;
        switch (severity){
            case 1:
                fmsg = new FacesMessage(FacesMessage.SEVERITY_INFO, msgResume, msgDetail);
                break;
            case 2:
                fmsg = new FacesMessage(FacesMessage.SEVERITY_WARN, msgResume, msgDetail);
                break;
            case 3:
                fmsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msgResume, msgDetail);
                break;
            case 4:
                fmsg = new FacesMessage(FacesMessage.SEVERITY_FATAL, msgResume, msgDetail);
                break;
            default:
                fmsg = new FacesMessage(FacesMessage.SEVERITY_INFO, msgResume, msgDetail);
                break;
        }
        
        FacesContext.getCurrentInstance().addMessage(idMsg, fmsg);
    }
    
    /**
     *
     * @param rscBundleName
     * @param rscBundleKey
     * @return
     * @throws MissingResourceException
     */
    public static String getBundle(String rscBundleName, String rscBundleKey) throws MissingResourceException {
        String bundleDefault = "bundle";
        if(rscBundleName != null) 
            bundleDefault = rscBundleName;
        
        FacesContext fc = FacesContext.getCurrentInstance();
        ResourceBundle bundle = fc.getApplication().getResourceBundle(fc, bundleDefault);
        return bundle.getString(rscBundleKey);
    }
    

    
}
