/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.minvivienda.controllers;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author Daniel Iregui Mayorga
 */
@ManagedBean
@ViewScoped
public class MenuBean implements Serializable {
    
    private String formulario = "Inicio";
    private String menu = "";
    private boolean muestra;
    private String reporteSeleccionado = null ;
    private boolean boolVistoRecurso = false;
    private MenuModel model;
    private boolean facturaHabilitacion = false;
    private String estiloMenu ;
    private String estiloContenido ;
    private String estiloCentro ;
    private String nombreEmpresa ;

    public MenuBean() {
        
    }
    
    @PostConstruct
    public void init() 
    {
        try
        {
            if (!FacesContext.getCurrentInstance().isPostback()) 
            {
                if(UtilBean.getInstance().isEmpresa(UtilBean.getInstance().getUsuarioSesion()) && !boolVistoRecurso){
                    boolVistoRecurso = false;
                }else{
                    boolVistoRecurso = true;
                }
            }
            setEstiloContenido("div_contentGral");
            setEstiloMenu("estiloConfig");
            setEstiloCentro("div_centerconfig");
            setMenu("Inicio");
            
        }
        catch (Exception ex) 
        {
            UtilBean.getInstance().showError(ex.getMessage());
            Logger.getLogger(MenuBean.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
    /**
     *
     * @throws IOException
     */
    public void salir() throws IOException {
        try{
            String contextName = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
            FacesContext.getCurrentInstance().getExternalContext().redirect(contextName+"/sua/cerrarSesion.jsp");
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
    
        /**
     * @return the formulario
     */
    public String getFormulario() 
    {
        return formulario;
    }

    /**
     * @param formulario the formulario to set
     */
    public void setFormulario(String formulario) 
    {
        this.formulario = formulario;
        try {
            getInfoPantalla();
        } catch (Exception ex) {
            Logger.getLogger(MenuBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    /**
     * @author diregui
     * @return
     * @throws java.lang.Exception
     */
    public String getInfoPantalla() throws Exception
            
    {
        switch (formulario) {
            case "Inicio":
                return "  Inicio";
            case "Municipal":
                return "  Municipal";
            case "Ayudas":
                return "  Ayudas";
            case "Muestra":
                return "  Muestra";
            default:
                break;
        }
        return null;
    }

    /**
     *
     * @return
     */
    public String getMenu() {
        return menu;
    }

    /**
     * @author diregui
     * @param menu
     */
    public void setMenu(String menu) {
        
            this.menu ="";
        try 
        {
            setFormulario("");
            setEstiloContenido("div_contentGral");
            setEstiloMenu("estiloConfig");
            setEstiloCentro("div_centerconfig");

            switch (menu) {
                case "Municipal":
                    muestra = true;
                    setFormulario(menu);
                    break;
                case "Ayudas":
                    muestra = true;
                    setFormulario(menu);
                    break;
                case "Muestra":
                    muestra = true;
                    setFormulario(menu);
                    break;
                default:
                    break;
            }
            
            this.menu = menu;
        } catch (Exception ex) {
            UtilBean.getInstance().showError(ex.getMessage());
            Logger.getLogger(MenuBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    public String getReporteSeleccionado() {
        return reporteSeleccionado;
    }

    public void setReporteSeleccionado(String reporteSeleccionado) {
        this.reporteSeleccionado = reporteSeleccionado;
    }

    public boolean isBoolVistoRecurso() {
        return boolVistoRecurso;
    }

    public void setBoolVistoRecurso(boolean boolVistoRecurso) {
        this.boolVistoRecurso = boolVistoRecurso;
    }

    public String getEstiloMenu() {
        return estiloMenu;
    }

    public void setEstiloMenu(String estiloMenu) {
        this.estiloMenu = estiloMenu;
    }

    public String getEstiloContenido() {
        return estiloContenido;
    }

    public void setEstiloContenido(String estiloContenido) {
        this.estiloContenido = estiloContenido;
    }

    public String getEstiloCentro() {
        return estiloCentro;
    }

    public void setEstiloCentro(String estiloCentro) {
        this.estiloCentro = estiloCentro;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public boolean isMuestra() {
        return muestra;
    }

    public void setMuestra(boolean muestra) {
        this.muestra = muestra;
    }

    public MenuModel getModel() {
        return model;
    }

    public void setModel(MenuModel model) {
        this.model = model;
    }

    public boolean isFacturaHabilitacion() {
        return facturaHabilitacion;
    }

    public void setFacturaHabilitacion(boolean facturaHabilitacion) {
        this.facturaHabilitacion = facturaHabilitacion;
    }
    
    
    
}
