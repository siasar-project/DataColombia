/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


 function showMonthYear(calendar){

        $('.ui-datepicker-calendar').css('display','none');
        $('.ui-datepicker').css('top', $('.ui-datepicker').offset().top);

        var month = '';
        var year = '';

        if($(calendar).val()){
            month = $(calendar).val().split('/')[0];
            year = $(calendar).val().split('/')[1];
        } else {
            month = $.datepicker.formatDate("mm", new Date());
            year = $.datepicker.formatDate("yy", new Date());
        }

        var exists = 0 != $('.ui-datepicker-year option[value='+year+']').length;
        if(!exists){

            $(".ui-datepicker-year").empty();
            var initYear = parseInt(year)-10;
            var endYear = parseInt(year)+10;
            while(initYear < endYear){

                $(".ui-datepicker-year").append($('<option>', {value:initYear, text: initYear}));
                initYear++;
            }

        }

        $('.ui-datepicker-month').val(parseInt(month)-1);
        $('.ui-datepicker-year').val(year);
        $(".ui-datepicker-month").change( function(){changeMonthYear(calendar);});
        $(".ui-datepicker-year").change( function(){changeMonthYear(calendar);});

    }

    function changeMonthYear(calendar){

        console.log('changeMonthYear');
        $('.ui-datepicker-calendar').css('display','none');
        $(".ui-datepicker-month").change( function(){changeMonthYear(calendar);});
        $(".ui-datepicker-year").change( function(){changeMonthYear(calendar);});

        var month = parseInt($('.ui-datepicker-month').val())+1; 
        if(month < 10){ month = '0'+month; } 
        $(calendar).val(month+"/"+parseInt($('.ui-datepicker-year').val()));

    }