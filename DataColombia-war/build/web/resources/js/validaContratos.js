/* 
 * Funcion javaScript que se encarga de dejar el valor por defecto 
 * Cuando los contratos son 0
 * Creado por Daniel Iregui 
 * Octubre 28 de 2016
 */
$.fn.validaContratos = function(){
    var $valor = parseInt($('#form_principal\\:tabvAmbito\\:itNroCttos').val());
    console.log("El valor es el siguiente " + $valor);
    if($valor === 0){

        PF("somRolAcueductoV").panel.find(".ui-selectonemenu-item:eq(4)").click();
        PF("somRolAlcantarilladoA").panel.find(".ui-selectonemenu-item:eq(2)").click();
        
    }   
};