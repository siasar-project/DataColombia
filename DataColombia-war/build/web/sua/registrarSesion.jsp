<?xml version="1.0"?>
<%@page import="java.util.Enumeration"%>
<!DOCTYPE session [
    <!ELEMENT encode   (#PCDATA)>
    <!ELEMENT id   (#PCDATA)>
    <!ELEMENT idName   (#PCDATA)>
    <!ELEMENT last   (#PCDATA)>
    <!ELEMENT session   (encode?,id,idName?,timeOut,last)>
    <!ELEMENT timeOut   (#PCDATA)>
    ]>

    <%
        Enumeration<String> parametros = request.getParameterNames();        
        String parametro;
        
        while (parametros.hasMoreElements())
        {
          parametro = parametros.nextElement();    
          request.getSession().setAttribute(parametro, request.getParameter(parametro));
          System.out.println(parametro+"="+request.getParameter(parametro));
        }    
        
        String ipServidor = request.getRemoteAddr();

        System.out.println(ipServidor);
        System.out.println("Creando sesion: " + session.getId());
        System.out.println("login: " + session.getAttribute("login"));

    %>

<session>
    <encode>
        <%=response.encodeURL("http://www.sui.gov.co/CargueMasivoAdmin-war/")%>
    </encode>
    <id>
        <%=request.getSession().getId()%>
    </id>
    <idName>jsessionid</idName>
    <timeOut>
        <%=request.getSession().getMaxInactiveInterval()%>
    </timeOut>
    <last>
        <%=request.getSession().getLastAccessedTime()%>
    </last>
</session>

<% 
    response.sendRedirect(request.getContextPath() + "/"); 
    //response.sendRedirect("http://www.sui.gov.co/CargueMasivoAdmin-war/"); //PRODUCCION
    //response.sendRedirect("localhost:8080/CargueMasivoAdmin-war"); //PRUEBAS PRUEBAS
%>