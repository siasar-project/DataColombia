<?xml version="1.0"?>
<%@page import="java.util.Enumeration"%>
<!DOCTYPE session [
    <!ELEMENT encode   (#PCDATA)>
    <!ELEMENT id   (#PCDATA)>
    <!ELEMENT idName   (#PCDATA)>
    <!ELEMENT last   (#PCDATA)>
    <!ELEMENT session   (encode?,id,idName?,timeOut,last)>
    <!ELEMENT timeOut   (#PCDATA)> 
<%
request.getSession().invalidate();
%>

<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="es-es" />
        <title>SIASAR</title>
        <!--script type="text/javascript" src="<-%=request.getContextPath()%>/misc/js/jquery-1.8.0.min.js"></script-->
        <!--link href="<-%=request.getContextPath()%>/resources/css/default.css" rel="stylesheet" type="text/css" /-->
        <!--link href="<-%=request.getContextPath()%>/misc/css/sspd.css" rel="stylesheet" type="text/css" /-->
    </head>
    <body>
        <div id="logo">
            <!--h:panelGrid columns="3" cellpadding="10" border="1">
                <p:graphicImage url="<-%=request.getContextPath()%>/misc/images/Logo_SSPD_logo_banner_sspd_logo.png"/-->
                <h:outputText value="TARIFARIO" style="width: 70%; alignment-adjust: central"/>
                <!--p:graphicImage url="<-%=request.getContextPath()%>/misc/images/SUI.png" />
            </h:panelGrid-->
        </div>
        
        <div id="conte_index">
            <div id="col_der_index">
                <div id="bienvenido">Sesi&oacute;n terminada</div>
                <div id="bienvenido_txt">Su sesi&oacute;n ha sido cerrada. Puede cerrar esta ventana
                                    para terminar.
                </div>
            </div>    
        </div>
    </body>
</html>
