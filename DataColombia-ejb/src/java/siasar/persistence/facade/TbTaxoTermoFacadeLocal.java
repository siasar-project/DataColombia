/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbTaxoTermo;

/**
 *
 * @author danielo
 */
@Local
public interface TbTaxoTermoFacadeLocal {

    void create(TbTaxoTermo tbTaxoTermo);

    void edit(TbTaxoTermo tbTaxoTermo);

    void remove(TbTaxoTermo tbTaxoTermo);

    TbTaxoTermo find(Object id);

    List<TbTaxoTermo> findAll();

    List<TbTaxoTermo> findRange(int[] range);

    int count();
    
}
