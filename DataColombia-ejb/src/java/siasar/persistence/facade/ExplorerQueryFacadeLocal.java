/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.ExplorerQuery;

/**
 *
 * @author danielo
 */
@Local
public interface ExplorerQueryFacadeLocal {

    void create(ExplorerQuery explorerQuery);

    void edit(ExplorerQuery explorerQuery);

    void remove(ExplorerQuery explorerQuery);

    ExplorerQuery find(Object id);

    List<ExplorerQuery> findAll();

    List<ExplorerQuery> findRange(int[] range);

    int count();
    
}
