/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbReabilAmpliacao;

/**
 *
 * @author danielo
 */
@Local
public interface TbReabilAmpliacaoFacadeLocal {

    void create(TbReabilAmpliacao tbReabilAmpliacao);

    void edit(TbReabilAmpliacao tbReabilAmpliacao);

    void remove(TbReabilAmpliacao tbReabilAmpliacao);

    TbReabilAmpliacao find(Object id);

    List<TbReabilAmpliacao> findAll();

    List<TbReabilAmpliacao> findRange(int[] range);

    int count();
    
}
