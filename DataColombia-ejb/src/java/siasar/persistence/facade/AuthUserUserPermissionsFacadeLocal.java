/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.AuthUserUserPermissions;

/**
 *
 * @author danielo
 */
@Local
public interface AuthUserUserPermissionsFacadeLocal {

    void create(AuthUserUserPermissions authUserUserPermissions);

    void edit(AuthUserUserPermissions authUserUserPermissions);

    void remove(AuthUserUserPermissions authUserUserPermissions);

    AuthUserUserPermissions find(Object id);

    List<AuthUserUserPermissions> findAll();

    List<AuthUserUserPermissions> findRange(int[] range);

    int count();
    
}
