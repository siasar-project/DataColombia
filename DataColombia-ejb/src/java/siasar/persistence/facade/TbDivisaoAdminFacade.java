/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import siasar.persistence.dto.PaisDTO;
import siasar.persistence.entities.TbDivisaoAdmin;

/**
 *
 * @author danielo
 */
@Stateless
public class TbDivisaoAdminFacade extends AbstractFacade<TbDivisaoAdmin> implements TbDivisaoAdminFacadeLocal {

    @PersistenceContext(unitName = "DataColombia-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TbDivisaoAdminFacade() {
        super(TbDivisaoAdmin.class);
    }

    @Override
    public List<PaisDTO> getPaises() {
        List<PaisDTO> paises;
        paises = new ArrayList<>();
        String query = "SELECT pais_nome, pais_abreviatura, pais_sigla FROM tb_pais where pais_sigla = 'CO'";
        System.out.println("query paises:::  " + query);
        try {
            Query queryCreado = em.createNativeQuery(query);

            List rst = (List) queryCreado.getResultList();
            if (rst.size() > 0) {
                for (Object obj : rst) {
                    Object[] oCargue = (Object[]) obj;
                    PaisDTO pais = new PaisDTO();
                    pais.setNombre(oCargue[0].toString());
                    pais.setAbreviatura(oCargue[1].toString());
                    pais.setSigla(oCargue[2].toString());
                    paises.add(pais);
                }

            }
        } catch (Exception e) {
            throw e;
        }
        return paises;
    }

    @Override
    public List<TbDivisaoAdmin> findLevel(String country, BigInteger pai) {
        
        String queryString;
        if(pai.equals(BigInteger.ZERO)){
            queryString = "select tbDivisaoAdmin from TbDivisaoAdmin as "
                    + "TbDivisaoAdmin where tbDivisaoAdmin.tdaPais =:pais and "
                    + "tbDivisaoAdmin.tdaPai is null";
        }else{
            queryString = "select tbDivisaoAdmin from TbDivisaoAdmin as "
                    + "TbDivisaoAdmin where tbDivisaoAdmin.tdaPais =:pais and "
                    + "tbDivisaoAdmin.tdaPai =:pai";
        }
        Query query = em.createQuery(queryString);
        query.setParameter("pais", country);
        if(!pai.equals(BigInteger.ZERO))
            query.setParameter("pai", pai);
         List<TbDivisaoAdmin> divisaoAdminList = query.getResultList();
        
        return divisaoAdminList;
    }
    
    
}
