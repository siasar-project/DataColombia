/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbUnit;

/**
 *
 * @author danielo
 */
@Local
public interface TbUnitFacadeLocal {

    void create(TbUnit tbUnit);

    void edit(TbUnit tbUnit);

    void remove(TbUnit tbUnit);

    TbUnit find(Object id);

    List<TbUnit> findAll();

    List<TbUnit> findRange(int[] range);

    int count();
    
}
