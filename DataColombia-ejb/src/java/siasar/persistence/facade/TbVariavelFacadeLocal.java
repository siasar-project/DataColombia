/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbVariavel;

/**
 *
 * @author danielo
 */
@Local
public interface TbVariavelFacadeLocal {

    void create(TbVariavel tbVariavel);

    void edit(TbVariavel tbVariavel);

    void remove(TbVariavel tbVariavel);

    TbVariavel find(Object id);

    List<TbVariavel> findAll();

    List<TbVariavel> findRange(int[] range);

    int count();
    
}
