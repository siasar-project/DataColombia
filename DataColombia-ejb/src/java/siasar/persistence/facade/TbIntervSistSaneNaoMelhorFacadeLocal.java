/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbIntervSistSaneNaoMelhor;

/**
 *
 * @author danielo
 */
@Local
public interface TbIntervSistSaneNaoMelhorFacadeLocal {

    void create(TbIntervSistSaneNaoMelhor tbIntervSistSaneNaoMelhor);

    void edit(TbIntervSistSaneNaoMelhor tbIntervSistSaneNaoMelhor);

    void remove(TbIntervSistSaneNaoMelhor tbIntervSistSaneNaoMelhor);

    TbIntervSistSaneNaoMelhor find(Object id);

    List<TbIntervSistSaneNaoMelhor> findAll();

    List<TbIntervSistSaneNaoMelhor> findRange(int[] range);

    int count();
    
}
