/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbTaxoVocab;

/**
 *
 * @author danielo
 */
@Local
public interface TbTaxoVocabFacadeLocal {

    void create(TbTaxoVocab tbTaxoVocab);

    void edit(TbTaxoVocab tbTaxoVocab);

    void remove(TbTaxoVocab tbTaxoVocab);

    TbTaxoVocab find(Object id);

    List<TbTaxoVocab> findAll();

    List<TbTaxoVocab> findRange(int[] range);

    int count();
    
}
