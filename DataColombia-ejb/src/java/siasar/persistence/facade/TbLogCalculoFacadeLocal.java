/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbLogCalculo;

/**
 *
 * @author danielo
 */
@Local
public interface TbLogCalculoFacadeLocal {

    void create(TbLogCalculo tbLogCalculo);

    void edit(TbLogCalculo tbLogCalculo);

    void remove(TbLogCalculo tbLogCalculo);

    TbLogCalculo find(Object id);

    List<TbLogCalculo> findAll();

    List<TbLogCalculo> findRange(int[] range);

    int count();
    
}
