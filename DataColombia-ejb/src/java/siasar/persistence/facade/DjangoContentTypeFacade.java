/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import siasar.persistence.entities.DjangoContentType;

/**
 *
 * @author danielo
 */
@Stateless
public class DjangoContentTypeFacade extends AbstractFacade<DjangoContentType> implements DjangoContentTypeFacadeLocal {

    @PersistenceContext(unitName = "DataColombia-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DjangoContentTypeFacade() {
        super(DjangoContentType.class);
    }
    
}
