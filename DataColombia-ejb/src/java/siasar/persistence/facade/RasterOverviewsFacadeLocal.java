/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.RasterOverviews;

/**
 *
 * @author danielo
 */
@Local
public interface RasterOverviewsFacadeLocal {

    void create(RasterOverviews rasterOverviews);

    void edit(RasterOverviews rasterOverviews);

    void remove(RasterOverviews rasterOverviews);

    RasterOverviews find(Object id);

    List<RasterOverviews> findAll();

    List<RasterOverviews> findRange(int[] range);

    int count();
    
}
