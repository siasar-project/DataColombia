/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.AuthGroupPermissions;

/**
 *
 * @author danielo
 */
@Local
public interface AuthGroupPermissionsFacadeLocal {

    void create(AuthGroupPermissions authGroupPermissions);

    void edit(AuthGroupPermissions authGroupPermissions);

    void remove(AuthGroupPermissions authGroupPermissions);

    AuthGroupPermissions find(Object id);

    List<AuthGroupPermissions> findAll();

    List<AuthGroupPermissions> findRange(int[] range);

    int count();
    
}
