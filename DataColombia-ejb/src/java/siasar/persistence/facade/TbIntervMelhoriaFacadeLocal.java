/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbIntervMelhoria;

/**
 *
 * @author danielo
 */
@Local
public interface TbIntervMelhoriaFacadeLocal {

    void create(TbIntervMelhoria tbIntervMelhoria);

    void edit(TbIntervMelhoria tbIntervMelhoria);

    void remove(TbIntervMelhoria tbIntervMelhoria);

    TbIntervMelhoria find(Object id);

    List<TbIntervMelhoria> findAll();

    List<TbIntervMelhoria> findRange(int[] range);

    int count();
    
}
