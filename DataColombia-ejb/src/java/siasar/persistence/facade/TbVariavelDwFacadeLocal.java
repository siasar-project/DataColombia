/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbVariavelDw;

/**
 *
 * @author danielo
 */
@Local
public interface TbVariavelDwFacadeLocal {

    void create(TbVariavelDw tbVariavelDw);

    void edit(TbVariavelDw tbVariavelDw);

    void remove(TbVariavelDw tbVariavelDw);

    TbVariavelDw find(Object id);

    List<TbVariavelDw> findAll();

    List<TbVariavelDw> findRange(int[] range);

    int count();
    
}
