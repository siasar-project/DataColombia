/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import siasar.persistence.dto.MunicipalReportDTO;
import siasar.persistence.entities.TbComunidade;

/**
 *
 * @author danielo
 */
@Stateless
public class TbComunidadeFacade extends AbstractFacade<TbComunidade> implements TbComunidadeFacadeLocal {

    @PersistenceContext(unitName = "DataColombia-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TbComunidadeFacade() {
        super(TbComunidade.class);
    }

    @Override
    public List<MunicipalReportDTO> findMunicipalInfo(String idDepartamento, String idMunicipio) {

        List<MunicipalReportDTO> municipalInfo;
        municipalInfo = new ArrayList<>();

        String query = "select div2.tda_nome as departamento,div1.tda_nome as municipio,count(c.comun_seq) as numero_veredas,"
                + "case "
                + "	when ps.sep_a_009_001 = 32013 then 'Asociación / Organización comunitaria'"
                + "	when ps.sep_a_009_001 = 32014 then 'Gestión directa por parte de institución pública'"
                + "	when ps.sep_a_009_001 = 32015 then 'Otra'"
                + "	else 'No tiene asignado'"
                + "	end "
                + "as nombre_clase_prestador,"
                + "to_char(CAST ( COUNT(CASE WHEN ps.sep_a_009_001 IS NULL THEN 1 ELSE 1 END) AS float)/CAST ((select count(cw.comun_seq) "
                + "from tb_comunidade cw "
                + "left join tb_grp_domicilio gd on cw.comun_seq = gd.comun_seq "
                + "left join tb_pais pais on pais.pais_sigla = cw.pais_sigla "
                + "left JOIN tb_divisao_admin div on div.tda_seq = cw.com_a_004_001 "
                + "left JOIN tb_divisao_admin div1 ON div.tda_pai = div1.tda_seq "
                + "left JOIN tb_divisao_admin div2 ON div1.tda_pai = div2.tda_seq "
                + "left JOIN tb_divisao_admin div3 ON div2.tda_pai = div3.tda_seq "
                + "left join tb_prest_servico ps on gd.prser_seq = ps.prser_seq "
                + "left join tb_sistema s on gd.siste_seq = s.siste_seq  "
                + "where cw.pais_sigla = 'CO' "
                + "AND div2.tda_seq = " + idDepartamento + " AND div1.tda_seq = " + idMunicipio
                + " GROUP BY div1.tda_seq,div2.tda_seq"
                + ")  AS float) *100, '999D99%') as clase_prestador,"
                + "sum(c.com_b_005_001+com_b_006_001+com_b_007_001) as excretas,"
                + "count(s.sys_g_006_001) as cantidad_cloro_residual,"
                + "sum(c.com_b_019_001) as agua_jabon,"
                + "count(nullif(c.com_b_021_001 = true, true)) as basuras "
                + "from tb_comunidade c "
                + "left join tb_grp_domicilio gd on c.comun_seq = gd.comun_seq "
                + "left join tb_pais pais on pais.pais_sigla = c.pais_sigla "
                + "left JOIN tb_divisao_admin div on div.tda_seq = c.com_a_004_001 "
                + "left JOIN tb_divisao_admin div1 ON div.tda_pai = div1.tda_seq "
                + "left JOIN tb_divisao_admin div2 ON div1.tda_pai = div2.tda_seq "
                + "left JOIN tb_divisao_admin div3 ON div2.tda_pai = div3.tda_seq "
                + "left join tb_prest_servico ps on gd.prser_seq = ps.prser_seq "
                + "left join tb_sistema s on gd.siste_seq = s.siste_seq  "
                + "where c.pais_sigla = 'CO' "
                + "AND div2.tda_seq = " + idDepartamento + " AND div1.tda_seq = " + idMunicipio
                + " GROUP BY "
                + "div1.tda_seq,div2.tda_seq,ps.sep_a_009_001";
                System.out.println("esta es la query del reporte municipal*** " + query);
                Query queryCreado = em.createNativeQuery(query);
                List rst = (List) queryCreado.getResultList();
                try {

                    if (rst.size() > 0) {
                        for (Object obj : rst) {
                            Object[] oMunicipal = (Object[]) obj;
                            MunicipalReportDTO municipalObj = new MunicipalReportDTO();
                            municipalObj.setDepartamento(oMunicipal[0].toString());
                            municipalObj.setMunicipio(oMunicipal[1].toString());
                            municipalObj.setVereda((BigInteger) oMunicipal[2]);
                            //municipalObj.setNroSistemas((BigInteger) oMunicipal[3]);
                            municipalObj.setNombreClasePrestador(oMunicipal[3].toString());
                            municipalObj.setNroClasePrestador(oMunicipal[4].toString());
                            //municipalObj.setAgua((BigInteger) oMunicipal[6]);
                            municipalObj.setExcretas((BigInteger) oMunicipal[5]);
                            municipalObj.setHigiene((BigInteger) oMunicipal[6]);
                            municipalObj.setAguaJabon((BigInteger) oMunicipal[7]);
                            municipalObj.setResiduosSolidos((BigInteger) oMunicipal[8]);
                            
                            municipalInfo.add(municipalObj);
                        }

                    }
                } catch (NumberFormatException e) {
                    System.out.println("Exception date" + e.getMessage());
                }
        return municipalInfo;
    }

}
