/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.math.BigInteger;
import java.util.List;
import javax.ejb.Local;
import siasar.persistence.dto.PaisDTO;
import siasar.persistence.entities.TbDivisaoAdmin;

/**
 *
 * @author danielo
 */
@Local
public interface TbDivisaoAdminFacadeLocal {

    void create(TbDivisaoAdmin tbDivisaoAdmin);

    void edit(TbDivisaoAdmin tbDivisaoAdmin);

    void remove(TbDivisaoAdmin tbDivisaoAdmin);

    TbDivisaoAdmin find(Object id);

    List<TbDivisaoAdmin> findAll();

    List<TbDivisaoAdmin> findRange(int[] range);

    int count();
    
    List<PaisDTO> getPaises();
    
    List<TbDivisaoAdmin> findLevel(String country, BigInteger pai);
    
}
