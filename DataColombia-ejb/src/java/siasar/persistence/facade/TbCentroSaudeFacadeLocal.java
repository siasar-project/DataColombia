/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbCentroSaude;

/**
 *
 * @author danielo
 */
@Local
public interface TbCentroSaudeFacadeLocal {

    void create(TbCentroSaude tbCentroSaude);

    void edit(TbCentroSaude tbCentroSaude);

    void remove(TbCentroSaude tbCentroSaude);

    TbCentroSaude find(Object id);

    List<TbCentroSaude> findAll();

    List<TbCentroSaude> findRange(int[] range);

    int count();
    
}
