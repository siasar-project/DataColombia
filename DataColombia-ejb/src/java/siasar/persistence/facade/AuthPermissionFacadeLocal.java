/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.AuthPermission;

/**
 *
 * @author danielo
 */
@Local
public interface AuthPermissionFacadeLocal {

    void create(AuthPermission authPermission);

    void edit(AuthPermission authPermission);

    void remove(AuthPermission authPermission);

    AuthPermission find(Object id);

    List<AuthPermission> findAll();

    List<AuthPermission> findRange(int[] range);

    int count();
    
}
