/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbGrpDomicilio;

/**
 *
 * @author danielo
 */
@Local
public interface TbGrpDomicilioFacadeLocal {

    void create(TbGrpDomicilio tbGrpDomicilio);

    void edit(TbGrpDomicilio tbGrpDomicilio);

    void remove(TbGrpDomicilio tbGrpDomicilio);

    TbGrpDomicilio find(Object id);

    List<TbGrpDomicilio> findAll();

    List<TbGrpDomicilio> findRange(int[] range);

    int count();
    
}
