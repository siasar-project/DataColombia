/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.AuthUser;

/**
 *
 * @author danielo
 */
@Local
public interface AuthUserFacadeLocal {

    void create(AuthUser authUser);

    void edit(AuthUser authUser);

    void remove(AuthUser authUser);

    AuthUser find(Object id);

    List<AuthUser> findAll();

    List<AuthUser> findRange(int[] range);

    int count();
    
}
