/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.GeometryColumns;

/**
 *
 * @author danielo
 */
@Local
public interface GeometryColumnsFacadeLocal {

    void create(GeometryColumns geometryColumns);

    void edit(GeometryColumns geometryColumns);

    void remove(GeometryColumns geometryColumns);

    GeometryColumns find(Object id);

    List<GeometryColumns> findAll();

    List<GeometryColumns> findRange(int[] range);

    int count();
    
}
