/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import siasar.persistence.entities.TbLogCalculo;

/**
 *
 * @author danielo
 */
@Stateless
public class TbLogCalculoFacade extends AbstractFacade<TbLogCalculo> implements TbLogCalculoFacadeLocal {

    @PersistenceContext(unitName = "DataColombia-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TbLogCalculoFacade() {
        super(TbLogCalculo.class);
    }
    
}
