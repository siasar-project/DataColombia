/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.GeographyColumns;

/**
 *
 * @author danielo
 */
@Local
public interface GeographyColumnsFacadeLocal {

    void create(GeographyColumns geographyColumns);

    void edit(GeographyColumns geographyColumns);

    void remove(GeographyColumns geographyColumns);

    GeographyColumns find(Object id);

    List<GeographyColumns> findAll();

    List<GeographyColumns> findRange(int[] range);

    int count();
    
}
