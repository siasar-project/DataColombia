/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import siasar.persistence.entities.TbCentroEducativo;

/**
 *
 * @author danielo
 */
@Stateless
public class TbCentroEducativoFacade extends AbstractFacade<TbCentroEducativo> implements TbCentroEducativoFacadeLocal {

    @PersistenceContext(unitName = "DataColombia-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TbCentroEducativoFacade() {
        super(TbCentroEducativo.class);
    }
    
}
