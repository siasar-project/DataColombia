/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import siasar.persistence.entities.TbGrpDomicilio;

/**
 *
 * @author danielo
 */
@Stateless
public class TbGrpDomicilioFacade extends AbstractFacade<TbGrpDomicilio> implements TbGrpDomicilioFacadeLocal {

    @PersistenceContext(unitName = "DataColombia-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TbGrpDomicilioFacade() {
        super(TbGrpDomicilio.class);
    }
    
}
