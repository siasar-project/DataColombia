/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbTratamento;

/**
 *
 * @author danielo
 */
@Local
public interface TbTratamentoFacadeLocal {

    void create(TbTratamento tbTratamento);

    void edit(TbTratamento tbTratamento);

    void remove(TbTratamento tbTratamento);

    TbTratamento find(Object id);

    List<TbTratamento> findAll();

    List<TbTratamento> findRange(int[] range);

    int count();
    
}
