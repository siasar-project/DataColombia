/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbPat;

/**
 *
 * @author danielo
 */
@Local
public interface TbPatFacadeLocal {

    void create(TbPat tbPat);

    void edit(TbPat tbPat);

    void remove(TbPat tbPat);

    TbPat find(Object id);

    List<TbPat> findAll();

    List<TbPat> findRange(int[] range);

    int count();
    
}
