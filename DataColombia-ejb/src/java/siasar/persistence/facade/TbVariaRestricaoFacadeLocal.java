/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbVariaRestricao;

/**
 *
 * @author danielo
 */
@Local
public interface TbVariaRestricaoFacadeLocal {

    void create(TbVariaRestricao tbVariaRestricao);

    void edit(TbVariaRestricao tbVariaRestricao);

    void remove(TbVariaRestricao tbVariaRestricao);

    TbVariaRestricao find(Object id);

    List<TbVariaRestricao> findAll();

    List<TbVariaRestricao> findRange(int[] range);

    int count();
    
}
