/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.RasterColumns;

/**
 *
 * @author danielo
 */
@Local
public interface RasterColumnsFacadeLocal {

    void create(RasterColumns rasterColumns);

    void edit(RasterColumns rasterColumns);

    void remove(RasterColumns rasterColumns);

    RasterColumns find(Object id);

    List<RasterColumns> findAll();

    List<RasterColumns> findRange(int[] range);

    int count();
    
}
