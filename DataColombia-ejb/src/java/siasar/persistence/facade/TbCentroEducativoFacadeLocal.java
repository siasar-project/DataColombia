/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbCentroEducativo;

/**
 *
 * @author danielo
 */
@Local
public interface TbCentroEducativoFacadeLocal {

    void create(TbCentroEducativo tbCentroEducativo);

    void edit(TbCentroEducativo tbCentroEducativo);

    void remove(TbCentroEducativo tbCentroEducativo);

    TbCentroEducativo find(Object id);

    List<TbCentroEducativo> findAll();

    List<TbCentroEducativo> findRange(int[] range);

    int count();
    
}
