/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.dto.MunicipalReportDTO;
import siasar.persistence.entities.TbComunidade;

/**
 *
 * @author danielo
 */
@Local
public interface TbComunidadeFacadeLocal {

    void create(TbComunidade tbComunidade);

    void edit(TbComunidade tbComunidade);

    void remove(TbComunidade tbComunidade);

    TbComunidade find(Object id);

    List<TbComunidade> findAll();

    List<TbComunidade> findRange(int[] range);

    int count();
    
    List<MunicipalReportDTO> findMunicipalInfo(String idDepartamento, String idMunicipio);
    
}
