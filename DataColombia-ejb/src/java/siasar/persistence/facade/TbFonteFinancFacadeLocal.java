/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbFonteFinanc;

/**
 *
 * @author danielo
 */
@Local
public interface TbFonteFinancFacadeLocal {

    void create(TbFonteFinanc tbFonteFinanc);

    void edit(TbFonteFinanc tbFonteFinanc);

    void remove(TbFonteFinanc tbFonteFinanc);

    TbFonteFinanc find(Object id);

    List<TbFonteFinanc> findAll();

    List<TbFonteFinanc> findRange(int[] range);

    int count();
    
}
