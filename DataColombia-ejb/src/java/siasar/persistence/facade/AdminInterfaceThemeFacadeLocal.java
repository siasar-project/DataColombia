/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.AdminInterfaceTheme;

/**
 *
 * @author danielo
 */
@Local
public interface AdminInterfaceThemeFacadeLocal {

    void create(AdminInterfaceTheme adminInterfaceTheme);

    void edit(AdminInterfaceTheme adminInterfaceTheme);

    void remove(AdminInterfaceTheme adminInterfaceTheme);

    AdminInterfaceTheme find(Object id);

    List<AdminInterfaceTheme> findAll();

    List<AdminInterfaceTheme> findRange(int[] range);

    int count();
    
}
