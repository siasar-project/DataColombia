/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbIntervNovoSistAgua;

/**
 *
 * @author danielo
 */
@Local
public interface TbIntervNovoSistAguaFacadeLocal {

    void create(TbIntervNovoSistAgua tbIntervNovoSistAgua);

    void edit(TbIntervNovoSistAgua tbIntervNovoSistAgua);

    void remove(TbIntervNovoSistAgua tbIntervNovoSistAgua);

    TbIntervNovoSistAgua find(Object id);

    List<TbIntervNovoSistAgua> findAll();

    List<TbIntervNovoSistAgua> findRange(int[] range);

    int count();
    
}
