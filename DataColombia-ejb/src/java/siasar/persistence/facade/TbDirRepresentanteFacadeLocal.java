/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbDirRepresentante;

/**
 *
 * @author danielo
 */
@Local
public interface TbDirRepresentanteFacadeLocal {

    void create(TbDirRepresentante tbDirRepresentante);

    void edit(TbDirRepresentante tbDirRepresentante);

    void remove(TbDirRepresentante tbDirRepresentante);

    TbDirRepresentante find(Object id);

    List<TbDirRepresentante> findAll();

    List<TbDirRepresentante> findRange(int[] range);

    int count();
    
}
