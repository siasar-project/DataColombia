/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbRedeDistribuicao;

/**
 *
 * @author danielo
 */
@Local
public interface TbRedeDistribuicaoFacadeLocal {

    void create(TbRedeDistribuicao tbRedeDistribuicao);

    void edit(TbRedeDistribuicao tbRedeDistribuicao);

    void remove(TbRedeDistribuicao tbRedeDistribuicao);

    TbRedeDistribuicao find(Object id);

    List<TbRedeDistribuicao> findAll();

    List<TbRedeDistribuicao> findRange(int[] range);

    int count();
    
}
