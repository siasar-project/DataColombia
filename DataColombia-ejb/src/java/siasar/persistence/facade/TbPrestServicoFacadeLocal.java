/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbPrestServico;

/**
 *
 * @author danielo
 */
@Local
public interface TbPrestServicoFacadeLocal {

    void create(TbPrestServico tbPrestServico);

    void edit(TbPrestServico tbPrestServico);

    void remove(TbPrestServico tbPrestServico);

    TbPrestServico find(Object id);

    List<TbPrestServico> findAll();

    List<TbPrestServico> findRange(int[] range);

    int count();
    
}
