/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbArmazenamento;

/**
 *
 * @author danielo
 */
@Local
public interface TbArmazenamentoFacadeLocal {

    void create(TbArmazenamento tbArmazenamento);

    void edit(TbArmazenamento tbArmazenamento);

    void remove(TbArmazenamento tbArmazenamento);

    TbArmazenamento find(Object id);

    List<TbArmazenamento> findAll();

    List<TbArmazenamento> findRange(int[] range);

    int count();
    
}
