/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import java.util.List;
import javax.ejb.Local;
import siasar.persistence.entities.TbTipSistAbastecimento;

/**
 *
 * @author danielo
 */
@Local
public interface TbTipSistAbastecimentoFacadeLocal {

    void create(TbTipSistAbastecimento tbTipSistAbastecimento);

    void edit(TbTipSistAbastecimento tbTipSistAbastecimento);

    void remove(TbTipSistAbastecimento tbTipSistAbastecimento);

    TbTipSistAbastecimento find(Object id);

    List<TbTipSistAbastecimento> findAll();

    List<TbTipSistAbastecimento> findRange(int[] range);

    int count();
    
}
