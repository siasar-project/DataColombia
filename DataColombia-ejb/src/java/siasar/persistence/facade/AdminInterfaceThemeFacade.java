/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import siasar.persistence.entities.AdminInterfaceTheme;

/**
 *
 * @author danielo
 */
@Stateless
public class AdminInterfaceThemeFacade extends AbstractFacade<AdminInterfaceTheme> implements AdminInterfaceThemeFacadeLocal {

    @PersistenceContext(unitName = "DataColombia-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AdminInterfaceThemeFacade() {
        super(AdminInterfaceTheme.class);
    }
    
}
