/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.dto;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 *
 * @author danielo
 */
public class MunicipalReportDTO {
    
    String departamento;
    String municipio;
    BigInteger vereda;
    BigInteger  nroSistemas;
    String  nroClasePrestador;
    String  nombreClasePrestador;
    BigInteger  agua;
    BigInteger  excretas;
    BigInteger higiene;
    BigInteger aguaJabon;
    BigInteger  residuosSolidos;
    BigDecimal  coberturaInfraestructuraTratamiento;
    BigDecimal  coberturaInfraestructuraSaneamiento;

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public BigInteger getVereda() {
        return vereda;
    }

    public void setVereda(BigInteger vereda) {
        this.vereda = vereda;
    }

    public BigInteger getNroSistemas() {
        return nroSistemas;
    }

    public void setNroSistemas(BigInteger nroSisitemas) {
        this.nroSistemas = nroSisitemas;
    }

    public String getNroClasePrestador() {
        return nroClasePrestador;
    }

    public void setNroClasePrestador(String nroClasePrestador) {
        this.nroClasePrestador = nroClasePrestador;
    }

    public BigInteger getAgua() {
        return agua;
    }

    public void setAgua(BigInteger agua) {
        this.agua = agua;
    }

    public BigInteger getExcretas() {
        return excretas;
    }

    public void setExcretas(BigInteger excretas) {
        this.excretas = excretas;
    }

    public BigInteger getHigiene() {
        return higiene;
    }

    public void setHigiene(BigInteger higiene) {
        this.higiene = higiene;
    }

    public BigInteger getResiduosSolidos() {
        return residuosSolidos;
    }

    public void setResiduosSolidos(BigInteger residuosSolidos) {
        this.residuosSolidos = residuosSolidos;
    }

    public BigDecimal getCoberturaInfraestructuraTratamiento() {
        return coberturaInfraestructuraTratamiento;
    }

    public void setCoberturaInfraestructuraTratamiento(BigDecimal coberturaInfraestructuraTratamiento) {
        this.coberturaInfraestructuraTratamiento = coberturaInfraestructuraTratamiento;
    }

    public BigDecimal getCoberturaInfraestructuraSaneamiento() {
        return coberturaInfraestructuraSaneamiento;
    }

    public void setCoberturaInfraestructuraSaneamiento(BigDecimal coberturaInfraestructuraSaneamiento) {
        this.coberturaInfraestructuraSaneamiento = coberturaInfraestructuraSaneamiento;
    }

    public String getNombreClasePrestador() {
        return nombreClasePrestador;
    }

    public void setNombreClasePrestador(String nombreClasePrestador) {
        this.nombreClasePrestador = nombreClasePrestador;
    }

    public BigInteger getAguaJabon() {
        return aguaJabon;
    }

    public void setAguaJabon(BigInteger aguaJabon) {
        this.aguaJabon = aguaJabon;
    }
    
    
    
}
