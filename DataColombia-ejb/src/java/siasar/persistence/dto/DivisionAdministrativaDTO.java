/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.dto;

/**
 *
 * @author danielo
 */
public class DivisionAdministrativaDTO {
    
    private long seq;
    private String name;
    private long dvPai;

    public long getSeq() {
        return seq;
    }

    public void setSeq(long seq) {
        this.seq = seq;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDvPai() {
        return dvPai;
    }

    public void setDvPai(long dvPai) {
        this.dvPai = dvPai;
    }
    
    
    
}
