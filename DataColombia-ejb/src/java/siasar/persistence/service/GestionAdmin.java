/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import siasar.persistence.dto.MunicipalReportDTO;
import siasar.persistence.dto.PaisDTO;
import siasar.persistence.entities.TbDivisaoAdmin;
import siasar.persistence.facade.TbComunidadeFacadeLocal;
import siasar.persistence.facade.TbDivisaoAdminFacadeLocal;

/**
 *
 * @author danielo
 */
public class GestionAdmin {

    private TbDivisaoAdminFacadeLocal divisionAdministrativaFacade;
    private TbComunidadeFacadeLocal tbComunidad;

    public GestionAdmin() {

        Context context;
        try {

            context = new InitialContext();
            divisionAdministrativaFacade = (TbDivisaoAdminFacadeLocal) context.lookup("java:global/DataColombia/DataColombia-ejb/TbDivisaoAdminFacade");
            tbComunidad = (TbComunidadeFacadeLocal) context.lookup("java:global/DataColombia/DataColombia-ejb/TbComunidadeFacade");

        } catch (NamingException e) {
            System.out.println("Exception in GestionAdmin  " + e);
        }
    }

    public List<PaisDTO> getCountries() {

        List<PaisDTO> listaPaises;
        listaPaises = new ArrayList();
        try {
            listaPaises = divisionAdministrativaFacade.getPaises();
        } catch (Exception e) {
            throw e;
        }
        return listaPaises;
    }

    public List<TbDivisaoAdmin> getLevel(String country, BigInteger pai) {

        List<TbDivisaoAdmin> levelList;
        levelList = new ArrayList();

        try {
            if (pai.equals(BigInteger.ZERO)) {
                levelList = divisionAdministrativaFacade.findLevel(country, BigInteger.ZERO);
            } else {
                levelList = divisionAdministrativaFacade.findLevel(country, pai);
            }
        } catch (Exception e) {
            throw e;
        }

        return levelList;
    }

    public List<MunicipalReportDTO> getMunicipalInfo(String idDepartamento, String idMunicipio) {
        
        List<MunicipalReportDTO> municipalList;
        municipalList = new ArrayList();

        try {
            municipalList = tbComunidad.findMunicipalInfo(idDepartamento, idMunicipio);

        } catch (Exception e) {
            throw e;
        }

        return municipalList;

    }
}
