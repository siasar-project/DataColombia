/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "django_content_type")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DjangoContentType.findAll", query = "SELECT d FROM DjangoContentType d")
    , @NamedQuery(name = "DjangoContentType.findById", query = "SELECT d FROM DjangoContentType d WHERE d.id = :id")
    , @NamedQuery(name = "DjangoContentType.findByAppLabel", query = "SELECT d FROM DjangoContentType d WHERE d.appLabel = :appLabel")
    , @NamedQuery(name = "DjangoContentType.findByModel", query = "SELECT d FROM DjangoContentType d WHERE d.model = :model")})
public class DjangoContentType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "app_label")
    private String appLabel;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "model")
    private String model;
    @OneToMany(mappedBy = "contentTypeId")
    private List<DjangoAdminLog> djangoAdminLogList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contentTypeId")
    private List<AuthPermission> authPermissionList;

    public DjangoContentType() {
    }

    public DjangoContentType(Integer id) {
        this.id = id;
    }

    public DjangoContentType(Integer id, String appLabel, String model) {
        this.id = id;
        this.appLabel = appLabel;
        this.model = model;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAppLabel() {
        return appLabel;
    }

    public void setAppLabel(String appLabel) {
        this.appLabel = appLabel;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @XmlTransient
    public List<DjangoAdminLog> getDjangoAdminLogList() {
        return djangoAdminLogList;
    }

    public void setDjangoAdminLogList(List<DjangoAdminLog> djangoAdminLogList) {
        this.djangoAdminLogList = djangoAdminLogList;
    }

    @XmlTransient
    public List<AuthPermission> getAuthPermissionList() {
        return authPermissionList;
    }

    public void setAuthPermissionList(List<AuthPermission> authPermissionList) {
        this.authPermissionList = authPermissionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DjangoContentType)) {
            return false;
        }
        DjangoContentType other = (DjangoContentType) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.DjangoContentType[ id=" + id + " ]";
    }
    
}
