/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_interv_melhoria")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbIntervMelhoria.findAll", query = "SELECT t FROM TbIntervMelhoria t")
    , @NamedQuery(name = "TbIntervMelhoria.findByIntmeSeq", query = "SELECT t FROM TbIntervMelhoria t WHERE t.intmeSeq = :intmeSeq")
    , @NamedQuery(name = "TbIntervMelhoria.findByIntmeFonte", query = "SELECT t FROM TbIntervMelhoria t WHERE t.intmeFonte = :intmeFonte")
    , @NamedQuery(name = "TbIntervMelhoria.findByDateLastUpdate", query = "SELECT t FROM TbIntervMelhoria t WHERE t.dateLastUpdate = :dateLastUpdate")})
public class TbIntervMelhoria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "intme_seq")
    private Long intmeSeq;
    @Size(max = 255)
    @Column(name = "intme_fonte")
    private String intmeFonte;
    @Column(name = "date_last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdate;
    @JoinColumn(name = "comun_seq", referencedColumnName = "comun_seq")
    @ManyToOne
    private TbComunidade comunSeq;
    @JoinColumn(name = "taxt_seq", referencedColumnName = "taxt_seq")
    @ManyToOne
    private TbTaxoTermo taxtSeq;

    public TbIntervMelhoria() {
    }

    public TbIntervMelhoria(Long intmeSeq) {
        this.intmeSeq = intmeSeq;
    }

    public Long getIntmeSeq() {
        return intmeSeq;
    }

    public void setIntmeSeq(Long intmeSeq) {
        this.intmeSeq = intmeSeq;
    }

    public String getIntmeFonte() {
        return intmeFonte;
    }

    public void setIntmeFonte(String intmeFonte) {
        this.intmeFonte = intmeFonte;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    public TbComunidade getComunSeq() {
        return comunSeq;
    }

    public void setComunSeq(TbComunidade comunSeq) {
        this.comunSeq = comunSeq;
    }

    public TbTaxoTermo getTaxtSeq() {
        return taxtSeq;
    }

    public void setTaxtSeq(TbTaxoTermo taxtSeq) {
        this.taxtSeq = taxtSeq;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (intmeSeq != null ? intmeSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbIntervMelhoria)) {
            return false;
        }
        TbIntervMelhoria other = (TbIntervMelhoria) object;
        if ((this.intmeSeq == null && other.intmeSeq != null) || (this.intmeSeq != null && !this.intmeSeq.equals(other.intmeSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbIntervMelhoria[ intmeSeq=" + intmeSeq + " ]";
    }
    
}
