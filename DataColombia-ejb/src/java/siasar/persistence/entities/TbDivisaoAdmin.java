/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_divisao_admin")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbDivisaoAdmin.findAll", query = "SELECT t FROM TbDivisaoAdmin t")
    , @NamedQuery(name = "TbDivisaoAdmin.findByTdaSeq", query = "SELECT t FROM TbDivisaoAdmin t WHERE t.tdaSeq = :tdaSeq")
    , @NamedQuery(name = "TbDivisaoAdmin.findByTdaNome", query = "SELECT t FROM TbDivisaoAdmin t WHERE t.tdaNome = :tdaNome")
    , @NamedQuery(name = "TbDivisaoAdmin.findByTdaPai", query = "SELECT t FROM TbDivisaoAdmin t WHERE t.tdaPai = :tdaPai")
    , @NamedQuery(name = "TbDivisaoAdmin.findByTdaPais", query = "SELECT t FROM TbDivisaoAdmin t WHERE t.tdaPais = :tdaPais")
    , @NamedQuery(name = "TbDivisaoAdmin.findByDateLastUpdate", query = "SELECT t FROM TbDivisaoAdmin t WHERE t.dateLastUpdate = :dateLastUpdate")})
public class TbDivisaoAdmin implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "tda_seq")
    private Long tdaSeq;
    @Size(max = 255)
    @Column(name = "tda_nome")
    private String tdaNome;
    @Column(name = "tda_pai")
    private BigInteger tdaPai;
    @Size(max = 2)
    @Column(name = "tda_pais")
    private String tdaPais;
    @Column(name = "date_last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdate;
    @OneToMany(mappedBy = "vardwLocalidade")
    private List<TbVariavelDw> tbVariavelDwList;

    public TbDivisaoAdmin() {
    }

    public TbDivisaoAdmin(Long tdaSeq) {
        this.tdaSeq = tdaSeq;
    }

    public Long getTdaSeq() {
        return tdaSeq;
    }

    public void setTdaSeq(Long tdaSeq) {
        this.tdaSeq = tdaSeq;
    }

    public String getTdaNome() {
        return tdaNome;
    }

    public void setTdaNome(String tdaNome) {
        this.tdaNome = tdaNome;
    }

    public BigInteger getTdaPai() {
        return tdaPai;
    }

    public void setTdaPai(BigInteger tdaPai) {
        this.tdaPai = tdaPai;
    }

    public String getTdaPais() {
        return tdaPais;
    }

    public void setTdaPais(String tdaPais) {
        this.tdaPais = tdaPais;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    @XmlTransient
    public List<TbVariavelDw> getTbVariavelDwList() {
        return tbVariavelDwList;
    }

    public void setTbVariavelDwList(List<TbVariavelDw> tbVariavelDwList) {
        this.tbVariavelDwList = tbVariavelDwList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tdaSeq != null ? tdaSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbDivisaoAdmin)) {
            return false;
        }
        TbDivisaoAdmin other = (TbDivisaoAdmin) object;
        if ((this.tdaSeq == null && other.tdaSeq != null) || (this.tdaSeq != null && !this.tdaSeq.equals(other.tdaSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbDivisaoAdmin[ tdaSeq=" + tdaSeq + " ]";
    }
    
}
