/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_variavaria")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbVariavaria.findAll", query = "SELECT t FROM TbVariavaria t")
    , @NamedQuery(name = "TbVariavaria.findByVavariaSeq", query = "SELECT t FROM TbVariavaria t WHERE t.vavariaSeq = :vavariaSeq")})
public class TbVariavaria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "vavaria_seq")
    private Long vavariaSeq;
    @JoinColumn(name = "varia_pai", referencedColumnName = "varia_seq")
    @ManyToOne
    private TbVariavel variaPai;
    @JoinColumn(name = "varia_filho", referencedColumnName = "varia_seq")
    @ManyToOne
    private TbVariavel variaFilho;

    public TbVariavaria() {
    }

    public TbVariavaria(Long vavariaSeq) {
        this.vavariaSeq = vavariaSeq;
    }

    public Long getVavariaSeq() {
        return vavariaSeq;
    }

    public void setVavariaSeq(Long vavariaSeq) {
        this.vavariaSeq = vavariaSeq;
    }

    public TbVariavel getVariaPai() {
        return variaPai;
    }

    public void setVariaPai(TbVariavel variaPai) {
        this.variaPai = variaPai;
    }

    public TbVariavel getVariaFilho() {
        return variaFilho;
    }

    public void setVariaFilho(TbVariavel variaFilho) {
        this.variaFilho = variaFilho;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vavariaSeq != null ? vavariaSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbVariavaria)) {
            return false;
        }
        TbVariavaria other = (TbVariavaria) object;
        if ((this.vavariaSeq == null && other.vavariaSeq != null) || (this.vavariaSeq != null && !this.vavariaSeq.equals(other.vavariaSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbVariavaria[ vavariaSeq=" + vavariaSeq + " ]";
    }
    
}
