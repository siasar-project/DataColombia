/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_log_calculo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbLogCalculo.findAll", query = "SELECT t FROM TbLogCalculo t")
    , @NamedQuery(name = "TbLogCalculo.findByLogcalSeq", query = "SELECT t FROM TbLogCalculo t WHERE t.logcalSeq = :logcalSeq")
    , @NamedQuery(name = "TbLogCalculo.findByLogcalNomeVar", query = "SELECT t FROM TbLogCalculo t WHERE t.logcalNomeVar = :logcalNomeVar")
    , @NamedQuery(name = "TbLogCalculo.findByLogcalCmdExec", query = "SELECT t FROM TbLogCalculo t WHERE t.logcalCmdExec = :logcalCmdExec")
    , @NamedQuery(name = "TbLogCalculo.findByLogcalErroValor", query = "SELECT t FROM TbLogCalculo t WHERE t.logcalErroValor = :logcalErroValor")
    , @NamedQuery(name = "TbLogCalculo.findByLogcalErroResult", query = "SELECT t FROM TbLogCalculo t WHERE t.logcalErroResult = :logcalErroResult")
    , @NamedQuery(name = "TbLogCalculo.findByLogcalErroReparado", query = "SELECT t FROM TbLogCalculo t WHERE t.logcalErroReparado = :logcalErroReparado")
    , @NamedQuery(name = "TbLogCalculo.findByLogcalData", query = "SELECT t FROM TbLogCalculo t WHERE t.logcalData = :logcalData")})
public class TbLogCalculo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "logcal_seq")
    private Long logcalSeq;
    @Size(max = 2147483647)
    @Column(name = "logcal_nome_var")
    private String logcalNomeVar;
    @Size(max = 2147483647)
    @Column(name = "logcal_cmd_exec")
    private String logcalCmdExec;
    @Size(max = 2147483647)
    @Column(name = "logcal_erro_valor")
    private String logcalErroValor;
    @Size(max = 2147483647)
    @Column(name = "logcal_erro_result")
    private String logcalErroResult;
    @Column(name = "logcal_erro_reparado")
    private Boolean logcalErroReparado;
    @Column(name = "logcal_data")
    @Temporal(TemporalType.TIMESTAMP)
    private Date logcalData;

    public TbLogCalculo() {
    }

    public TbLogCalculo(Long logcalSeq) {
        this.logcalSeq = logcalSeq;
    }

    public Long getLogcalSeq() {
        return logcalSeq;
    }

    public void setLogcalSeq(Long logcalSeq) {
        this.logcalSeq = logcalSeq;
    }

    public String getLogcalNomeVar() {
        return logcalNomeVar;
    }

    public void setLogcalNomeVar(String logcalNomeVar) {
        this.logcalNomeVar = logcalNomeVar;
    }

    public String getLogcalCmdExec() {
        return logcalCmdExec;
    }

    public void setLogcalCmdExec(String logcalCmdExec) {
        this.logcalCmdExec = logcalCmdExec;
    }

    public String getLogcalErroValor() {
        return logcalErroValor;
    }

    public void setLogcalErroValor(String logcalErroValor) {
        this.logcalErroValor = logcalErroValor;
    }

    public String getLogcalErroResult() {
        return logcalErroResult;
    }

    public void setLogcalErroResult(String logcalErroResult) {
        this.logcalErroResult = logcalErroResult;
    }

    public Boolean getLogcalErroReparado() {
        return logcalErroReparado;
    }

    public void setLogcalErroReparado(Boolean logcalErroReparado) {
        this.logcalErroReparado = logcalErroReparado;
    }

    public Date getLogcalData() {
        return logcalData;
    }

    public void setLogcalData(Date logcalData) {
        this.logcalData = logcalData;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (logcalSeq != null ? logcalSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbLogCalculo)) {
            return false;
        }
        TbLogCalculo other = (TbLogCalculo) object;
        if ((this.logcalSeq == null && other.logcalSeq != null) || (this.logcalSeq != null && !this.logcalSeq.equals(other.logcalSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbLogCalculo[ logcalSeq=" + logcalSeq + " ]";
    }
    
}
