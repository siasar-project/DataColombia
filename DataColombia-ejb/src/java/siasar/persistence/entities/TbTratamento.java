/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_tratamento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbTratamento.findAll", query = "SELECT t FROM TbTratamento t")
    , @NamedQuery(name = "TbTratamento.findByTrataSeq", query = "SELECT t FROM TbTratamento t WHERE t.trataSeq = :trataSeq")
    , @NamedQuery(name = "TbTratamento.findBySysD001001", query = "SELECT t FROM TbTratamento t WHERE t.sysD001001 = :sysD001001")
    , @NamedQuery(name = "TbTratamento.findBySysD002001", query = "SELECT t FROM TbTratamento t WHERE t.sysD002001 = :sysD002001")
    , @NamedQuery(name = "TbTratamento.findBySysD003001", query = "SELECT t FROM TbTratamento t WHERE t.sysD003001 = :sysD003001")
    , @NamedQuery(name = "TbTratamento.findBySysD004001", query = "SELECT t FROM TbTratamento t WHERE t.sysD004001 = :sysD004001")
    , @NamedQuery(name = "TbTratamento.findBySysD005001", query = "SELECT t FROM TbTratamento t WHERE t.sysD005001 = :sysD005001")
    , @NamedQuery(name = "TbTratamento.findBySysD006001", query = "SELECT t FROM TbTratamento t WHERE t.sysD006001 = :sysD006001")
    , @NamedQuery(name = "TbTratamento.findBySysD007001", query = "SELECT t FROM TbTratamento t WHERE t.sysD007001 = :sysD007001")
    , @NamedQuery(name = "TbTratamento.findBySysD008001", query = "SELECT t FROM TbTratamento t WHERE t.sysD008001 = :sysD008001")
    , @NamedQuery(name = "TbTratamento.findByDateLastUpdate", query = "SELECT t FROM TbTratamento t WHERE t.dateLastUpdate = :dateLastUpdate")})
public class TbTratamento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "trata_seq")
    private Long trataSeq;
    @Size(max = 100)
    @Column(name = "sys_d_001_001")
    private String sysD001001;
    @Column(name = "sys_d_002_001")
    private Integer sysD002001;
    @Column(name = "sys_d_003_001")
    private Boolean sysD003001;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "sys_d_004_001")
    private Double sysD004001;
    @Column(name = "sys_d_005_001")
    private Double sysD005001;
    @Column(name = "sys_d_006_001")
    private Double sysD006001;
    @Column(name = "sys_d_007_001")
    private Integer sysD007001;
    @Size(max = 2147483647)
    @Column(name = "sys_d_008_001")
    private String sysD008001;
    @Column(name = "date_last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdate;
    @JoinColumn(name = "siste_seq", referencedColumnName = "siste_seq")
    @ManyToOne
    private TbSistema sisteSeq;

    public TbTratamento() {
    }

    public TbTratamento(Long trataSeq) {
        this.trataSeq = trataSeq;
    }

    public Long getTrataSeq() {
        return trataSeq;
    }

    public void setTrataSeq(Long trataSeq) {
        this.trataSeq = trataSeq;
    }

    public String getSysD001001() {
        return sysD001001;
    }

    public void setSysD001001(String sysD001001) {
        this.sysD001001 = sysD001001;
    }

    public Integer getSysD002001() {
        return sysD002001;
    }

    public void setSysD002001(Integer sysD002001) {
        this.sysD002001 = sysD002001;
    }

    public Boolean getSysD003001() {
        return sysD003001;
    }

    public void setSysD003001(Boolean sysD003001) {
        this.sysD003001 = sysD003001;
    }

    public Double getSysD004001() {
        return sysD004001;
    }

    public void setSysD004001(Double sysD004001) {
        this.sysD004001 = sysD004001;
    }

    public Double getSysD005001() {
        return sysD005001;
    }

    public void setSysD005001(Double sysD005001) {
        this.sysD005001 = sysD005001;
    }

    public Double getSysD006001() {
        return sysD006001;
    }

    public void setSysD006001(Double sysD006001) {
        this.sysD006001 = sysD006001;
    }

    public Integer getSysD007001() {
        return sysD007001;
    }

    public void setSysD007001(Integer sysD007001) {
        this.sysD007001 = sysD007001;
    }

    public String getSysD008001() {
        return sysD008001;
    }

    public void setSysD008001(String sysD008001) {
        this.sysD008001 = sysD008001;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    public TbSistema getSisteSeq() {
        return sisteSeq;
    }

    public void setSisteSeq(TbSistema sisteSeq) {
        this.sisteSeq = sisteSeq;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (trataSeq != null ? trataSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbTratamento)) {
            return false;
        }
        TbTratamento other = (TbTratamento) object;
        if ((this.trataSeq == null && other.trataSeq != null) || (this.trataSeq != null && !this.trataSeq.equals(other.trataSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbTratamento[ trataSeq=" + trataSeq + " ]";
    }
    
}
