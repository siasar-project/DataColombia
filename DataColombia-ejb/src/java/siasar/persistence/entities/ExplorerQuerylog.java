/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "explorer_querylog")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ExplorerQuerylog.findAll", query = "SELECT e FROM ExplorerQuerylog e")
    , @NamedQuery(name = "ExplorerQuerylog.findById", query = "SELECT e FROM ExplorerQuerylog e WHERE e.id = :id")
    , @NamedQuery(name = "ExplorerQuerylog.findBySql", query = "SELECT e FROM ExplorerQuerylog e WHERE e.sql = :sql")
    , @NamedQuery(name = "ExplorerQuerylog.findByRunAt", query = "SELECT e FROM ExplorerQuerylog e WHERE e.runAt = :runAt")
    , @NamedQuery(name = "ExplorerQuerylog.findByDuration", query = "SELECT e FROM ExplorerQuerylog e WHERE e.duration = :duration")})
public class ExplorerQuerylog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 2147483647)
    @Column(name = "sql")
    private String sql;
    @Basic(optional = false)
    @NotNull
    @Column(name = "run_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date runAt;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "duration")
    private Double duration;
    @JoinColumn(name = "run_by_user_id", referencedColumnName = "id")
    @ManyToOne
    private AuthUser runByUserId;
    @JoinColumn(name = "query_id", referencedColumnName = "id")
    @ManyToOne
    private ExplorerQuery queryId;

    public ExplorerQuerylog() {
    }

    public ExplorerQuerylog(Integer id) {
        this.id = id;
    }

    public ExplorerQuerylog(Integer id, Date runAt) {
        this.id = id;
        this.runAt = runAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public Date getRunAt() {
        return runAt;
    }

    public void setRunAt(Date runAt) {
        this.runAt = runAt;
    }

    public Double getDuration() {
        return duration;
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }

    public AuthUser getRunByUserId() {
        return runByUserId;
    }

    public void setRunByUserId(AuthUser runByUserId) {
        this.runByUserId = runByUserId;
    }

    public ExplorerQuery getQueryId() {
        return queryId;
    }

    public void setQueryId(ExplorerQuery queryId) {
        this.queryId = queryId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExplorerQuerylog)) {
            return false;
        }
        ExplorerQuerylog other = (ExplorerQuerylog) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.ExplorerQuerylog[ id=" + id + " ]";
    }
    
}
