/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_comunidade")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbComunidade.findAll", query = "SELECT t FROM TbComunidade t")
    , @NamedQuery(name = "TbComunidade.findByComunSeq", query = "SELECT t FROM TbComunidade t WHERE t.comunSeq = :comunSeq")
    , @NamedQuery(name = "TbComunidade.findByPatSeq", query = "SELECT t FROM TbComunidade t WHERE t.patSeq = :patSeq")
    , @NamedQuery(name = "TbComunidade.findByValidado", query = "SELECT t FROM TbComunidade t WHERE t.validado = :validado")
    , @NamedQuery(name = "TbComunidade.findByDataValidacao", query = "SELECT t FROM TbComunidade t WHERE t.dataValidacao = :dataValidacao")
    , @NamedQuery(name = "TbComunidade.findByComA001001", query = "SELECT t FROM TbComunidade t WHERE t.comA001001 = :comA001001")
    , @NamedQuery(name = "TbComunidade.findByComA002001", query = "SELECT t FROM TbComunidade t WHERE t.comA002001 = :comA002001")
    , @NamedQuery(name = "TbComunidade.findByComA003001", query = "SELECT t FROM TbComunidade t WHERE t.comA003001 = :comA003001")
    , @NamedQuery(name = "TbComunidade.findByComA004001", query = "SELECT t FROM TbComunidade t WHERE t.comA004001 = :comA004001")
    , @NamedQuery(name = "TbComunidade.findByComA005001", query = "SELECT t FROM TbComunidade t WHERE t.comA005001 = :comA005001")
    , @NamedQuery(name = "TbComunidade.findByComA006001", query = "SELECT t FROM TbComunidade t WHERE t.comA006001 = :comA006001")
    , @NamedQuery(name = "TbComunidade.findByComA007001", query = "SELECT t FROM TbComunidade t WHERE t.comA007001 = :comA007001")
    , @NamedQuery(name = "TbComunidade.findByComA008001", query = "SELECT t FROM TbComunidade t WHERE t.comA008001 = :comA008001")
    , @NamedQuery(name = "TbComunidade.findByComA009001", query = "SELECT t FROM TbComunidade t WHERE t.comA009001 = :comA009001")
    , @NamedQuery(name = "TbComunidade.findByComA010001", query = "SELECT t FROM TbComunidade t WHERE t.comA010001 = :comA010001")
    , @NamedQuery(name = "TbComunidade.findByComA014001", query = "SELECT t FROM TbComunidade t WHERE t.comA014001 = :comA014001")
    , @NamedQuery(name = "TbComunidade.findByComA015001", query = "SELECT t FROM TbComunidade t WHERE t.comA015001 = :comA015001")
    , @NamedQuery(name = "TbComunidade.findByComA016001", query = "SELECT t FROM TbComunidade t WHERE t.comA016001 = :comA016001")
    , @NamedQuery(name = "TbComunidade.findByComA017001", query = "SELECT t FROM TbComunidade t WHERE t.comA017001 = :comA017001")
    , @NamedQuery(name = "TbComunidade.findByComA018001", query = "SELECT t FROM TbComunidade t WHERE t.comA018001 = :comA018001")
    , @NamedQuery(name = "TbComunidade.findByComA023001", query = "SELECT t FROM TbComunidade t WHERE t.comA023001 = :comA023001")
    , @NamedQuery(name = "TbComunidade.findByComA024001", query = "SELECT t FROM TbComunidade t WHERE t.comA024001 = :comA024001")
    , @NamedQuery(name = "TbComunidade.findByComA025001", query = "SELECT t FROM TbComunidade t WHERE t.comA025001 = :comA025001")
    , @NamedQuery(name = "TbComunidade.findByComA026001", query = "SELECT t FROM TbComunidade t WHERE t.comA026001 = :comA026001")
    , @NamedQuery(name = "TbComunidade.findByComA027001", query = "SELECT t FROM TbComunidade t WHERE t.comA027001 = :comA027001")
    , @NamedQuery(name = "TbComunidade.findByComA028001", query = "SELECT t FROM TbComunidade t WHERE t.comA028001 = :comA028001")
    , @NamedQuery(name = "TbComunidade.findByComB001001", query = "SELECT t FROM TbComunidade t WHERE t.comB001001 = :comB001001")
    , @NamedQuery(name = "TbComunidade.findByComB002001", query = "SELECT t FROM TbComunidade t WHERE t.comB002001 = :comB002001")
    , @NamedQuery(name = "TbComunidade.findByComB003001", query = "SELECT t FROM TbComunidade t WHERE t.comB003001 = :comB003001")
    , @NamedQuery(name = "TbComunidade.findByComB004001", query = "SELECT t FROM TbComunidade t WHERE t.comB004001 = :comB004001")
    , @NamedQuery(name = "TbComunidade.findByComB005001", query = "SELECT t FROM TbComunidade t WHERE t.comB005001 = :comB005001")
    , @NamedQuery(name = "TbComunidade.findByComB006001", query = "SELECT t FROM TbComunidade t WHERE t.comB006001 = :comB006001")
    , @NamedQuery(name = "TbComunidade.findByComB007001", query = "SELECT t FROM TbComunidade t WHERE t.comB007001 = :comB007001")
    , @NamedQuery(name = "TbComunidade.findByComB008001", query = "SELECT t FROM TbComunidade t WHERE t.comB008001 = :comB008001")
    , @NamedQuery(name = "TbComunidade.findByComB009001", query = "SELECT t FROM TbComunidade t WHERE t.comB009001 = :comB009001")
    , @NamedQuery(name = "TbComunidade.findByComB010001", query = "SELECT t FROM TbComunidade t WHERE t.comB010001 = :comB010001")
    , @NamedQuery(name = "TbComunidade.findByComB011001", query = "SELECT t FROM TbComunidade t WHERE t.comB011001 = :comB011001")
    , @NamedQuery(name = "TbComunidade.findByComB012001", query = "SELECT t FROM TbComunidade t WHERE t.comB012001 = :comB012001")
    , @NamedQuery(name = "TbComunidade.findByComB013001", query = "SELECT t FROM TbComunidade t WHERE t.comB013001 = :comB013001")
    , @NamedQuery(name = "TbComunidade.findByComB014001", query = "SELECT t FROM TbComunidade t WHERE t.comB014001 = :comB014001")
    , @NamedQuery(name = "TbComunidade.findByComB015001", query = "SELECT t FROM TbComunidade t WHERE t.comB015001 = :comB015001")
    , @NamedQuery(name = "TbComunidade.findByComB016001", query = "SELECT t FROM TbComunidade t WHERE t.comB016001 = :comB016001")
    , @NamedQuery(name = "TbComunidade.findByComB017001", query = "SELECT t FROM TbComunidade t WHERE t.comB017001 = :comB017001")
    , @NamedQuery(name = "TbComunidade.findByComB018001", query = "SELECT t FROM TbComunidade t WHERE t.comB018001 = :comB018001")
    , @NamedQuery(name = "TbComunidade.findByComB019001", query = "SELECT t FROM TbComunidade t WHERE t.comB019001 = :comB019001")
    , @NamedQuery(name = "TbComunidade.findByComB020001", query = "SELECT t FROM TbComunidade t WHERE t.comB020001 = :comB020001")
    , @NamedQuery(name = "TbComunidade.findByComB021001", query = "SELECT t FROM TbComunidade t WHERE t.comB021001 = :comB021001")
    , @NamedQuery(name = "TbComunidade.findByComB022001", query = "SELECT t FROM TbComunidade t WHERE t.comB022001 = :comB022001")
    , @NamedQuery(name = "TbComunidade.findByComB023001", query = "SELECT t FROM TbComunidade t WHERE t.comB023001 = :comB023001")
    , @NamedQuery(name = "TbComunidade.findByComC001001", query = "SELECT t FROM TbComunidade t WHERE t.comC001001 = :comC001001")
    , @NamedQuery(name = "TbComunidade.findByComC002001", query = "SELECT t FROM TbComunidade t WHERE t.comC002001 = :comC002001")
    , @NamedQuery(name = "TbComunidade.findByComC003001", query = "SELECT t FROM TbComunidade t WHERE t.comC003001 = :comC003001")
    , @NamedQuery(name = "TbComunidade.findByComD001001", query = "SELECT t FROM TbComunidade t WHERE t.comD001001 = :comD001001")
    , @NamedQuery(name = "TbComunidade.findByComD002001", query = "SELECT t FROM TbComunidade t WHERE t.comD002001 = :comD002001")
    , @NamedQuery(name = "TbComunidade.findByComD003001", query = "SELECT t FROM TbComunidade t WHERE t.comD003001 = :comD003001")
    , @NamedQuery(name = "TbComunidade.findByComF001001", query = "SELECT t FROM TbComunidade t WHERE t.comF001001 = :comF001001")
    , @NamedQuery(name = "TbComunidade.findByUrlImagem", query = "SELECT t FROM TbComunidade t WHERE t.urlImagem = :urlImagem")
    , @NamedQuery(name = "TbComunidade.findByClassificacao", query = "SELECT t FROM TbComunidade t WHERE t.classificacao = :classificacao")
    , @NamedQuery(name = "TbComunidade.findByPaisSigla", query = "SELECT t FROM TbComunidade t WHERE t.paisSigla = :paisSigla")
    , @NamedQuery(name = "TbComunidade.findByDateLastUpdate", query = "SELECT t FROM TbComunidade t WHERE t.dateLastUpdate = :dateLastUpdate")
    , @NamedQuery(name = "TbComunidade.findByComA011001", query = "SELECT t FROM TbComunidade t WHERE t.comA011001 = :comA011001")
    , @NamedQuery(name = "TbComunidade.findByComA012001", query = "SELECT t FROM TbComunidade t WHERE t.comA012001 = :comA012001")
    , @NamedQuery(name = "TbComunidade.findByComA013001", query = "SELECT t FROM TbComunidade t WHERE t.comA013001 = :comA013001")})
public class TbComunidade implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "comun_seq")
    private Long comunSeq;
    @Column(name = "pat_seq")
    private BigInteger patSeq;
    @Column(name = "validado")
    private Boolean validado;
    @Column(name = "data_validacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataValidacao;
    @Column(name = "com_a_001_001")
    @Temporal(TemporalType.DATE)
    private Date comA001001;
    @Size(max = 255)
    @Column(name = "com_a_002_001")
    private String comA002001;
    @Size(max = 255)
    @Column(name = "com_a_003_001")
    private String comA003001;
    @Column(name = "com_a_004_001")
    private Integer comA004001;
    @Column(name = "com_a_005_001")
    private Integer comA005001;
    @Size(max = 2)
    @Column(name = "com_a_006_001")
    private String comA006001;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "com_a_007_001")
    private Double comA007001;
    @Column(name = "com_a_008_001")
    private Double comA008001;
    @Column(name = "com_a_009_001")
    private Double comA009001;
    @Size(max = 30)
    @Column(name = "com_a_010_001")
    private String comA010001;
    @Column(name = "com_a_014_001")
    private Integer comA014001;
    @Column(name = "com_a_015_001")
    private Integer comA015001;
    @Column(name = "com_a_016_001")
    private Integer comA016001;
    @Size(max = 2147483647)
    @Column(name = "com_a_017_001")
    private String comA017001;
    @Column(name = "com_a_018_001")
    private Integer comA018001;
    @Column(name = "com_a_023_001")
    private Integer comA023001;
    @Column(name = "com_a_024_001")
    private Boolean comA024001;
    @Column(name = "com_a_025_001")
    private Boolean comA025001;
    @Column(name = "com_a_026_001")
    private Boolean comA026001;
    @Column(name = "com_a_027_001")
    private Boolean comA027001;
    @Size(max = 2147483647)
    @Column(name = "com_a_028_001")
    private String comA028001;
    @Column(name = "com_b_001_001")
    private Boolean comB001001;
    @Column(name = "com_b_002_001")
    private Boolean comB002001;
    @Column(name = "com_b_003_001")
    private Boolean comB003001;
    @Column(name = "com_b_004_001")
    private Integer comB004001;
    @Column(name = "com_b_005_001")
    private Integer comB005001;
    @Column(name = "com_b_006_001")
    private Integer comB006001;
    @Column(name = "com_b_007_001")
    private Integer comB007001;
    @Column(name = "com_b_008_001")
    private Integer comB008001;
    @Column(name = "com_b_009_001")
    private Integer comB009001;
    @Column(name = "com_b_010_001")
    private Integer comB010001;
    @Column(name = "com_b_011_001")
    private Integer comB011001;
    @Column(name = "com_b_012_001")
    private Integer comB012001;
    @Column(name = "com_b_013_001")
    private Integer comB013001;
    @Column(name = "com_b_014_001")
    private Integer comB014001;
    @Column(name = "com_b_015_001")
    private Integer comB015001;
    @Column(name = "com_b_016_001")
    private Integer comB016001;
    @Column(name = "com_b_017_001")
    private Integer comB017001;
    @Column(name = "com_b_018_001")
    private Integer comB018001;
    @Column(name = "com_b_019_001")
    private Integer comB019001;
    @Column(name = "com_b_020_001")
    private Integer comB020001;
    @Column(name = "com_b_021_001")
    private Boolean comB021001;
    @Column(name = "com_b_022_001")
    private Integer comB022001;
    @Column(name = "com_b_023_001")
    private Integer comB023001;
    @Column(name = "com_c_001_001")
    private Boolean comC001001;
    @Size(max = 255)
    @Column(name = "com_c_002_001")
    private String comC002001;
    @Column(name = "com_c_003_001")
    private Integer comC003001;
    @Column(name = "com_d_001_001")
    private Boolean comD001001;
    @Size(max = 2147483647)
    @Column(name = "com_d_002_001")
    private String comD002001;
    @Size(max = 2147483647)
    @Column(name = "com_d_003_001")
    private String comD003001;
    @Size(max = 2147483647)
    @Column(name = "com_f_001_001")
    private String comF001001;
    @Size(max = 4000)
    @Column(name = "url_imagem")
    private String urlImagem;
    @Size(max = 1)
    @Column(name = "classificacao")
    private String classificacao;
    @Size(max = 60)
    @Column(name = "pais_sigla")
    private String paisSigla;
    @Column(name = "date_last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdate;
    @Column(name = "com_a_011_001")
    private Integer comA011001;
    @Column(name = "com_a_012_001")
    private Integer comA012001;
    @Column(name = "com_a_013_001")
    private Integer comA013001;
    @OneToMany(mappedBy = "comunSeq")
    private List<TbIntervSistSaneMelhor> tbIntervSistSaneMelhorList;
    @OneToMany(mappedBy = "comunSeq")
    private List<TbIntervMelhoria> tbIntervMelhoriaList;
    @OneToMany(mappedBy = "comunSeq")
    private List<TbCentroEducativo> tbCentroEducativoList;
    @OneToMany(mappedBy = "comunSeq")
    private List<TbGrpDomicilio> tbGrpDomicilioList;
    @OneToMany(mappedBy = "comunSeq")
    private List<TbIntervSistSaneNaoMelhor> tbIntervSistSaneNaoMelhorList;
    @OneToMany(mappedBy = "comunSeq")
    private List<TbCentroSaude> tbCentroSaudeList;
    @OneToMany(mappedBy = "comunSeq")
    private List<TbIntervNovoSistAgua> tbIntervNovoSistAguaList;

    public TbComunidade() {
    }

    public TbComunidade(Long comunSeq) {
        this.comunSeq = comunSeq;
    }

    public Long getComunSeq() {
        return comunSeq;
    }

    public void setComunSeq(Long comunSeq) {
        this.comunSeq = comunSeq;
    }

    public BigInteger getPatSeq() {
        return patSeq;
    }

    public void setPatSeq(BigInteger patSeq) {
        this.patSeq = patSeq;
    }

    public Boolean getValidado() {
        return validado;
    }

    public void setValidado(Boolean validado) {
        this.validado = validado;
    }

    public Date getDataValidacao() {
        return dataValidacao;
    }

    public void setDataValidacao(Date dataValidacao) {
        this.dataValidacao = dataValidacao;
    }

    public Date getComA001001() {
        return comA001001;
    }

    public void setComA001001(Date comA001001) {
        this.comA001001 = comA001001;
    }

    public String getComA002001() {
        return comA002001;
    }

    public void setComA002001(String comA002001) {
        this.comA002001 = comA002001;
    }

    public String getComA003001() {
        return comA003001;
    }

    public void setComA003001(String comA003001) {
        this.comA003001 = comA003001;
    }

    public Integer getComA004001() {
        return comA004001;
    }

    public void setComA004001(Integer comA004001) {
        this.comA004001 = comA004001;
    }

    public Integer getComA005001() {
        return comA005001;
    }

    public void setComA005001(Integer comA005001) {
        this.comA005001 = comA005001;
    }

    public String getComA006001() {
        return comA006001;
    }

    public void setComA006001(String comA006001) {
        this.comA006001 = comA006001;
    }

    public Double getComA007001() {
        return comA007001;
    }

    public void setComA007001(Double comA007001) {
        this.comA007001 = comA007001;
    }

    public Double getComA008001() {
        return comA008001;
    }

    public void setComA008001(Double comA008001) {
        this.comA008001 = comA008001;
    }

    public Double getComA009001() {
        return comA009001;
    }

    public void setComA009001(Double comA009001) {
        this.comA009001 = comA009001;
    }

    public String getComA010001() {
        return comA010001;
    }

    public void setComA010001(String comA010001) {
        this.comA010001 = comA010001;
    }

    public Integer getComA014001() {
        return comA014001;
    }

    public void setComA014001(Integer comA014001) {
        this.comA014001 = comA014001;
    }

    public Integer getComA015001() {
        return comA015001;
    }

    public void setComA015001(Integer comA015001) {
        this.comA015001 = comA015001;
    }

    public Integer getComA016001() {
        return comA016001;
    }

    public void setComA016001(Integer comA016001) {
        this.comA016001 = comA016001;
    }

    public String getComA017001() {
        return comA017001;
    }

    public void setComA017001(String comA017001) {
        this.comA017001 = comA017001;
    }

    public Integer getComA018001() {
        return comA018001;
    }

    public void setComA018001(Integer comA018001) {
        this.comA018001 = comA018001;
    }

    public Integer getComA023001() {
        return comA023001;
    }

    public void setComA023001(Integer comA023001) {
        this.comA023001 = comA023001;
    }

    public Boolean getComA024001() {
        return comA024001;
    }

    public void setComA024001(Boolean comA024001) {
        this.comA024001 = comA024001;
    }

    public Boolean getComA025001() {
        return comA025001;
    }

    public void setComA025001(Boolean comA025001) {
        this.comA025001 = comA025001;
    }

    public Boolean getComA026001() {
        return comA026001;
    }

    public void setComA026001(Boolean comA026001) {
        this.comA026001 = comA026001;
    }

    public Boolean getComA027001() {
        return comA027001;
    }

    public void setComA027001(Boolean comA027001) {
        this.comA027001 = comA027001;
    }

    public String getComA028001() {
        return comA028001;
    }

    public void setComA028001(String comA028001) {
        this.comA028001 = comA028001;
    }

    public Boolean getComB001001() {
        return comB001001;
    }

    public void setComB001001(Boolean comB001001) {
        this.comB001001 = comB001001;
    }

    public Boolean getComB002001() {
        return comB002001;
    }

    public void setComB002001(Boolean comB002001) {
        this.comB002001 = comB002001;
    }

    public Boolean getComB003001() {
        return comB003001;
    }

    public void setComB003001(Boolean comB003001) {
        this.comB003001 = comB003001;
    }

    public Integer getComB004001() {
        return comB004001;
    }

    public void setComB004001(Integer comB004001) {
        this.comB004001 = comB004001;
    }

    public Integer getComB005001() {
        return comB005001;
    }

    public void setComB005001(Integer comB005001) {
        this.comB005001 = comB005001;
    }

    public Integer getComB006001() {
        return comB006001;
    }

    public void setComB006001(Integer comB006001) {
        this.comB006001 = comB006001;
    }

    public Integer getComB007001() {
        return comB007001;
    }

    public void setComB007001(Integer comB007001) {
        this.comB007001 = comB007001;
    }

    public Integer getComB008001() {
        return comB008001;
    }

    public void setComB008001(Integer comB008001) {
        this.comB008001 = comB008001;
    }

    public Integer getComB009001() {
        return comB009001;
    }

    public void setComB009001(Integer comB009001) {
        this.comB009001 = comB009001;
    }

    public Integer getComB010001() {
        return comB010001;
    }

    public void setComB010001(Integer comB010001) {
        this.comB010001 = comB010001;
    }

    public Integer getComB011001() {
        return comB011001;
    }

    public void setComB011001(Integer comB011001) {
        this.comB011001 = comB011001;
    }

    public Integer getComB012001() {
        return comB012001;
    }

    public void setComB012001(Integer comB012001) {
        this.comB012001 = comB012001;
    }

    public Integer getComB013001() {
        return comB013001;
    }

    public void setComB013001(Integer comB013001) {
        this.comB013001 = comB013001;
    }

    public Integer getComB014001() {
        return comB014001;
    }

    public void setComB014001(Integer comB014001) {
        this.comB014001 = comB014001;
    }

    public Integer getComB015001() {
        return comB015001;
    }

    public void setComB015001(Integer comB015001) {
        this.comB015001 = comB015001;
    }

    public Integer getComB016001() {
        return comB016001;
    }

    public void setComB016001(Integer comB016001) {
        this.comB016001 = comB016001;
    }

    public Integer getComB017001() {
        return comB017001;
    }

    public void setComB017001(Integer comB017001) {
        this.comB017001 = comB017001;
    }

    public Integer getComB018001() {
        return comB018001;
    }

    public void setComB018001(Integer comB018001) {
        this.comB018001 = comB018001;
    }

    public Integer getComB019001() {
        return comB019001;
    }

    public void setComB019001(Integer comB019001) {
        this.comB019001 = comB019001;
    }

    public Integer getComB020001() {
        return comB020001;
    }

    public void setComB020001(Integer comB020001) {
        this.comB020001 = comB020001;
    }

    public Boolean getComB021001() {
        return comB021001;
    }

    public void setComB021001(Boolean comB021001) {
        this.comB021001 = comB021001;
    }

    public Integer getComB022001() {
        return comB022001;
    }

    public void setComB022001(Integer comB022001) {
        this.comB022001 = comB022001;
    }

    public Integer getComB023001() {
        return comB023001;
    }

    public void setComB023001(Integer comB023001) {
        this.comB023001 = comB023001;
    }

    public Boolean getComC001001() {
        return comC001001;
    }

    public void setComC001001(Boolean comC001001) {
        this.comC001001 = comC001001;
    }

    public String getComC002001() {
        return comC002001;
    }

    public void setComC002001(String comC002001) {
        this.comC002001 = comC002001;
    }

    public Integer getComC003001() {
        return comC003001;
    }

    public void setComC003001(Integer comC003001) {
        this.comC003001 = comC003001;
    }

    public Boolean getComD001001() {
        return comD001001;
    }

    public void setComD001001(Boolean comD001001) {
        this.comD001001 = comD001001;
    }

    public String getComD002001() {
        return comD002001;
    }

    public void setComD002001(String comD002001) {
        this.comD002001 = comD002001;
    }

    public String getComD003001() {
        return comD003001;
    }

    public void setComD003001(String comD003001) {
        this.comD003001 = comD003001;
    }

    public String getComF001001() {
        return comF001001;
    }

    public void setComF001001(String comF001001) {
        this.comF001001 = comF001001;
    }

    public String getUrlImagem() {
        return urlImagem;
    }

    public void setUrlImagem(String urlImagem) {
        this.urlImagem = urlImagem;
    }

    public String getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(String classificacao) {
        this.classificacao = classificacao;
    }

    public String getPaisSigla() {
        return paisSigla;
    }

    public void setPaisSigla(String paisSigla) {
        this.paisSigla = paisSigla;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    public Integer getComA011001() {
        return comA011001;
    }

    public void setComA011001(Integer comA011001) {
        this.comA011001 = comA011001;
    }

    public Integer getComA012001() {
        return comA012001;
    }

    public void setComA012001(Integer comA012001) {
        this.comA012001 = comA012001;
    }

    public Integer getComA013001() {
        return comA013001;
    }

    public void setComA013001(Integer comA013001) {
        this.comA013001 = comA013001;
    }

    @XmlTransient
    public List<TbIntervSistSaneMelhor> getTbIntervSistSaneMelhorList() {
        return tbIntervSistSaneMelhorList;
    }

    public void setTbIntervSistSaneMelhorList(List<TbIntervSistSaneMelhor> tbIntervSistSaneMelhorList) {
        this.tbIntervSistSaneMelhorList = tbIntervSistSaneMelhorList;
    }

    @XmlTransient
    public List<TbIntervMelhoria> getTbIntervMelhoriaList() {
        return tbIntervMelhoriaList;
    }

    public void setTbIntervMelhoriaList(List<TbIntervMelhoria> tbIntervMelhoriaList) {
        this.tbIntervMelhoriaList = tbIntervMelhoriaList;
    }

    @XmlTransient
    public List<TbCentroEducativo> getTbCentroEducativoList() {
        return tbCentroEducativoList;
    }

    public void setTbCentroEducativoList(List<TbCentroEducativo> tbCentroEducativoList) {
        this.tbCentroEducativoList = tbCentroEducativoList;
    }

    @XmlTransient
    public List<TbGrpDomicilio> getTbGrpDomicilioList() {
        return tbGrpDomicilioList;
    }

    public void setTbGrpDomicilioList(List<TbGrpDomicilio> tbGrpDomicilioList) {
        this.tbGrpDomicilioList = tbGrpDomicilioList;
    }

    @XmlTransient
    public List<TbIntervSistSaneNaoMelhor> getTbIntervSistSaneNaoMelhorList() {
        return tbIntervSistSaneNaoMelhorList;
    }

    public void setTbIntervSistSaneNaoMelhorList(List<TbIntervSistSaneNaoMelhor> tbIntervSistSaneNaoMelhorList) {
        this.tbIntervSistSaneNaoMelhorList = tbIntervSistSaneNaoMelhorList;
    }

    @XmlTransient
    public List<TbCentroSaude> getTbCentroSaudeList() {
        return tbCentroSaudeList;
    }

    public void setTbCentroSaudeList(List<TbCentroSaude> tbCentroSaudeList) {
        this.tbCentroSaudeList = tbCentroSaudeList;
    }

    @XmlTransient
    public List<TbIntervNovoSistAgua> getTbIntervNovoSistAguaList() {
        return tbIntervNovoSistAguaList;
    }

    public void setTbIntervNovoSistAguaList(List<TbIntervNovoSistAgua> tbIntervNovoSistAguaList) {
        this.tbIntervNovoSistAguaList = tbIntervNovoSistAguaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (comunSeq != null ? comunSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbComunidade)) {
            return false;
        }
        TbComunidade other = (TbComunidade) object;
        if ((this.comunSeq == null && other.comunSeq != null) || (this.comunSeq != null && !this.comunSeq.equals(other.comunSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbComunidade[ comunSeq=" + comunSeq + " ]";
    }
    
}
