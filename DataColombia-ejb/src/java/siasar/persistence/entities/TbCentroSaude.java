/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_centro_saude")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbCentroSaude.findAll", query = "SELECT t FROM TbCentroSaude t")
    , @NamedQuery(name = "TbCentroSaude.findByCensaSeq", query = "SELECT t FROM TbCentroSaude t WHERE t.censaSeq = :censaSeq")
    , @NamedQuery(name = "TbCentroSaude.findByComD004001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD004001 = :comD004001")
    , @NamedQuery(name = "TbCentroSaude.findByComD005001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD005001 = :comD005001")
    , @NamedQuery(name = "TbCentroSaude.findByComD006001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD006001 = :comD006001")
    , @NamedQuery(name = "TbCentroSaude.findByComD007001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD007001 = :comD007001")
    , @NamedQuery(name = "TbCentroSaude.findByComD008001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD008001 = :comD008001")
    , @NamedQuery(name = "TbCentroSaude.findByComD009001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD009001 = :comD009001")
    , @NamedQuery(name = "TbCentroSaude.findByComD010001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD010001 = :comD010001")
    , @NamedQuery(name = "TbCentroSaude.findByComD012001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD012001 = :comD012001")
    , @NamedQuery(name = "TbCentroSaude.findByComD013001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD013001 = :comD013001")
    , @NamedQuery(name = "TbCentroSaude.findByComD014001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD014001 = :comD014001")
    , @NamedQuery(name = "TbCentroSaude.findByComD015001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD015001 = :comD015001")
    , @NamedQuery(name = "TbCentroSaude.findByComD016001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD016001 = :comD016001")
    , @NamedQuery(name = "TbCentroSaude.findByComD017001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD017001 = :comD017001")
    , @NamedQuery(name = "TbCentroSaude.findByComD018001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD018001 = :comD018001")
    , @NamedQuery(name = "TbCentroSaude.findByComD019001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD019001 = :comD019001")
    , @NamedQuery(name = "TbCentroSaude.findByComD020001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD020001 = :comD020001")
    , @NamedQuery(name = "TbCentroSaude.findByComD021001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD021001 = :comD021001")
    , @NamedQuery(name = "TbCentroSaude.findByComD022001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD022001 = :comD022001")
    , @NamedQuery(name = "TbCentroSaude.findByComD023001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD023001 = :comD023001")
    , @NamedQuery(name = "TbCentroSaude.findByComD024001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD024001 = :comD024001")
    , @NamedQuery(name = "TbCentroSaude.findByComD025001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD025001 = :comD025001")
    , @NamedQuery(name = "TbCentroSaude.findByComD026001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD026001 = :comD026001")
    , @NamedQuery(name = "TbCentroSaude.findByComD027001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD027001 = :comD027001")
    , @NamedQuery(name = "TbCentroSaude.findByComD028001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD028001 = :comD028001")
    , @NamedQuery(name = "TbCentroSaude.findByComD029001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD029001 = :comD029001")
    , @NamedQuery(name = "TbCentroSaude.findByComD030001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD030001 = :comD030001")
    , @NamedQuery(name = "TbCentroSaude.findByComD031001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD031001 = :comD031001")
    , @NamedQuery(name = "TbCentroSaude.findByComD032001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD032001 = :comD032001")
    , @NamedQuery(name = "TbCentroSaude.findByComD033001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD033001 = :comD033001")
    , @NamedQuery(name = "TbCentroSaude.findByComD034001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD034001 = :comD034001")
    , @NamedQuery(name = "TbCentroSaude.findByComD035001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD035001 = :comD035001")
    , @NamedQuery(name = "TbCentroSaude.findByComD036001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD036001 = :comD036001")
    , @NamedQuery(name = "TbCentroSaude.findByComD037001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD037001 = :comD037001")
    , @NamedQuery(name = "TbCentroSaude.findByComD038001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD038001 = :comD038001")
    , @NamedQuery(name = "TbCentroSaude.findByComD039001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD039001 = :comD039001")
    , @NamedQuery(name = "TbCentroSaude.findByComD040001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD040001 = :comD040001")
    , @NamedQuery(name = "TbCentroSaude.findByComD041001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD041001 = :comD041001")
    , @NamedQuery(name = "TbCentroSaude.findByComD042001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD042001 = :comD042001")
    , @NamedQuery(name = "TbCentroSaude.findByComD043001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD043001 = :comD043001")
    , @NamedQuery(name = "TbCentroSaude.findByComD044001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD044001 = :comD044001")
    , @NamedQuery(name = "TbCentroSaude.findByComD045001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD045001 = :comD045001")
    , @NamedQuery(name = "TbCentroSaude.findByComD046001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD046001 = :comD046001")
    , @NamedQuery(name = "TbCentroSaude.findByComD047001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD047001 = :comD047001")
    , @NamedQuery(name = "TbCentroSaude.findByComD048001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD048001 = :comD048001")
    , @NamedQuery(name = "TbCentroSaude.findByComD049001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD049001 = :comD049001")
    , @NamedQuery(name = "TbCentroSaude.findByComD050001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD050001 = :comD050001")
    , @NamedQuery(name = "TbCentroSaude.findByDateLastUpdate", query = "SELECT t FROM TbCentroSaude t WHERE t.dateLastUpdate = :dateLastUpdate")
    , @NamedQuery(name = "TbCentroSaude.findByComD011001", query = "SELECT t FROM TbCentroSaude t WHERE t.comD011001 = :comD011001")})
public class TbCentroSaude implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "censa_seq")
    private Long censaSeq;
    @Size(max = 2147483647)
    @Column(name = "com_d_004_001")
    private String comD004001;
    @Size(max = 255)
    @Column(name = "com_d_005_001")
    private String comD005001;
    @Column(name = "com_d_006_001")
    private Integer comD006001;
    @Size(max = 255)
    @Column(name = "com_d_007_001")
    private String comD007001;
    @Column(name = "com_d_008_001")
    private Integer comD008001;
    @Column(name = "com_d_009_001")
    private Integer comD009001;
    @Size(max = 255)
    @Column(name = "com_d_010_001")
    private String comD010001;
    @Size(max = 2147483647)
    @Column(name = "com_d_012_001")
    private String comD012001;
    @Column(name = "com_d_013_001")
    private Integer comD013001;
    @Size(max = 2147483647)
    @Column(name = "com_d_014_001")
    private String comD014001;
    @Column(name = "com_d_015_001")
    private Integer comD015001;
    @Column(name = "com_d_016_001")
    private Integer comD016001;
    @Column(name = "com_d_017_001")
    private Integer comD017001;
    @Column(name = "com_d_018_001")
    private Integer comD018001;
    @Column(name = "com_d_019_001")
    private Integer comD019001;
    @Column(name = "com_d_020_001")
    private Integer comD020001;
    @Column(name = "com_d_021_001")
    private Integer comD021001;
    @Column(name = "com_d_022_001")
    private Integer comD022001;
    @Column(name = "com_d_023_001")
    private Integer comD023001;
    @Column(name = "com_d_024_001")
    private Integer comD024001;
    @Column(name = "com_d_025_001")
    private Integer comD025001;
    @Column(name = "com_d_026_001")
    private Integer comD026001;
    @Column(name = "com_d_027_001")
    private Integer comD027001;
    @Column(name = "com_d_028_001")
    private Integer comD028001;
    @Column(name = "com_d_029_001")
    private Integer comD029001;
    @Column(name = "com_d_030_001")
    private Integer comD030001;
    @Column(name = "com_d_031_001")
    private Integer comD031001;
    @Column(name = "com_d_032_001")
    private Integer comD032001;
    @Column(name = "com_d_033_001")
    private Integer comD033001;
    @Column(name = "com_d_034_001")
    private Integer comD034001;
    @Column(name = "com_d_035_001")
    private Integer comD035001;
    @Column(name = "com_d_036_001")
    private Integer comD036001;
    @Column(name = "com_d_037_001")
    private Integer comD037001;
    @Column(name = "com_d_038_001")
    private Integer comD038001;
    @Column(name = "com_d_039_001")
    private Integer comD039001;
    @Column(name = "com_d_040_001")
    private Integer comD040001;
    @Column(name = "com_d_041_001")
    private Integer comD041001;
    @Column(name = "com_d_042_001")
    private Integer comD042001;
    @Column(name = "com_d_043_001")
    private Integer comD043001;
    @Column(name = "com_d_044_001")
    private Integer comD044001;
    @Column(name = "com_d_045_001")
    private Integer comD045001;
    @Column(name = "com_d_046_001")
    private Integer comD046001;
    @Column(name = "com_d_047_001")
    private Integer comD047001;
    @Column(name = "com_d_048_001")
    private Integer comD048001;
    @Column(name = "com_d_049_001")
    private Integer comD049001;
    @Column(name = "com_d_050_001")
    private Integer comD050001;
    @Column(name = "date_last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdate;
    @Column(name = "com_d_011_001")
    private Integer comD011001;
    @JoinColumn(name = "comun_seq", referencedColumnName = "comun_seq")
    @ManyToOne
    private TbComunidade comunSeq;
    @JoinColumn(name = "siste_seq", referencedColumnName = "siste_seq")
    @ManyToOne
    private TbSistema sisteSeq;

    public TbCentroSaude() {
    }

    public TbCentroSaude(Long censaSeq) {
        this.censaSeq = censaSeq;
    }

    public Long getCensaSeq() {
        return censaSeq;
    }

    public void setCensaSeq(Long censaSeq) {
        this.censaSeq = censaSeq;
    }

    public String getComD004001() {
        return comD004001;
    }

    public void setComD004001(String comD004001) {
        this.comD004001 = comD004001;
    }

    public String getComD005001() {
        return comD005001;
    }

    public void setComD005001(String comD005001) {
        this.comD005001 = comD005001;
    }

    public Integer getComD006001() {
        return comD006001;
    }

    public void setComD006001(Integer comD006001) {
        this.comD006001 = comD006001;
    }

    public String getComD007001() {
        return comD007001;
    }

    public void setComD007001(String comD007001) {
        this.comD007001 = comD007001;
    }

    public Integer getComD008001() {
        return comD008001;
    }

    public void setComD008001(Integer comD008001) {
        this.comD008001 = comD008001;
    }

    public Integer getComD009001() {
        return comD009001;
    }

    public void setComD009001(Integer comD009001) {
        this.comD009001 = comD009001;
    }

    public String getComD010001() {
        return comD010001;
    }

    public void setComD010001(String comD010001) {
        this.comD010001 = comD010001;
    }

    public String getComD012001() {
        return comD012001;
    }

    public void setComD012001(String comD012001) {
        this.comD012001 = comD012001;
    }

    public Integer getComD013001() {
        return comD013001;
    }

    public void setComD013001(Integer comD013001) {
        this.comD013001 = comD013001;
    }

    public String getComD014001() {
        return comD014001;
    }

    public void setComD014001(String comD014001) {
        this.comD014001 = comD014001;
    }

    public Integer getComD015001() {
        return comD015001;
    }

    public void setComD015001(Integer comD015001) {
        this.comD015001 = comD015001;
    }

    public Integer getComD016001() {
        return comD016001;
    }

    public void setComD016001(Integer comD016001) {
        this.comD016001 = comD016001;
    }

    public Integer getComD017001() {
        return comD017001;
    }

    public void setComD017001(Integer comD017001) {
        this.comD017001 = comD017001;
    }

    public Integer getComD018001() {
        return comD018001;
    }

    public void setComD018001(Integer comD018001) {
        this.comD018001 = comD018001;
    }

    public Integer getComD019001() {
        return comD019001;
    }

    public void setComD019001(Integer comD019001) {
        this.comD019001 = comD019001;
    }

    public Integer getComD020001() {
        return comD020001;
    }

    public void setComD020001(Integer comD020001) {
        this.comD020001 = comD020001;
    }

    public Integer getComD021001() {
        return comD021001;
    }

    public void setComD021001(Integer comD021001) {
        this.comD021001 = comD021001;
    }

    public Integer getComD022001() {
        return comD022001;
    }

    public void setComD022001(Integer comD022001) {
        this.comD022001 = comD022001;
    }

    public Integer getComD023001() {
        return comD023001;
    }

    public void setComD023001(Integer comD023001) {
        this.comD023001 = comD023001;
    }

    public Integer getComD024001() {
        return comD024001;
    }

    public void setComD024001(Integer comD024001) {
        this.comD024001 = comD024001;
    }

    public Integer getComD025001() {
        return comD025001;
    }

    public void setComD025001(Integer comD025001) {
        this.comD025001 = comD025001;
    }

    public Integer getComD026001() {
        return comD026001;
    }

    public void setComD026001(Integer comD026001) {
        this.comD026001 = comD026001;
    }

    public Integer getComD027001() {
        return comD027001;
    }

    public void setComD027001(Integer comD027001) {
        this.comD027001 = comD027001;
    }

    public Integer getComD028001() {
        return comD028001;
    }

    public void setComD028001(Integer comD028001) {
        this.comD028001 = comD028001;
    }

    public Integer getComD029001() {
        return comD029001;
    }

    public void setComD029001(Integer comD029001) {
        this.comD029001 = comD029001;
    }

    public Integer getComD030001() {
        return comD030001;
    }

    public void setComD030001(Integer comD030001) {
        this.comD030001 = comD030001;
    }

    public Integer getComD031001() {
        return comD031001;
    }

    public void setComD031001(Integer comD031001) {
        this.comD031001 = comD031001;
    }

    public Integer getComD032001() {
        return comD032001;
    }

    public void setComD032001(Integer comD032001) {
        this.comD032001 = comD032001;
    }

    public Integer getComD033001() {
        return comD033001;
    }

    public void setComD033001(Integer comD033001) {
        this.comD033001 = comD033001;
    }

    public Integer getComD034001() {
        return comD034001;
    }

    public void setComD034001(Integer comD034001) {
        this.comD034001 = comD034001;
    }

    public Integer getComD035001() {
        return comD035001;
    }

    public void setComD035001(Integer comD035001) {
        this.comD035001 = comD035001;
    }

    public Integer getComD036001() {
        return comD036001;
    }

    public void setComD036001(Integer comD036001) {
        this.comD036001 = comD036001;
    }

    public Integer getComD037001() {
        return comD037001;
    }

    public void setComD037001(Integer comD037001) {
        this.comD037001 = comD037001;
    }

    public Integer getComD038001() {
        return comD038001;
    }

    public void setComD038001(Integer comD038001) {
        this.comD038001 = comD038001;
    }

    public Integer getComD039001() {
        return comD039001;
    }

    public void setComD039001(Integer comD039001) {
        this.comD039001 = comD039001;
    }

    public Integer getComD040001() {
        return comD040001;
    }

    public void setComD040001(Integer comD040001) {
        this.comD040001 = comD040001;
    }

    public Integer getComD041001() {
        return comD041001;
    }

    public void setComD041001(Integer comD041001) {
        this.comD041001 = comD041001;
    }

    public Integer getComD042001() {
        return comD042001;
    }

    public void setComD042001(Integer comD042001) {
        this.comD042001 = comD042001;
    }

    public Integer getComD043001() {
        return comD043001;
    }

    public void setComD043001(Integer comD043001) {
        this.comD043001 = comD043001;
    }

    public Integer getComD044001() {
        return comD044001;
    }

    public void setComD044001(Integer comD044001) {
        this.comD044001 = comD044001;
    }

    public Integer getComD045001() {
        return comD045001;
    }

    public void setComD045001(Integer comD045001) {
        this.comD045001 = comD045001;
    }

    public Integer getComD046001() {
        return comD046001;
    }

    public void setComD046001(Integer comD046001) {
        this.comD046001 = comD046001;
    }

    public Integer getComD047001() {
        return comD047001;
    }

    public void setComD047001(Integer comD047001) {
        this.comD047001 = comD047001;
    }

    public Integer getComD048001() {
        return comD048001;
    }

    public void setComD048001(Integer comD048001) {
        this.comD048001 = comD048001;
    }

    public Integer getComD049001() {
        return comD049001;
    }

    public void setComD049001(Integer comD049001) {
        this.comD049001 = comD049001;
    }

    public Integer getComD050001() {
        return comD050001;
    }

    public void setComD050001(Integer comD050001) {
        this.comD050001 = comD050001;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    public Integer getComD011001() {
        return comD011001;
    }

    public void setComD011001(Integer comD011001) {
        this.comD011001 = comD011001;
    }

    public TbComunidade getComunSeq() {
        return comunSeq;
    }

    public void setComunSeq(TbComunidade comunSeq) {
        this.comunSeq = comunSeq;
    }

    public TbSistema getSisteSeq() {
        return sisteSeq;
    }

    public void setSisteSeq(TbSistema sisteSeq) {
        this.sisteSeq = sisteSeq;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (censaSeq != null ? censaSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbCentroSaude)) {
            return false;
        }
        TbCentroSaude other = (TbCentroSaude) object;
        if ((this.censaSeq == null && other.censaSeq != null) || (this.censaSeq != null && !this.censaSeq.equals(other.censaSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbCentroSaude[ censaSeq=" + censaSeq + " ]";
    }
    
}
