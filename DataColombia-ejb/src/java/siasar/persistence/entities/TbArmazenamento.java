/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_armazenamento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbArmazenamento.findAll", query = "SELECT t FROM TbArmazenamento t")
    , @NamedQuery(name = "TbArmazenamento.findByArmazSeq", query = "SELECT t FROM TbArmazenamento t WHERE t.armazSeq = :armazSeq")
    , @NamedQuery(name = "TbArmazenamento.findBySysE001001", query = "SELECT t FROM TbArmazenamento t WHERE t.sysE001001 = :sysE001001")
    , @NamedQuery(name = "TbArmazenamento.findBySysE002001", query = "SELECT t FROM TbArmazenamento t WHERE t.sysE002001 = :sysE002001")
    , @NamedQuery(name = "TbArmazenamento.findBySysE003001", query = "SELECT t FROM TbArmazenamento t WHERE t.sysE003001 = :sysE003001")
    , @NamedQuery(name = "TbArmazenamento.findBySysE004001", query = "SELECT t FROM TbArmazenamento t WHERE t.sysE004001 = :sysE004001")
    , @NamedQuery(name = "TbArmazenamento.findBySysE005001", query = "SELECT t FROM TbArmazenamento t WHERE t.sysE005001 = :sysE005001")
    , @NamedQuery(name = "TbArmazenamento.findBySysE006001", query = "SELECT t FROM TbArmazenamento t WHERE t.sysE006001 = :sysE006001")
    , @NamedQuery(name = "TbArmazenamento.findBySysE007001", query = "SELECT t FROM TbArmazenamento t WHERE t.sysE007001 = :sysE007001")
    , @NamedQuery(name = "TbArmazenamento.findBySysE008001", query = "SELECT t FROM TbArmazenamento t WHERE t.sysE008001 = :sysE008001")
    , @NamedQuery(name = "TbArmazenamento.findBySysE009001", query = "SELECT t FROM TbArmazenamento t WHERE t.sysE009001 = :sysE009001")
    , @NamedQuery(name = "TbArmazenamento.findBySysE010001", query = "SELECT t FROM TbArmazenamento t WHERE t.sysE010001 = :sysE010001")
    , @NamedQuery(name = "TbArmazenamento.findByDateLastUpdate", query = "SELECT t FROM TbArmazenamento t WHERE t.dateLastUpdate = :dateLastUpdate")})
public class TbArmazenamento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "armaz_seq")
    private Long armazSeq;
    @Size(max = 100)
    @Column(name = "sys_e_001_001")
    private String sysE001001;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "sys_e_002_001")
    private BigDecimal sysE002001;
    @Column(name = "sys_e_003_001")
    private Integer sysE003001;
    @Column(name = "sys_e_004_001")
    private BigDecimal sysE004001;
    @Column(name = "sys_e_005_001")
    private Integer sysE005001;
    @Column(name = "sys_e_006_001")
    private Double sysE006001;
    @Column(name = "sys_e_007_001")
    private Double sysE007001;
    @Column(name = "sys_e_008_001")
    private Double sysE008001;
    @Column(name = "sys_e_009_001")
    private Integer sysE009001;
    @Size(max = 2147483647)
    @Column(name = "sys_e_010_001")
    private String sysE010001;
    @Column(name = "date_last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdate;
    @JoinColumn(name = "siste_seq", referencedColumnName = "siste_seq")
    @ManyToOne
    private TbSistema sisteSeq;

    public TbArmazenamento() {
    }

    public TbArmazenamento(Long armazSeq) {
        this.armazSeq = armazSeq;
    }

    public Long getArmazSeq() {
        return armazSeq;
    }

    public void setArmazSeq(Long armazSeq) {
        this.armazSeq = armazSeq;
    }

    public String getSysE001001() {
        return sysE001001;
    }

    public void setSysE001001(String sysE001001) {
        this.sysE001001 = sysE001001;
    }

    public BigDecimal getSysE002001() {
        return sysE002001;
    }

    public void setSysE002001(BigDecimal sysE002001) {
        this.sysE002001 = sysE002001;
    }

    public Integer getSysE003001() {
        return sysE003001;
    }

    public void setSysE003001(Integer sysE003001) {
        this.sysE003001 = sysE003001;
    }

    public BigDecimal getSysE004001() {
        return sysE004001;
    }

    public void setSysE004001(BigDecimal sysE004001) {
        this.sysE004001 = sysE004001;
    }

    public Integer getSysE005001() {
        return sysE005001;
    }

    public void setSysE005001(Integer sysE005001) {
        this.sysE005001 = sysE005001;
    }

    public Double getSysE006001() {
        return sysE006001;
    }

    public void setSysE006001(Double sysE006001) {
        this.sysE006001 = sysE006001;
    }

    public Double getSysE007001() {
        return sysE007001;
    }

    public void setSysE007001(Double sysE007001) {
        this.sysE007001 = sysE007001;
    }

    public Double getSysE008001() {
        return sysE008001;
    }

    public void setSysE008001(Double sysE008001) {
        this.sysE008001 = sysE008001;
    }

    public Integer getSysE009001() {
        return sysE009001;
    }

    public void setSysE009001(Integer sysE009001) {
        this.sysE009001 = sysE009001;
    }

    public String getSysE010001() {
        return sysE010001;
    }

    public void setSysE010001(String sysE010001) {
        this.sysE010001 = sysE010001;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    public TbSistema getSisteSeq() {
        return sisteSeq;
    }

    public void setSisteSeq(TbSistema sisteSeq) {
        this.sisteSeq = sisteSeq;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (armazSeq != null ? armazSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbArmazenamento)) {
            return false;
        }
        TbArmazenamento other = (TbArmazenamento) object;
        if ((this.armazSeq == null && other.armazSeq != null) || (this.armazSeq != null && !this.armazSeq.equals(other.armazSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbArmazenamento[ armazSeq=" + armazSeq + " ]";
    }
    
}
