/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_fonte_financ")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbFonteFinanc.findAll", query = "SELECT t FROM TbFonteFinanc t")
    , @NamedQuery(name = "TbFonteFinanc.findByFonteSeq", query = "SELECT t FROM TbFonteFinanc t WHERE t.fonteSeq = :fonteSeq")
    , @NamedQuery(name = "TbFonteFinanc.findBySysA014001", query = "SELECT t FROM TbFonteFinanc t WHERE t.sysA014001 = :sysA014001")
    , @NamedQuery(name = "TbFonteFinanc.findBySysA015001", query = "SELECT t FROM TbFonteFinanc t WHERE t.sysA015001 = :sysA015001")
    , @NamedQuery(name = "TbFonteFinanc.findBySysA016001", query = "SELECT t FROM TbFonteFinanc t WHERE t.sysA016001 = :sysA016001")
    , @NamedQuery(name = "TbFonteFinanc.findBySysA017001", query = "SELECT t FROM TbFonteFinanc t WHERE t.sysA017001 = :sysA017001")
    , @NamedQuery(name = "TbFonteFinanc.findByDateLastUpdate", query = "SELECT t FROM TbFonteFinanc t WHERE t.dateLastUpdate = :dateLastUpdate")})
public class TbFonteFinanc implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "fonte_seq")
    private Long fonteSeq;
    @Column(name = "sys_a_014_001")
    private Integer sysA014001;
    @Column(name = "sys_a_015_001")
    private Integer sysA015001;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "sys_a_016_001")
    private BigDecimal sysA016001;
    @Column(name = "sys_a_017_001")
    private Integer sysA017001;
    @Column(name = "date_last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdate;
    @JoinColumn(name = "siste_seq", referencedColumnName = "siste_seq")
    @ManyToOne
    private TbSistema sisteSeq;

    public TbFonteFinanc() {
    }

    public TbFonteFinanc(Long fonteSeq) {
        this.fonteSeq = fonteSeq;
    }

    public Long getFonteSeq() {
        return fonteSeq;
    }

    public void setFonteSeq(Long fonteSeq) {
        this.fonteSeq = fonteSeq;
    }

    public Integer getSysA014001() {
        return sysA014001;
    }

    public void setSysA014001(Integer sysA014001) {
        this.sysA014001 = sysA014001;
    }

    public Integer getSysA015001() {
        return sysA015001;
    }

    public void setSysA015001(Integer sysA015001) {
        this.sysA015001 = sysA015001;
    }

    public BigDecimal getSysA016001() {
        return sysA016001;
    }

    public void setSysA016001(BigDecimal sysA016001) {
        this.sysA016001 = sysA016001;
    }

    public Integer getSysA017001() {
        return sysA017001;
    }

    public void setSysA017001(Integer sysA017001) {
        this.sysA017001 = sysA017001;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    public TbSistema getSisteSeq() {
        return sisteSeq;
    }

    public void setSisteSeq(TbSistema sisteSeq) {
        this.sisteSeq = sisteSeq;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fonteSeq != null ? fonteSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbFonteFinanc)) {
            return false;
        }
        TbFonteFinanc other = (TbFonteFinanc) object;
        if ((this.fonteSeq == null && other.fonteSeq != null) || (this.fonteSeq != null && !this.fonteSeq.equals(other.fonteSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbFonteFinanc[ fonteSeq=" + fonteSeq + " ]";
    }
    
}
