/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_interv_novo_sist_agua")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbIntervNovoSistAgua.findAll", query = "SELECT t FROM TbIntervNovoSistAgua t")
    , @NamedQuery(name = "TbIntervNovoSistAgua.findByIntnsaSeq", query = "SELECT t FROM TbIntervNovoSistAgua t WHERE t.intnsaSeq = :intnsaSeq")
    , @NamedQuery(name = "TbIntervNovoSistAgua.findByIntnsaFonte", query = "SELECT t FROM TbIntervNovoSistAgua t WHERE t.intnsaFonte = :intnsaFonte")
    , @NamedQuery(name = "TbIntervNovoSistAgua.findByDateLastUpdate", query = "SELECT t FROM TbIntervNovoSistAgua t WHERE t.dateLastUpdate = :dateLastUpdate")})
public class TbIntervNovoSistAgua implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "intnsa_seq")
    private Long intnsaSeq;
    @Size(max = 255)
    @Column(name = "intnsa_fonte")
    private String intnsaFonte;
    @Column(name = "date_last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdate;
    @JoinColumn(name = "comun_seq", referencedColumnName = "comun_seq")
    @ManyToOne
    private TbComunidade comunSeq;
    @JoinColumn(name = "taxt_seq", referencedColumnName = "taxt_seq")
    @ManyToOne
    private TbTaxoTermo taxtSeq;

    public TbIntervNovoSistAgua() {
    }

    public TbIntervNovoSistAgua(Long intnsaSeq) {
        this.intnsaSeq = intnsaSeq;
    }

    public Long getIntnsaSeq() {
        return intnsaSeq;
    }

    public void setIntnsaSeq(Long intnsaSeq) {
        this.intnsaSeq = intnsaSeq;
    }

    public String getIntnsaFonte() {
        return intnsaFonte;
    }

    public void setIntnsaFonte(String intnsaFonte) {
        this.intnsaFonte = intnsaFonte;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    public TbComunidade getComunSeq() {
        return comunSeq;
    }

    public void setComunSeq(TbComunidade comunSeq) {
        this.comunSeq = comunSeq;
    }

    public TbTaxoTermo getTaxtSeq() {
        return taxtSeq;
    }

    public void setTaxtSeq(TbTaxoTermo taxtSeq) {
        this.taxtSeq = taxtSeq;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (intnsaSeq != null ? intnsaSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbIntervNovoSistAgua)) {
            return false;
        }
        TbIntervNovoSistAgua other = (TbIntervNovoSistAgua) object;
        if ((this.intnsaSeq == null && other.intnsaSeq != null) || (this.intnsaSeq != null && !this.intnsaSeq.equals(other.intnsaSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbIntervNovoSistAgua[ intnsaSeq=" + intnsaSeq + " ]";
    }
    
}
