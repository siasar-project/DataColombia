/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "django_migrations")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DjangoMigrations.findAll", query = "SELECT d FROM DjangoMigrations d")
    , @NamedQuery(name = "DjangoMigrations.findById", query = "SELECT d FROM DjangoMigrations d WHERE d.id = :id")
    , @NamedQuery(name = "DjangoMigrations.findByApp", query = "SELECT d FROM DjangoMigrations d WHERE d.app = :app")
    , @NamedQuery(name = "DjangoMigrations.findByName", query = "SELECT d FROM DjangoMigrations d WHERE d.name = :name")
    , @NamedQuery(name = "DjangoMigrations.findByApplied", query = "SELECT d FROM DjangoMigrations d WHERE d.applied = :applied")})
public class DjangoMigrations implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "app")
    private String app;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "applied")
    @Temporal(TemporalType.TIMESTAMP)
    private Date applied;

    public DjangoMigrations() {
    }

    public DjangoMigrations(Integer id) {
        this.id = id;
    }

    public DjangoMigrations(Integer id, String app, String name, Date applied) {
        this.id = id;
        this.app = app;
        this.name = name;
        this.applied = applied;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getApplied() {
        return applied;
    }

    public void setApplied(Date applied) {
        this.applied = applied;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DjangoMigrations)) {
            return false;
        }
        DjangoMigrations other = (DjangoMigrations) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.DjangoMigrations[ id=" + id + " ]";
    }
    
}
