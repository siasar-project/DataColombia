/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_interv_sist_sane_nao_melhor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbIntervSistSaneNaoMelhor.findAll", query = "SELECT t FROM TbIntervSistSaneNaoMelhor t")
    , @NamedQuery(name = "TbIntervSistSaneNaoMelhor.findByIntssnmSeq", query = "SELECT t FROM TbIntervSistSaneNaoMelhor t WHERE t.intssnmSeq = :intssnmSeq")
    , @NamedQuery(name = "TbIntervSistSaneNaoMelhor.findByIntssnmFonte", query = "SELECT t FROM TbIntervSistSaneNaoMelhor t WHERE t.intssnmFonte = :intssnmFonte")
    , @NamedQuery(name = "TbIntervSistSaneNaoMelhor.findByDateLastUpdate", query = "SELECT t FROM TbIntervSistSaneNaoMelhor t WHERE t.dateLastUpdate = :dateLastUpdate")})
public class TbIntervSistSaneNaoMelhor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "intssnm_seq")
    private Long intssnmSeq;
    @Size(max = 255)
    @Column(name = "intssnm_fonte")
    private String intssnmFonte;
    @Column(name = "date_last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdate;
    @JoinColumn(name = "comun_seq", referencedColumnName = "comun_seq")
    @ManyToOne
    private TbComunidade comunSeq;
    @JoinColumn(name = "taxt_seq", referencedColumnName = "taxt_seq")
    @ManyToOne
    private TbTaxoTermo taxtSeq;

    public TbIntervSistSaneNaoMelhor() {
    }

    public TbIntervSistSaneNaoMelhor(Long intssnmSeq) {
        this.intssnmSeq = intssnmSeq;
    }

    public Long getIntssnmSeq() {
        return intssnmSeq;
    }

    public void setIntssnmSeq(Long intssnmSeq) {
        this.intssnmSeq = intssnmSeq;
    }

    public String getIntssnmFonte() {
        return intssnmFonte;
    }

    public void setIntssnmFonte(String intssnmFonte) {
        this.intssnmFonte = intssnmFonte;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    public TbComunidade getComunSeq() {
        return comunSeq;
    }

    public void setComunSeq(TbComunidade comunSeq) {
        this.comunSeq = comunSeq;
    }

    public TbTaxoTermo getTaxtSeq() {
        return taxtSeq;
    }

    public void setTaxtSeq(TbTaxoTermo taxtSeq) {
        this.taxtSeq = taxtSeq;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (intssnmSeq != null ? intssnmSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbIntervSistSaneNaoMelhor)) {
            return false;
        }
        TbIntervSistSaneNaoMelhor other = (TbIntervSistSaneNaoMelhor) object;
        if ((this.intssnmSeq == null && other.intssnmSeq != null) || (this.intssnmSeq != null && !this.intssnmSeq.equals(other.intssnmSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbIntervSistSaneNaoMelhor[ intssnmSeq=" + intssnmSeq + " ]";
    }
    
}
