/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_grp_domicilio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbGrpDomicilio.findAll", query = "SELECT t FROM TbGrpDomicilio t")
    , @NamedQuery(name = "TbGrpDomicilio.findByGrpdoSeq", query = "SELECT t FROM TbGrpDomicilio t WHERE t.grpdoSeq = :grpdoSeq")
    , @NamedQuery(name = "TbGrpDomicilio.findByPrserSeq", query = "SELECT t FROM TbGrpDomicilio t WHERE t.prserSeq = :prserSeq")
    , @NamedQuery(name = "TbGrpDomicilio.findBySisteSeq", query = "SELECT t FROM TbGrpDomicilio t WHERE t.sisteSeq = :sisteSeq")
    , @NamedQuery(name = "TbGrpDomicilio.findByGrpdoNumDomicAtend", query = "SELECT t FROM TbGrpDomicilio t WHERE t.grpdoNumDomicAtend = :grpdoNumDomicAtend")
    , @NamedQuery(name = "TbGrpDomicilio.findByDateLastUpdate", query = "SELECT t FROM TbGrpDomicilio t WHERE t.dateLastUpdate = :dateLastUpdate")
    , @NamedQuery(name = "TbGrpDomicilio.findByComA022001", query = "SELECT t FROM TbGrpDomicilio t WHERE t.comA022001 = :comA022001")})
public class TbGrpDomicilio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "grpdo_seq")
    private Long grpdoSeq;
    @Column(name = "prser_seq")
    private BigInteger prserSeq;
    @Column(name = "siste_seq")
    private BigInteger sisteSeq;
    @Column(name = "grpdo_num_domic_atend")
    private Integer grpdoNumDomicAtend;
    @Column(name = "date_last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdate;
    @Column(name = "com_a_022_001")
    private Integer comA022001;
    @JoinColumn(name = "comun_seq", referencedColumnName = "comun_seq")
    @ManyToOne
    private TbComunidade comunSeq;

    public TbGrpDomicilio() {
    }

    public TbGrpDomicilio(Long grpdoSeq) {
        this.grpdoSeq = grpdoSeq;
    }

    public Long getGrpdoSeq() {
        return grpdoSeq;
    }

    public void setGrpdoSeq(Long grpdoSeq) {
        this.grpdoSeq = grpdoSeq;
    }

    public BigInteger getPrserSeq() {
        return prserSeq;
    }

    public void setPrserSeq(BigInteger prserSeq) {
        this.prserSeq = prserSeq;
    }

    public BigInteger getSisteSeq() {
        return sisteSeq;
    }

    public void setSisteSeq(BigInteger sisteSeq) {
        this.sisteSeq = sisteSeq;
    }

    public Integer getGrpdoNumDomicAtend() {
        return grpdoNumDomicAtend;
    }

    public void setGrpdoNumDomicAtend(Integer grpdoNumDomicAtend) {
        this.grpdoNumDomicAtend = grpdoNumDomicAtend;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    public Integer getComA022001() {
        return comA022001;
    }

    public void setComA022001(Integer comA022001) {
        this.comA022001 = comA022001;
    }

    public TbComunidade getComunSeq() {
        return comunSeq;
    }

    public void setComunSeq(TbComunidade comunSeq) {
        this.comunSeq = comunSeq;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (grpdoSeq != null ? grpdoSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbGrpDomicilio)) {
            return false;
        }
        TbGrpDomicilio other = (TbGrpDomicilio) object;
        if ((this.grpdoSeq == null && other.grpdoSeq != null) || (this.grpdoSeq != null && !this.grpdoSeq.equals(other.grpdoSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbGrpDomicilio[ grpdoSeq=" + grpdoSeq + " ]";
    }
    
}
