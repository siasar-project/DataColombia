/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_taxo_termo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbTaxoTermo.findAll", query = "SELECT t FROM TbTaxoTermo t")
    , @NamedQuery(name = "TbTaxoTermo.findByTaxtSeq", query = "SELECT t FROM TbTaxoTermo t WHERE t.taxtSeq = :taxtSeq")
    , @NamedQuery(name = "TbTaxoTermo.findByTaxtNome", query = "SELECT t FROM TbTaxoTermo t WHERE t.taxtNome = :taxtNome")
    , @NamedQuery(name = "TbTaxoTermo.findByTaxtDescricao", query = "SELECT t FROM TbTaxoTermo t WHERE t.taxtDescricao = :taxtDescricao")
    , @NamedQuery(name = "TbTaxoTermo.findByTaxtPais", query = "SELECT t FROM TbTaxoTermo t WHERE t.taxtPais = :taxtPais")
    , @NamedQuery(name = "TbTaxoTermo.findByDateLastUpdate", query = "SELECT t FROM TbTaxoTermo t WHERE t.dateLastUpdate = :dateLastUpdate")})
public class TbTaxoTermo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxt_seq")
    private Long taxtSeq;
    @Size(max = 255)
    @Column(name = "taxt_nome")
    private String taxtNome;
    @Size(max = 4000)
    @Column(name = "taxt_descricao")
    private String taxtDescricao;
    @Size(max = 2)
    @Column(name = "taxt_pais")
    private String taxtPais;
    @Column(name = "date_last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdate;
    @OneToMany(mappedBy = "taxtSeq")
    private List<TbIntervSistSaneMelhor> tbIntervSistSaneMelhorList;
    @OneToMany(mappedBy = "taxtSeq")
    private List<TbIntervMelhoria> tbIntervMelhoriaList;
    @OneToMany(mappedBy = "sepB010001")
    private List<TbDirTecnico> tbDirTecnicoList;
    @OneToMany(mappedBy = "sepB012001")
    private List<TbDirTecnico> tbDirTecnicoList1;
    @OneToMany(mappedBy = "sepB006001")
    private List<TbDirRepresentante> tbDirRepresentanteList;
    @OneToMany(mappedBy = "sepB008001")
    private List<TbDirRepresentante> tbDirRepresentanteList1;
    @JoinColumn(name = "taxv_seq", referencedColumnName = "taxv_seq")
    @ManyToOne
    private TbTaxoVocab taxvSeq;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tbTaxoTermo")
    private List<TbTipSistAbastecimento> tbTipSistAbastecimentoList;
    @OneToMany(mappedBy = "taxtSeq")
    private List<TbIntervSistSaneNaoMelhor> tbIntervSistSaneNaoMelhorList;
    @OneToMany(mappedBy = "taxtSeq")
    private List<TbIntervNovoSistAgua> tbIntervNovoSistAguaList;

    public TbTaxoTermo() {
    }

    public TbTaxoTermo(Long taxtSeq) {
        this.taxtSeq = taxtSeq;
    }

    public Long getTaxtSeq() {
        return taxtSeq;
    }

    public void setTaxtSeq(Long taxtSeq) {
        this.taxtSeq = taxtSeq;
    }

    public String getTaxtNome() {
        return taxtNome;
    }

    public void setTaxtNome(String taxtNome) {
        this.taxtNome = taxtNome;
    }

    public String getTaxtDescricao() {
        return taxtDescricao;
    }

    public void setTaxtDescricao(String taxtDescricao) {
        this.taxtDescricao = taxtDescricao;
    }

    public String getTaxtPais() {
        return taxtPais;
    }

    public void setTaxtPais(String taxtPais) {
        this.taxtPais = taxtPais;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    @XmlTransient
    public List<TbIntervSistSaneMelhor> getTbIntervSistSaneMelhorList() {
        return tbIntervSistSaneMelhorList;
    }

    public void setTbIntervSistSaneMelhorList(List<TbIntervSistSaneMelhor> tbIntervSistSaneMelhorList) {
        this.tbIntervSistSaneMelhorList = tbIntervSistSaneMelhorList;
    }

    @XmlTransient
    public List<TbIntervMelhoria> getTbIntervMelhoriaList() {
        return tbIntervMelhoriaList;
    }

    public void setTbIntervMelhoriaList(List<TbIntervMelhoria> tbIntervMelhoriaList) {
        this.tbIntervMelhoriaList = tbIntervMelhoriaList;
    }

    @XmlTransient
    public List<TbDirTecnico> getTbDirTecnicoList() {
        return tbDirTecnicoList;
    }

    public void setTbDirTecnicoList(List<TbDirTecnico> tbDirTecnicoList) {
        this.tbDirTecnicoList = tbDirTecnicoList;
    }

    @XmlTransient
    public List<TbDirTecnico> getTbDirTecnicoList1() {
        return tbDirTecnicoList1;
    }

    public void setTbDirTecnicoList1(List<TbDirTecnico> tbDirTecnicoList1) {
        this.tbDirTecnicoList1 = tbDirTecnicoList1;
    }

    @XmlTransient
    public List<TbDirRepresentante> getTbDirRepresentanteList() {
        return tbDirRepresentanteList;
    }

    public void setTbDirRepresentanteList(List<TbDirRepresentante> tbDirRepresentanteList) {
        this.tbDirRepresentanteList = tbDirRepresentanteList;
    }

    @XmlTransient
    public List<TbDirRepresentante> getTbDirRepresentanteList1() {
        return tbDirRepresentanteList1;
    }

    public void setTbDirRepresentanteList1(List<TbDirRepresentante> tbDirRepresentanteList1) {
        this.tbDirRepresentanteList1 = tbDirRepresentanteList1;
    }

    public TbTaxoVocab getTaxvSeq() {
        return taxvSeq;
    }

    public void setTaxvSeq(TbTaxoVocab taxvSeq) {
        this.taxvSeq = taxvSeq;
    }

    @XmlTransient
    public List<TbTipSistAbastecimento> getTbTipSistAbastecimentoList() {
        return tbTipSistAbastecimentoList;
    }

    public void setTbTipSistAbastecimentoList(List<TbTipSistAbastecimento> tbTipSistAbastecimentoList) {
        this.tbTipSistAbastecimentoList = tbTipSistAbastecimentoList;
    }

    @XmlTransient
    public List<TbIntervSistSaneNaoMelhor> getTbIntervSistSaneNaoMelhorList() {
        return tbIntervSistSaneNaoMelhorList;
    }

    public void setTbIntervSistSaneNaoMelhorList(List<TbIntervSistSaneNaoMelhor> tbIntervSistSaneNaoMelhorList) {
        this.tbIntervSistSaneNaoMelhorList = tbIntervSistSaneNaoMelhorList;
    }

    @XmlTransient
    public List<TbIntervNovoSistAgua> getTbIntervNovoSistAguaList() {
        return tbIntervNovoSistAguaList;
    }

    public void setTbIntervNovoSistAguaList(List<TbIntervNovoSistAgua> tbIntervNovoSistAguaList) {
        this.tbIntervNovoSistAguaList = tbIntervNovoSistAguaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (taxtSeq != null ? taxtSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbTaxoTermo)) {
            return false;
        }
        TbTaxoTermo other = (TbTaxoTermo) object;
        if ((this.taxtSeq == null && other.taxtSeq != null) || (this.taxtSeq != null && !this.taxtSeq.equals(other.taxtSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbTaxoTermo[ taxtSeq=" + taxtSeq + " ]";
    }
    
}
