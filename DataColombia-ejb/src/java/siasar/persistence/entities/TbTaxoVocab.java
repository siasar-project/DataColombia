/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_taxo_vocab")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbTaxoVocab.findAll", query = "SELECT t FROM TbTaxoVocab t")
    , @NamedQuery(name = "TbTaxoVocab.findByTaxvSeq", query = "SELECT t FROM TbTaxoVocab t WHERE t.taxvSeq = :taxvSeq")
    , @NamedQuery(name = "TbTaxoVocab.findByTaxvNome", query = "SELECT t FROM TbTaxoVocab t WHERE t.taxvNome = :taxvNome")
    , @NamedQuery(name = "TbTaxoVocab.findByTaxvDescricao", query = "SELECT t FROM TbTaxoVocab t WHERE t.taxvDescricao = :taxvDescricao")
    , @NamedQuery(name = "TbTaxoVocab.findByTaxvMachineName", query = "SELECT t FROM TbTaxoVocab t WHERE t.taxvMachineName = :taxvMachineName")})
public class TbTaxoVocab implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxv_seq")
    private Long taxvSeq;
    @Size(max = 255)
    @Column(name = "taxv_nome")
    private String taxvNome;
    @Size(max = 4000)
    @Column(name = "taxv_descricao")
    private String taxvDescricao;
    @Size(max = 255)
    @Column(name = "taxv_machine_name")
    private String taxvMachineName;
    @OneToMany(mappedBy = "taxvSeq")
    private List<TbTaxoTermo> tbTaxoTermoList;

    public TbTaxoVocab() {
    }

    public TbTaxoVocab(Long taxvSeq) {
        this.taxvSeq = taxvSeq;
    }

    public Long getTaxvSeq() {
        return taxvSeq;
    }

    public void setTaxvSeq(Long taxvSeq) {
        this.taxvSeq = taxvSeq;
    }

    public String getTaxvNome() {
        return taxvNome;
    }

    public void setTaxvNome(String taxvNome) {
        this.taxvNome = taxvNome;
    }

    public String getTaxvDescricao() {
        return taxvDescricao;
    }

    public void setTaxvDescricao(String taxvDescricao) {
        this.taxvDescricao = taxvDescricao;
    }

    public String getTaxvMachineName() {
        return taxvMachineName;
    }

    public void setTaxvMachineName(String taxvMachineName) {
        this.taxvMachineName = taxvMachineName;
    }

    @XmlTransient
    public List<TbTaxoTermo> getTbTaxoTermoList() {
        return tbTaxoTermoList;
    }

    public void setTbTaxoTermoList(List<TbTaxoTermo> tbTaxoTermoList) {
        this.tbTaxoTermoList = tbTaxoTermoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (taxvSeq != null ? taxvSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbTaxoVocab)) {
            return false;
        }
        TbTaxoVocab other = (TbTaxoVocab) object;
        if ((this.taxvSeq == null && other.taxvSeq != null) || (this.taxvSeq != null && !this.taxvSeq.equals(other.taxvSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbTaxoVocab[ taxvSeq=" + taxvSeq + " ]";
    }
    
}
