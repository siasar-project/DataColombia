/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_reabil_ampliacao")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbReabilAmpliacao.findAll", query = "SELECT t FROM TbReabilAmpliacao t")
    , @NamedQuery(name = "TbReabilAmpliacao.findByReampSeq", query = "SELECT t FROM TbReabilAmpliacao t WHERE t.reampSeq = :reampSeq")
    , @NamedQuery(name = "TbReabilAmpliacao.findBySysA019001", query = "SELECT t FROM TbReabilAmpliacao t WHERE t.sysA019001 = :sysA019001")
    , @NamedQuery(name = "TbReabilAmpliacao.findBySysA020001", query = "SELECT t FROM TbReabilAmpliacao t WHERE t.sysA020001 = :sysA020001")
    , @NamedQuery(name = "TbReabilAmpliacao.findBySysA021001", query = "SELECT t FROM TbReabilAmpliacao t WHERE t.sysA021001 = :sysA021001")
    , @NamedQuery(name = "TbReabilAmpliacao.findBySysA022001", query = "SELECT t FROM TbReabilAmpliacao t WHERE t.sysA022001 = :sysA022001")
    , @NamedQuery(name = "TbReabilAmpliacao.findBySysA023001", query = "SELECT t FROM TbReabilAmpliacao t WHERE t.sysA023001 = :sysA023001")
    , @NamedQuery(name = "TbReabilAmpliacao.findBySysA024001", query = "SELECT t FROM TbReabilAmpliacao t WHERE t.sysA024001 = :sysA024001")
    , @NamedQuery(name = "TbReabilAmpliacao.findBySysA025001", query = "SELECT t FROM TbReabilAmpliacao t WHERE t.sysA025001 = :sysA025001")
    , @NamedQuery(name = "TbReabilAmpliacao.findBySysA026001", query = "SELECT t FROM TbReabilAmpliacao t WHERE t.sysA026001 = :sysA026001")
    , @NamedQuery(name = "TbReabilAmpliacao.findByDateLastUpdate", query = "SELECT t FROM TbReabilAmpliacao t WHERE t.dateLastUpdate = :dateLastUpdate")})
public class TbReabilAmpliacao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "reamp_seq")
    private Long reampSeq;
    @Column(name = "sys_a_019_001")
    private Integer sysA019001;
    @Column(name = "sys_a_020_001")
    private Integer sysA020001;
    @Column(name = "sys_a_021_001")
    private Integer sysA021001;
    @Column(name = "sys_a_022_001")
    private Integer sysA022001;
    @Column(name = "sys_a_023_001")
    private Integer sysA023001;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "sys_a_024_001")
    private BigDecimal sysA024001;
    @Column(name = "sys_a_025_001")
    private Integer sysA025001;
    @Column(name = "sys_a_026_001")
    private BigDecimal sysA026001;
    @Column(name = "date_last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdate;
    @JoinColumn(name = "siste_seq", referencedColumnName = "siste_seq")
    @ManyToOne
    private TbSistema sisteSeq;

    public TbReabilAmpliacao() {
    }

    public TbReabilAmpliacao(Long reampSeq) {
        this.reampSeq = reampSeq;
    }

    public Long getReampSeq() {
        return reampSeq;
    }

    public void setReampSeq(Long reampSeq) {
        this.reampSeq = reampSeq;
    }

    public Integer getSysA019001() {
        return sysA019001;
    }

    public void setSysA019001(Integer sysA019001) {
        this.sysA019001 = sysA019001;
    }

    public Integer getSysA020001() {
        return sysA020001;
    }

    public void setSysA020001(Integer sysA020001) {
        this.sysA020001 = sysA020001;
    }

    public Integer getSysA021001() {
        return sysA021001;
    }

    public void setSysA021001(Integer sysA021001) {
        this.sysA021001 = sysA021001;
    }

    public Integer getSysA022001() {
        return sysA022001;
    }

    public void setSysA022001(Integer sysA022001) {
        this.sysA022001 = sysA022001;
    }

    public Integer getSysA023001() {
        return sysA023001;
    }

    public void setSysA023001(Integer sysA023001) {
        this.sysA023001 = sysA023001;
    }

    public BigDecimal getSysA024001() {
        return sysA024001;
    }

    public void setSysA024001(BigDecimal sysA024001) {
        this.sysA024001 = sysA024001;
    }

    public Integer getSysA025001() {
        return sysA025001;
    }

    public void setSysA025001(Integer sysA025001) {
        this.sysA025001 = sysA025001;
    }

    public BigDecimal getSysA026001() {
        return sysA026001;
    }

    public void setSysA026001(BigDecimal sysA026001) {
        this.sysA026001 = sysA026001;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    public TbSistema getSisteSeq() {
        return sisteSeq;
    }

    public void setSisteSeq(TbSistema sisteSeq) {
        this.sisteSeq = sisteSeq;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reampSeq != null ? reampSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbReabilAmpliacao)) {
            return false;
        }
        TbReabilAmpliacao other = (TbReabilAmpliacao) object;
        if ((this.reampSeq == null && other.reampSeq != null) || (this.reampSeq != null && !this.reampSeq.equals(other.reampSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbReabilAmpliacao[ reampSeq=" + reampSeq + " ]";
    }
    
}
