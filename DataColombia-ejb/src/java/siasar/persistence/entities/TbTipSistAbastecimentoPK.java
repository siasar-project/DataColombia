/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author danielo
 */
@Embeddable
public class TbTipSistAbastecimentoPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "siste_seq")
    private long sisteSeq;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxt_seq")
    private long taxtSeq;

    public TbTipSistAbastecimentoPK() {
    }

    public TbTipSistAbastecimentoPK(long sisteSeq, long taxtSeq) {
        this.sisteSeq = sisteSeq;
        this.taxtSeq = taxtSeq;
    }

    public long getSisteSeq() {
        return sisteSeq;
    }

    public void setSisteSeq(long sisteSeq) {
        this.sisteSeq = sisteSeq;
    }

    public long getTaxtSeq() {
        return taxtSeq;
    }

    public void setTaxtSeq(long taxtSeq) {
        this.taxtSeq = taxtSeq;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) sisteSeq;
        hash += (int) taxtSeq;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbTipSistAbastecimentoPK)) {
            return false;
        }
        TbTipSistAbastecimentoPK other = (TbTipSistAbastecimentoPK) object;
        if (this.sisteSeq != other.sisteSeq) {
            return false;
        }
        if (this.taxtSeq != other.taxtSeq) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbTipSistAbastecimentoPK[ sisteSeq=" + sisteSeq + ", taxtSeq=" + taxtSeq + " ]";
    }
    
}
