/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_variavel")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbVariavel.findAll", query = "SELECT t FROM TbVariavel t")
    , @NamedQuery(name = "TbVariavel.findByVariaSeq", query = "SELECT t FROM TbVariavel t WHERE t.variaSeq = :variaSeq")
    , @NamedQuery(name = "TbVariavel.findByVariaNome", query = "SELECT t FROM TbVariavel t WHERE t.variaNome = :variaNome")
    , @NamedQuery(name = "TbVariavel.findByVariaDescricao", query = "SELECT t FROM TbVariavel t WHERE t.variaDescricao = :variaDescricao")
    , @NamedQuery(name = "TbVariavel.findByVariaConsulta", query = "SELECT t FROM TbVariavel t WHERE t.variaConsulta = :variaConsulta")
    , @NamedQuery(name = "TbVariavel.findByVariaFormula", query = "SELECT t FROM TbVariavel t WHERE t.variaFormula = :variaFormula")
    , @NamedQuery(name = "TbVariavel.findByVariaAtivo", query = "SELECT t FROM TbVariavel t WHERE t.variaAtivo = :variaAtivo")
    , @NamedQuery(name = "TbVariavel.findByVariaNivel", query = "SELECT t FROM TbVariavel t WHERE t.variaNivel = :variaNivel")
    , @NamedQuery(name = "TbVariavel.findByVariaTipo", query = "SELECT t FROM TbVariavel t WHERE t.variaTipo = :variaTipo")
    , @NamedQuery(name = "TbVariavel.findByVariaCodigo", query = "SELECT t FROM TbVariavel t WHERE t.variaCodigo = :variaCodigo")})
public class TbVariavel implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "varia_seq")
    private Long variaSeq;
    @Size(max = 255)
    @Column(name = "varia_nome")
    private String variaNome;
    @Size(max = 600)
    @Column(name = "varia_descricao")
    private String variaDescricao;
    @Size(max = 2147483647)
    @Column(name = "varia_consulta")
    private String variaConsulta;
    @Size(max = 4000)
    @Column(name = "varia_formula")
    private String variaFormula;
    @Column(name = "varia_ativo")
    private Boolean variaAtivo;
    @Column(name = "varia_nivel")
    private Integer variaNivel;
    @Size(max = 3)
    @Column(name = "varia_tipo")
    private String variaTipo;
    @Size(max = 100)
    @Column(name = "varia_codigo")
    private String variaCodigo;
    @OneToMany(mappedBy = "variaPai")
    private List<TbVariavaria> tbVariavariaList;
    @OneToMany(mappedBy = "variaFilho")
    private List<TbVariavaria> tbVariavariaList1;
    @JoinColumn(name = "tipo_con_seq", referencedColumnName = "tipo_con_seq")
    @ManyToOne
    private TbTipoContexto tipoConSeq;
    @JoinColumn(name = "unit_seq", referencedColumnName = "unit_seq")
    @ManyToOne
    private TbUnit unitSeq;
    @OneToMany(mappedBy = "variaSeq")
    private List<TbVariaRestricao> tbVariaRestricaoList;
    @OneToMany(mappedBy = "variaSeq")
    private List<TbVariavelDw> tbVariavelDwList;

    public TbVariavel() {
    }

    public TbVariavel(Long variaSeq) {
        this.variaSeq = variaSeq;
    }

    public Long getVariaSeq() {
        return variaSeq;
    }

    public void setVariaSeq(Long variaSeq) {
        this.variaSeq = variaSeq;
    }

    public String getVariaNome() {
        return variaNome;
    }

    public void setVariaNome(String variaNome) {
        this.variaNome = variaNome;
    }

    public String getVariaDescricao() {
        return variaDescricao;
    }

    public void setVariaDescricao(String variaDescricao) {
        this.variaDescricao = variaDescricao;
    }

    public String getVariaConsulta() {
        return variaConsulta;
    }

    public void setVariaConsulta(String variaConsulta) {
        this.variaConsulta = variaConsulta;
    }

    public String getVariaFormula() {
        return variaFormula;
    }

    public void setVariaFormula(String variaFormula) {
        this.variaFormula = variaFormula;
    }

    public Boolean getVariaAtivo() {
        return variaAtivo;
    }

    public void setVariaAtivo(Boolean variaAtivo) {
        this.variaAtivo = variaAtivo;
    }

    public Integer getVariaNivel() {
        return variaNivel;
    }

    public void setVariaNivel(Integer variaNivel) {
        this.variaNivel = variaNivel;
    }

    public String getVariaTipo() {
        return variaTipo;
    }

    public void setVariaTipo(String variaTipo) {
        this.variaTipo = variaTipo;
    }

    public String getVariaCodigo() {
        return variaCodigo;
    }

    public void setVariaCodigo(String variaCodigo) {
        this.variaCodigo = variaCodigo;
    }

    @XmlTransient
    public List<TbVariavaria> getTbVariavariaList() {
        return tbVariavariaList;
    }

    public void setTbVariavariaList(List<TbVariavaria> tbVariavariaList) {
        this.tbVariavariaList = tbVariavariaList;
    }

    @XmlTransient
    public List<TbVariavaria> getTbVariavariaList1() {
        return tbVariavariaList1;
    }

    public void setTbVariavariaList1(List<TbVariavaria> tbVariavariaList1) {
        this.tbVariavariaList1 = tbVariavariaList1;
    }

    public TbTipoContexto getTipoConSeq() {
        return tipoConSeq;
    }

    public void setTipoConSeq(TbTipoContexto tipoConSeq) {
        this.tipoConSeq = tipoConSeq;
    }

    public TbUnit getUnitSeq() {
        return unitSeq;
    }

    public void setUnitSeq(TbUnit unitSeq) {
        this.unitSeq = unitSeq;
    }

    @XmlTransient
    public List<TbVariaRestricao> getTbVariaRestricaoList() {
        return tbVariaRestricaoList;
    }

    public void setTbVariaRestricaoList(List<TbVariaRestricao> tbVariaRestricaoList) {
        this.tbVariaRestricaoList = tbVariaRestricaoList;
    }

    @XmlTransient
    public List<TbVariavelDw> getTbVariavelDwList() {
        return tbVariavelDwList;
    }

    public void setTbVariavelDwList(List<TbVariavelDw> tbVariavelDwList) {
        this.tbVariavelDwList = tbVariavelDwList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (variaSeq != null ? variaSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbVariavel)) {
            return false;
        }
        TbVariavel other = (TbVariavel) object;
        if ((this.variaSeq == null && other.variaSeq != null) || (this.variaSeq != null && !this.variaSeq.equals(other.variaSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbVariavel[ variaSeq=" + variaSeq + " ]";
    }
    
}
