/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_sistema")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbSistema.findAll", query = "SELECT t FROM TbSistema t")
    , @NamedQuery(name = "TbSistema.findBySisteSeq", query = "SELECT t FROM TbSistema t WHERE t.sisteSeq = :sisteSeq")
    , @NamedQuery(name = "TbSistema.findByValidado", query = "SELECT t FROM TbSistema t WHERE t.validado = :validado")
    , @NamedQuery(name = "TbSistema.findByDataValidacao", query = "SELECT t FROM TbSistema t WHERE t.dataValidacao = :dataValidacao")
    , @NamedQuery(name = "TbSistema.findBySysA001001", query = "SELECT t FROM TbSistema t WHERE t.sysA001001 = :sysA001001")
    , @NamedQuery(name = "TbSistema.findBySysA002001", query = "SELECT t FROM TbSistema t WHERE t.sysA002001 = :sysA002001")
    , @NamedQuery(name = "TbSistema.findBySysA004001", query = "SELECT t FROM TbSistema t WHERE t.sysA004001 = :sysA004001")
    , @NamedQuery(name = "TbSistema.findBySysA005001", query = "SELECT t FROM TbSistema t WHERE t.sysA005001 = :sysA005001")
    , @NamedQuery(name = "TbSistema.findBySysA006001", query = "SELECT t FROM TbSistema t WHERE t.sysA006001 = :sysA006001")
    , @NamedQuery(name = "TbSistema.findBySysA007001", query = "SELECT t FROM TbSistema t WHERE t.sysA007001 = :sysA007001")
    , @NamedQuery(name = "TbSistema.findBySysA008001", query = "SELECT t FROM TbSistema t WHERE t.sysA008001 = :sysA008001")
    , @NamedQuery(name = "TbSistema.findBySysA009001", query = "SELECT t FROM TbSistema t WHERE t.sysA009001 = :sysA009001")
    , @NamedQuery(name = "TbSistema.findBySysA010001", query = "SELECT t FROM TbSistema t WHERE t.sysA010001 = :sysA010001")
    , @NamedQuery(name = "TbSistema.findBySysA011001", query = "SELECT t FROM TbSistema t WHERE t.sysA011001 = :sysA011001")
    , @NamedQuery(name = "TbSistema.findBySysA012001", query = "SELECT t FROM TbSistema t WHERE t.sysA012001 = :sysA012001")
    , @NamedQuery(name = "TbSistema.findBySysA013001", query = "SELECT t FROM TbSistema t WHERE t.sysA013001 = :sysA013001")
    , @NamedQuery(name = "TbSistema.findBySysA018001", query = "SELECT t FROM TbSistema t WHERE t.sysA018001 = :sysA018001")
    , @NamedQuery(name = "TbSistema.findBySysA026001", query = "SELECT t FROM TbSistema t WHERE t.sysA026001 = :sysA026001")
    , @NamedQuery(name = "TbSistema.findBySysA028001", query = "SELECT t FROM TbSistema t WHERE t.sysA028001 = :sysA028001")
    , @NamedQuery(name = "TbSistema.findBySysA029001", query = "SELECT t FROM TbSistema t WHERE t.sysA029001 = :sysA029001")
    , @NamedQuery(name = "TbSistema.findBySysA030001", query = "SELECT t FROM TbSistema t WHERE t.sysA030001 = :sysA030001")
    , @NamedQuery(name = "TbSistema.findBySysA031001", query = "SELECT t FROM TbSistema t WHERE t.sysA031001 = :sysA031001")
    , @NamedQuery(name = "TbSistema.findBySysA032001", query = "SELECT t FROM TbSistema t WHERE t.sysA032001 = :sysA032001")
    , @NamedQuery(name = "TbSistema.findBySysG001001", query = "SELECT t FROM TbSistema t WHERE t.sysG001001 = :sysG001001")
    , @NamedQuery(name = "TbSistema.findBySysG002001", query = "SELECT t FROM TbSistema t WHERE t.sysG002001 = :sysG002001")
    , @NamedQuery(name = "TbSistema.findBySysG003001", query = "SELECT t FROM TbSistema t WHERE t.sysG003001 = :sysG003001")
    , @NamedQuery(name = "TbSistema.findBySysG004001", query = "SELECT t FROM TbSistema t WHERE t.sysG004001 = :sysG004001")
    , @NamedQuery(name = "TbSistema.findBySysG005001", query = "SELECT t FROM TbSistema t WHERE t.sysG005001 = :sysG005001")
    , @NamedQuery(name = "TbSistema.findBySysG006001", query = "SELECT t FROM TbSistema t WHERE t.sysG006001 = :sysG006001")
    , @NamedQuery(name = "TbSistema.findBySysG007001", query = "SELECT t FROM TbSistema t WHERE t.sysG007001 = :sysG007001")
    , @NamedQuery(name = "TbSistema.findBySysG008001", query = "SELECT t FROM TbSistema t WHERE t.sysG008001 = :sysG008001")
    , @NamedQuery(name = "TbSistema.findBySysG009001", query = "SELECT t FROM TbSistema t WHERE t.sysG009001 = :sysG009001")
    , @NamedQuery(name = "TbSistema.findBySysG010001", query = "SELECT t FROM TbSistema t WHERE t.sysG010001 = :sysG010001")
    , @NamedQuery(name = "TbSistema.findBySysG011001", query = "SELECT t FROM TbSistema t WHERE t.sysG011001 = :sysG011001")
    , @NamedQuery(name = "TbSistema.findBySysH001001", query = "SELECT t FROM TbSistema t WHERE t.sysH001001 = :sysH001001")
    , @NamedQuery(name = "TbSistema.findBySysA034001", query = "SELECT t FROM TbSistema t WHERE t.sysA034001 = :sysA034001")
    , @NamedQuery(name = "TbSistema.findByClassificacao", query = "SELECT t FROM TbSistema t WHERE t.classificacao = :classificacao")
    , @NamedQuery(name = "TbSistema.findByPaisSigla", query = "SELECT t FROM TbSistema t WHERE t.paisSigla = :paisSigla")
    , @NamedQuery(name = "TbSistema.findByDateLastUpdate", query = "SELECT t FROM TbSistema t WHERE t.dateLastUpdate = :dateLastUpdate")
    , @NamedQuery(name = "TbSistema.findBySysA033001", query = "SELECT t FROM TbSistema t WHERE t.sysA033001 = :sysA033001")})
public class TbSistema implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "siste_seq")
    private Long sisteSeq;
    @Column(name = "validado")
    private Boolean validado;
    @Column(name = "data_validacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataValidacao;
    @Size(max = 2147483647)
    @Column(name = "sys_a_001_001")
    private String sysA001001;
    @Column(name = "sys_a_002_001")
    private Integer sysA002001;
    @Column(name = "sys_a_004_001")
    private Integer sysA004001;
    @Column(name = "sys_a_005_001")
    private Integer sysA005001;
    @Size(max = 2)
    @Column(name = "sys_a_006_001")
    private String sysA006001;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "sys_a_007_001")
    private Double sysA007001;
    @Column(name = "sys_a_008_001")
    private Double sysA008001;
    @Column(name = "sys_a_009_001")
    private Double sysA009001;
    @Size(max = 100)
    @Column(name = "sys_a_010_001")
    private String sysA010001;
    @Column(name = "sys_a_011_001")
    private Integer sysA011001;
    @Column(name = "sys_a_012_001")
    private Integer sysA012001;
    @Column(name = "sys_a_013_001")
    private Integer sysA013001;
    @Column(name = "sys_a_018_001")
    private BigDecimal sysA018001;
    @Column(name = "sys_a_026_001")
    private BigDecimal sysA026001;
    @Column(name = "sys_a_028_001")
    private Boolean sysA028001;
    @Column(name = "sys_a_029_001")
    private Boolean sysA029001;
    @Size(max = 4000)
    @Column(name = "sys_a_030_001")
    private String sysA030001;
    @Column(name = "sys_a_031_001")
    @Temporal(TemporalType.DATE)
    private Date sysA031001;
    @Size(max = 255)
    @Column(name = "sys_a_032_001")
    private String sysA032001;
    @Column(name = "sys_g_001_001")
    private BigDecimal sysG001001;
    @Column(name = "sys_g_002_001")
    private Integer sysG002001;
    @Column(name = "sys_g_003_001")
    private Integer sysG003001;
    @Column(name = "sys_g_004_001")
    private Integer sysG004001;
    @Column(name = "sys_g_005_001")
    @Temporal(TemporalType.DATE)
    private Date sysG005001;
    @Column(name = "sys_g_006_001")
    private BigDecimal sysG006001;
    @Column(name = "sys_g_007_001")
    private Integer sysG007001;
    @Column(name = "sys_g_008_001")
    @Temporal(TemporalType.DATE)
    private Date sysG008001;
    @Column(name = "sys_g_009_001")
    private Boolean sysG009001;
    @Column(name = "sys_g_010_001")
    @Temporal(TemporalType.DATE)
    private Date sysG010001;
    @Column(name = "sys_g_011_001")
    private Boolean sysG011001;
    @Size(max = 2147483647)
    @Column(name = "sys_h_001_001")
    private String sysH001001;
    @Size(max = 4000)
    @Column(name = "sys_a_034_001")
    private String sysA034001;
    @Size(max = 1)
    @Column(name = "classificacao")
    private String classificacao;
    @Size(max = 60)
    @Column(name = "pais_sigla")
    private String paisSigla;
    @Column(name = "date_last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdate;
    @Size(max = 2)
    @Column(name = "sys_a_033_001")
    private String sysA033001;
    @OneToMany(mappedBy = "sisteSeq")
    private List<TbArmazenamento> tbArmazenamentoList;
    @OneToMany(mappedBy = "sisteSeq")
    private List<TbReabilAmpliacao> tbReabilAmpliacaoList;
    @OneToMany(mappedBy = "sisteSeq")
    private List<TbTratamento> tbTratamentoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tbSistema")
    private List<TbTipSistAbastecimento> tbTipSistAbastecimentoList;
    @OneToMany(mappedBy = "sisteSeq")
    private List<TbCaptacao> tbCaptacaoList;
    @OneToMany(mappedBy = "sisteSeq")
    private List<TbFonteFinanc> tbFonteFinancList;
    @OneToMany(mappedBy = "sisteSeq")
    private List<TbCentroSaude> tbCentroSaudeList;
    @OneToMany(mappedBy = "sisteSeq")
    private List<TbRedeDistribuicao> tbRedeDistribuicaoList;
    @OneToMany(mappedBy = "sisteSeq")
    private List<TbConducao> tbConducaoList;

    public TbSistema() {
    }

    public TbSistema(Long sisteSeq) {
        this.sisteSeq = sisteSeq;
    }

    public Long getSisteSeq() {
        return sisteSeq;
    }

    public void setSisteSeq(Long sisteSeq) {
        this.sisteSeq = sisteSeq;
    }

    public Boolean getValidado() {
        return validado;
    }

    public void setValidado(Boolean validado) {
        this.validado = validado;
    }

    public Date getDataValidacao() {
        return dataValidacao;
    }

    public void setDataValidacao(Date dataValidacao) {
        this.dataValidacao = dataValidacao;
    }

    public String getSysA001001() {
        return sysA001001;
    }

    public void setSysA001001(String sysA001001) {
        this.sysA001001 = sysA001001;
    }

    public Integer getSysA002001() {
        return sysA002001;
    }

    public void setSysA002001(Integer sysA002001) {
        this.sysA002001 = sysA002001;
    }

    public Integer getSysA004001() {
        return sysA004001;
    }

    public void setSysA004001(Integer sysA004001) {
        this.sysA004001 = sysA004001;
    }

    public Integer getSysA005001() {
        return sysA005001;
    }

    public void setSysA005001(Integer sysA005001) {
        this.sysA005001 = sysA005001;
    }

    public String getSysA006001() {
        return sysA006001;
    }

    public void setSysA006001(String sysA006001) {
        this.sysA006001 = sysA006001;
    }

    public Double getSysA007001() {
        return sysA007001;
    }

    public void setSysA007001(Double sysA007001) {
        this.sysA007001 = sysA007001;
    }

    public Double getSysA008001() {
        return sysA008001;
    }

    public void setSysA008001(Double sysA008001) {
        this.sysA008001 = sysA008001;
    }

    public Double getSysA009001() {
        return sysA009001;
    }

    public void setSysA009001(Double sysA009001) {
        this.sysA009001 = sysA009001;
    }

    public String getSysA010001() {
        return sysA010001;
    }

    public void setSysA010001(String sysA010001) {
        this.sysA010001 = sysA010001;
    }

    public Integer getSysA011001() {
        return sysA011001;
    }

    public void setSysA011001(Integer sysA011001) {
        this.sysA011001 = sysA011001;
    }

    public Integer getSysA012001() {
        return sysA012001;
    }

    public void setSysA012001(Integer sysA012001) {
        this.sysA012001 = sysA012001;
    }

    public Integer getSysA013001() {
        return sysA013001;
    }

    public void setSysA013001(Integer sysA013001) {
        this.sysA013001 = sysA013001;
    }

    public BigDecimal getSysA018001() {
        return sysA018001;
    }

    public void setSysA018001(BigDecimal sysA018001) {
        this.sysA018001 = sysA018001;
    }

    public BigDecimal getSysA026001() {
        return sysA026001;
    }

    public void setSysA026001(BigDecimal sysA026001) {
        this.sysA026001 = sysA026001;
    }

    public Boolean getSysA028001() {
        return sysA028001;
    }

    public void setSysA028001(Boolean sysA028001) {
        this.sysA028001 = sysA028001;
    }

    public Boolean getSysA029001() {
        return sysA029001;
    }

    public void setSysA029001(Boolean sysA029001) {
        this.sysA029001 = sysA029001;
    }

    public String getSysA030001() {
        return sysA030001;
    }

    public void setSysA030001(String sysA030001) {
        this.sysA030001 = sysA030001;
    }

    public Date getSysA031001() {
        return sysA031001;
    }

    public void setSysA031001(Date sysA031001) {
        this.sysA031001 = sysA031001;
    }

    public String getSysA032001() {
        return sysA032001;
    }

    public void setSysA032001(String sysA032001) {
        this.sysA032001 = sysA032001;
    }

    public BigDecimal getSysG001001() {
        return sysG001001;
    }

    public void setSysG001001(BigDecimal sysG001001) {
        this.sysG001001 = sysG001001;
    }

    public Integer getSysG002001() {
        return sysG002001;
    }

    public void setSysG002001(Integer sysG002001) {
        this.sysG002001 = sysG002001;
    }

    public Integer getSysG003001() {
        return sysG003001;
    }

    public void setSysG003001(Integer sysG003001) {
        this.sysG003001 = sysG003001;
    }

    public Integer getSysG004001() {
        return sysG004001;
    }

    public void setSysG004001(Integer sysG004001) {
        this.sysG004001 = sysG004001;
    }

    public Date getSysG005001() {
        return sysG005001;
    }

    public void setSysG005001(Date sysG005001) {
        this.sysG005001 = sysG005001;
    }

    public BigDecimal getSysG006001() {
        return sysG006001;
    }

    public void setSysG006001(BigDecimal sysG006001) {
        this.sysG006001 = sysG006001;
    }

    public Integer getSysG007001() {
        return sysG007001;
    }

    public void setSysG007001(Integer sysG007001) {
        this.sysG007001 = sysG007001;
    }

    public Date getSysG008001() {
        return sysG008001;
    }

    public void setSysG008001(Date sysG008001) {
        this.sysG008001 = sysG008001;
    }

    public Boolean getSysG009001() {
        return sysG009001;
    }

    public void setSysG009001(Boolean sysG009001) {
        this.sysG009001 = sysG009001;
    }

    public Date getSysG010001() {
        return sysG010001;
    }

    public void setSysG010001(Date sysG010001) {
        this.sysG010001 = sysG010001;
    }

    public Boolean getSysG011001() {
        return sysG011001;
    }

    public void setSysG011001(Boolean sysG011001) {
        this.sysG011001 = sysG011001;
    }

    public String getSysH001001() {
        return sysH001001;
    }

    public void setSysH001001(String sysH001001) {
        this.sysH001001 = sysH001001;
    }

    public String getSysA034001() {
        return sysA034001;
    }

    public void setSysA034001(String sysA034001) {
        this.sysA034001 = sysA034001;
    }

    public String getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(String classificacao) {
        this.classificacao = classificacao;
    }

    public String getPaisSigla() {
        return paisSigla;
    }

    public void setPaisSigla(String paisSigla) {
        this.paisSigla = paisSigla;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    public String getSysA033001() {
        return sysA033001;
    }

    public void setSysA033001(String sysA033001) {
        this.sysA033001 = sysA033001;
    }

    @XmlTransient
    public List<TbArmazenamento> getTbArmazenamentoList() {
        return tbArmazenamentoList;
    }

    public void setTbArmazenamentoList(List<TbArmazenamento> tbArmazenamentoList) {
        this.tbArmazenamentoList = tbArmazenamentoList;
    }

    @XmlTransient
    public List<TbReabilAmpliacao> getTbReabilAmpliacaoList() {
        return tbReabilAmpliacaoList;
    }

    public void setTbReabilAmpliacaoList(List<TbReabilAmpliacao> tbReabilAmpliacaoList) {
        this.tbReabilAmpliacaoList = tbReabilAmpliacaoList;
    }

    @XmlTransient
    public List<TbTratamento> getTbTratamentoList() {
        return tbTratamentoList;
    }

    public void setTbTratamentoList(List<TbTratamento> tbTratamentoList) {
        this.tbTratamentoList = tbTratamentoList;
    }

    @XmlTransient
    public List<TbTipSistAbastecimento> getTbTipSistAbastecimentoList() {
        return tbTipSistAbastecimentoList;
    }

    public void setTbTipSistAbastecimentoList(List<TbTipSistAbastecimento> tbTipSistAbastecimentoList) {
        this.tbTipSistAbastecimentoList = tbTipSistAbastecimentoList;
    }

    @XmlTransient
    public List<TbCaptacao> getTbCaptacaoList() {
        return tbCaptacaoList;
    }

    public void setTbCaptacaoList(List<TbCaptacao> tbCaptacaoList) {
        this.tbCaptacaoList = tbCaptacaoList;
    }

    @XmlTransient
    public List<TbFonteFinanc> getTbFonteFinancList() {
        return tbFonteFinancList;
    }

    public void setTbFonteFinancList(List<TbFonteFinanc> tbFonteFinancList) {
        this.tbFonteFinancList = tbFonteFinancList;
    }

    @XmlTransient
    public List<TbCentroSaude> getTbCentroSaudeList() {
        return tbCentroSaudeList;
    }

    public void setTbCentroSaudeList(List<TbCentroSaude> tbCentroSaudeList) {
        this.tbCentroSaudeList = tbCentroSaudeList;
    }

    @XmlTransient
    public List<TbRedeDistribuicao> getTbRedeDistribuicaoList() {
        return tbRedeDistribuicaoList;
    }

    public void setTbRedeDistribuicaoList(List<TbRedeDistribuicao> tbRedeDistribuicaoList) {
        this.tbRedeDistribuicaoList = tbRedeDistribuicaoList;
    }

    @XmlTransient
    public List<TbConducao> getTbConducaoList() {
        return tbConducaoList;
    }

    public void setTbConducaoList(List<TbConducao> tbConducaoList) {
        this.tbConducaoList = tbConducaoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sisteSeq != null ? sisteSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbSistema)) {
            return false;
        }
        TbSistema other = (TbSistema) object;
        if ((this.sisteSeq == null && other.sisteSeq != null) || (this.sisteSeq != null && !this.sisteSeq.equals(other.sisteSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbSistema[ sisteSeq=" + sisteSeq + " ]";
    }
    
}
