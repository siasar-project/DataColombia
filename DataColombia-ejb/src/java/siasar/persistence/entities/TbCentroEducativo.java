/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_centro_educativo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbCentroEducativo.findAll", query = "SELECT t FROM TbCentroEducativo t")
    , @NamedQuery(name = "TbCentroEducativo.findByCenedSeq", query = "SELECT t FROM TbCentroEducativo t WHERE t.cenedSeq = :cenedSeq")
    , @NamedQuery(name = "TbCentroEducativo.findBySisteSeq", query = "SELECT t FROM TbCentroEducativo t WHERE t.sisteSeq = :sisteSeq")
    , @NamedQuery(name = "TbCentroEducativo.findByComC004001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC004001 = :comC004001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC005001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC005001 = :comC005001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC006001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC006001 = :comC006001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC007001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC007001 = :comC007001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC008001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC008001 = :comC008001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC009001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC009001 = :comC009001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC013001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC013001 = :comC013001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC014001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC014001 = :comC014001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC015001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC015001 = :comC015001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC016001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC016001 = :comC016001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC017001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC017001 = :comC017001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC018001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC018001 = :comC018001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC019001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC019001 = :comC019001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC020001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC020001 = :comC020001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC021001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC021001 = :comC021001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC022001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC022001 = :comC022001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC023001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC023001 = :comC023001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC024001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC024001 = :comC024001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC025001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC025001 = :comC025001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC026001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC026001 = :comC026001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC027001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC027001 = :comC027001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC028001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC028001 = :comC028001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC029001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC029001 = :comC029001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC030001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC030001 = :comC030001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC031001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC031001 = :comC031001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC032001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC032001 = :comC032001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC033001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC033001 = :comC033001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC034001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC034001 = :comC034001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC035001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC035001 = :comC035001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC036001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC036001 = :comC036001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC037001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC037001 = :comC037001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC038001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC038001 = :comC038001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC039001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC039001 = :comC039001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC040001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC040001 = :comC040001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC041001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC041001 = :comC041001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC042001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC042001 = :comC042001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC043001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC043001 = :comC043001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC044001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC044001 = :comC044001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC045001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC045001 = :comC045001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC046001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC046001 = :comC046001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC047001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC047001 = :comC047001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC048001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC048001 = :comC048001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC049001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC049001 = :comC049001")
    , @NamedQuery(name = "TbCentroEducativo.findByComC050001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC050001 = :comC050001")
    , @NamedQuery(name = "TbCentroEducativo.findByDateLastUpdate", query = "SELECT t FROM TbCentroEducativo t WHERE t.dateLastUpdate = :dateLastUpdate")
    , @NamedQuery(name = "TbCentroEducativo.findByComC010001", query = "SELECT t FROM TbCentroEducativo t WHERE t.comC010001 = :comC010001")})
public class TbCentroEducativo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "cened_seq")
    private Long cenedSeq;
    @Column(name = "siste_seq")
    private BigInteger sisteSeq;
    @Size(max = 2147483647)
    @Column(name = "com_c_004_001")
    private String comC004001;
    @Size(max = 255)
    @Column(name = "com_c_005_001")
    private String comC005001;
    @Column(name = "com_c_006_001")
    private Integer comC006001;
    @Column(name = "com_c_007_001")
    private Integer comC007001;
    @Column(name = "com_c_008_001")
    private Integer comC008001;
    @Column(name = "com_c_009_001")
    private Integer comC009001;
    @Column(name = "com_c_013_001")
    private Integer comC013001;
    @Size(max = 2147483647)
    @Column(name = "com_c_014_001")
    private String comC014001;
    @Column(name = "com_c_015_001")
    private Integer comC015001;
    @Column(name = "com_c_016_001")
    private Integer comC016001;
    @Column(name = "com_c_017_001")
    private Integer comC017001;
    @Column(name = "com_c_018_001")
    private Integer comC018001;
    @Column(name = "com_c_019_001")
    private Integer comC019001;
    @Column(name = "com_c_020_001")
    private Integer comC020001;
    @Column(name = "com_c_021_001")
    private Integer comC021001;
    @Column(name = "com_c_022_001")
    private Integer comC022001;
    @Column(name = "com_c_023_001")
    private Integer comC023001;
    @Column(name = "com_c_024_001")
    private Integer comC024001;
    @Column(name = "com_c_025_001")
    private Integer comC025001;
    @Column(name = "com_c_026_001")
    private Integer comC026001;
    @Column(name = "com_c_027_001")
    private Integer comC027001;
    @Column(name = "com_c_028_001")
    private Integer comC028001;
    @Column(name = "com_c_029_001")
    private Integer comC029001;
    @Column(name = "com_c_030_001")
    private Integer comC030001;
    @Column(name = "com_c_031_001")
    private Integer comC031001;
    @Column(name = "com_c_032_001")
    private Integer comC032001;
    @Column(name = "com_c_033_001")
    private Integer comC033001;
    @Column(name = "com_c_034_001")
    private Integer comC034001;
    @Column(name = "com_c_035_001")
    private Integer comC035001;
    @Column(name = "com_c_036_001")
    private Integer comC036001;
    @Column(name = "com_c_037_001")
    private Integer comC037001;
    @Column(name = "com_c_038_001")
    private Integer comC038001;
    @Column(name = "com_c_039_001")
    private Integer comC039001;
    @Column(name = "com_c_040_001")
    private Integer comC040001;
    @Column(name = "com_c_041_001")
    private Integer comC041001;
    @Column(name = "com_c_042_001")
    private Integer comC042001;
    @Column(name = "com_c_043_001")
    private Integer comC043001;
    @Column(name = "com_c_044_001")
    private Integer comC044001;
    @Column(name = "com_c_045_001")
    private Integer comC045001;
    @Column(name = "com_c_046_001")
    private Integer comC046001;
    @Column(name = "com_c_047_001")
    private Integer comC047001;
    @Column(name = "com_c_048_001")
    private Integer comC048001;
    @Column(name = "com_c_049_001")
    private Integer comC049001;
    @Column(name = "com_c_050_001")
    private Integer comC050001;
    @Column(name = "date_last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdate;
    @Size(max = 200)
    @Column(name = "com_c_010_001")
    private String comC010001;
    @JoinColumn(name = "comun_seq", referencedColumnName = "comun_seq")
    @ManyToOne
    private TbComunidade comunSeq;

    public TbCentroEducativo() {
    }

    public TbCentroEducativo(Long cenedSeq) {
        this.cenedSeq = cenedSeq;
    }

    public Long getCenedSeq() {
        return cenedSeq;
    }

    public void setCenedSeq(Long cenedSeq) {
        this.cenedSeq = cenedSeq;
    }

    public BigInteger getSisteSeq() {
        return sisteSeq;
    }

    public void setSisteSeq(BigInteger sisteSeq) {
        this.sisteSeq = sisteSeq;
    }

    public String getComC004001() {
        return comC004001;
    }

    public void setComC004001(String comC004001) {
        this.comC004001 = comC004001;
    }

    public String getComC005001() {
        return comC005001;
    }

    public void setComC005001(String comC005001) {
        this.comC005001 = comC005001;
    }

    public Integer getComC006001() {
        return comC006001;
    }

    public void setComC006001(Integer comC006001) {
        this.comC006001 = comC006001;
    }

    public Integer getComC007001() {
        return comC007001;
    }

    public void setComC007001(Integer comC007001) {
        this.comC007001 = comC007001;
    }

    public Integer getComC008001() {
        return comC008001;
    }

    public void setComC008001(Integer comC008001) {
        this.comC008001 = comC008001;
    }

    public Integer getComC009001() {
        return comC009001;
    }

    public void setComC009001(Integer comC009001) {
        this.comC009001 = comC009001;
    }

    public Integer getComC013001() {
        return comC013001;
    }

    public void setComC013001(Integer comC013001) {
        this.comC013001 = comC013001;
    }

    public String getComC014001() {
        return comC014001;
    }

    public void setComC014001(String comC014001) {
        this.comC014001 = comC014001;
    }

    public Integer getComC015001() {
        return comC015001;
    }

    public void setComC015001(Integer comC015001) {
        this.comC015001 = comC015001;
    }

    public Integer getComC016001() {
        return comC016001;
    }

    public void setComC016001(Integer comC016001) {
        this.comC016001 = comC016001;
    }

    public Integer getComC017001() {
        return comC017001;
    }

    public void setComC017001(Integer comC017001) {
        this.comC017001 = comC017001;
    }

    public Integer getComC018001() {
        return comC018001;
    }

    public void setComC018001(Integer comC018001) {
        this.comC018001 = comC018001;
    }

    public Integer getComC019001() {
        return comC019001;
    }

    public void setComC019001(Integer comC019001) {
        this.comC019001 = comC019001;
    }

    public Integer getComC020001() {
        return comC020001;
    }

    public void setComC020001(Integer comC020001) {
        this.comC020001 = comC020001;
    }

    public Integer getComC021001() {
        return comC021001;
    }

    public void setComC021001(Integer comC021001) {
        this.comC021001 = comC021001;
    }

    public Integer getComC022001() {
        return comC022001;
    }

    public void setComC022001(Integer comC022001) {
        this.comC022001 = comC022001;
    }

    public Integer getComC023001() {
        return comC023001;
    }

    public void setComC023001(Integer comC023001) {
        this.comC023001 = comC023001;
    }

    public Integer getComC024001() {
        return comC024001;
    }

    public void setComC024001(Integer comC024001) {
        this.comC024001 = comC024001;
    }

    public Integer getComC025001() {
        return comC025001;
    }

    public void setComC025001(Integer comC025001) {
        this.comC025001 = comC025001;
    }

    public Integer getComC026001() {
        return comC026001;
    }

    public void setComC026001(Integer comC026001) {
        this.comC026001 = comC026001;
    }

    public Integer getComC027001() {
        return comC027001;
    }

    public void setComC027001(Integer comC027001) {
        this.comC027001 = comC027001;
    }

    public Integer getComC028001() {
        return comC028001;
    }

    public void setComC028001(Integer comC028001) {
        this.comC028001 = comC028001;
    }

    public Integer getComC029001() {
        return comC029001;
    }

    public void setComC029001(Integer comC029001) {
        this.comC029001 = comC029001;
    }

    public Integer getComC030001() {
        return comC030001;
    }

    public void setComC030001(Integer comC030001) {
        this.comC030001 = comC030001;
    }

    public Integer getComC031001() {
        return comC031001;
    }

    public void setComC031001(Integer comC031001) {
        this.comC031001 = comC031001;
    }

    public Integer getComC032001() {
        return comC032001;
    }

    public void setComC032001(Integer comC032001) {
        this.comC032001 = comC032001;
    }

    public Integer getComC033001() {
        return comC033001;
    }

    public void setComC033001(Integer comC033001) {
        this.comC033001 = comC033001;
    }

    public Integer getComC034001() {
        return comC034001;
    }

    public void setComC034001(Integer comC034001) {
        this.comC034001 = comC034001;
    }

    public Integer getComC035001() {
        return comC035001;
    }

    public void setComC035001(Integer comC035001) {
        this.comC035001 = comC035001;
    }

    public Integer getComC036001() {
        return comC036001;
    }

    public void setComC036001(Integer comC036001) {
        this.comC036001 = comC036001;
    }

    public Integer getComC037001() {
        return comC037001;
    }

    public void setComC037001(Integer comC037001) {
        this.comC037001 = comC037001;
    }

    public Integer getComC038001() {
        return comC038001;
    }

    public void setComC038001(Integer comC038001) {
        this.comC038001 = comC038001;
    }

    public Integer getComC039001() {
        return comC039001;
    }

    public void setComC039001(Integer comC039001) {
        this.comC039001 = comC039001;
    }

    public Integer getComC040001() {
        return comC040001;
    }

    public void setComC040001(Integer comC040001) {
        this.comC040001 = comC040001;
    }

    public Integer getComC041001() {
        return comC041001;
    }

    public void setComC041001(Integer comC041001) {
        this.comC041001 = comC041001;
    }

    public Integer getComC042001() {
        return comC042001;
    }

    public void setComC042001(Integer comC042001) {
        this.comC042001 = comC042001;
    }

    public Integer getComC043001() {
        return comC043001;
    }

    public void setComC043001(Integer comC043001) {
        this.comC043001 = comC043001;
    }

    public Integer getComC044001() {
        return comC044001;
    }

    public void setComC044001(Integer comC044001) {
        this.comC044001 = comC044001;
    }

    public Integer getComC045001() {
        return comC045001;
    }

    public void setComC045001(Integer comC045001) {
        this.comC045001 = comC045001;
    }

    public Integer getComC046001() {
        return comC046001;
    }

    public void setComC046001(Integer comC046001) {
        this.comC046001 = comC046001;
    }

    public Integer getComC047001() {
        return comC047001;
    }

    public void setComC047001(Integer comC047001) {
        this.comC047001 = comC047001;
    }

    public Integer getComC048001() {
        return comC048001;
    }

    public void setComC048001(Integer comC048001) {
        this.comC048001 = comC048001;
    }

    public Integer getComC049001() {
        return comC049001;
    }

    public void setComC049001(Integer comC049001) {
        this.comC049001 = comC049001;
    }

    public Integer getComC050001() {
        return comC050001;
    }

    public void setComC050001(Integer comC050001) {
        this.comC050001 = comC050001;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    public String getComC010001() {
        return comC010001;
    }

    public void setComC010001(String comC010001) {
        this.comC010001 = comC010001;
    }

    public TbComunidade getComunSeq() {
        return comunSeq;
    }

    public void setComunSeq(TbComunidade comunSeq) {
        this.comunSeq = comunSeq;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cenedSeq != null ? cenedSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbCentroEducativo)) {
            return false;
        }
        TbCentroEducativo other = (TbCentroEducativo) object;
        if ((this.cenedSeq == null && other.cenedSeq != null) || (this.cenedSeq != null && !this.cenedSeq.equals(other.cenedSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbCentroEducativo[ cenedSeq=" + cenedSeq + " ]";
    }
    
}
