/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_conducao")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbConducao.findAll", query = "SELECT t FROM TbConducao t")
    , @NamedQuery(name = "TbConducao.findByConduSeq", query = "SELECT t FROM TbConducao t WHERE t.conduSeq = :conduSeq")
    , @NamedQuery(name = "TbConducao.findBySysC001001", query = "SELECT t FROM TbConducao t WHERE t.sysC001001 = :sysC001001")
    , @NamedQuery(name = "TbConducao.findBySysC002001", query = "SELECT t FROM TbConducao t WHERE t.sysC002001 = :sysC002001")
    , @NamedQuery(name = "TbConducao.findBySysC003001", query = "SELECT t FROM TbConducao t WHERE t.sysC003001 = :sysC003001")
    , @NamedQuery(name = "TbConducao.findBySysC004001", query = "SELECT t FROM TbConducao t WHERE t.sysC004001 = :sysC004001")
    , @NamedQuery(name = "TbConducao.findBySysC005001", query = "SELECT t FROM TbConducao t WHERE t.sysC005001 = :sysC005001")
    , @NamedQuery(name = "TbConducao.findBySysC006001", query = "SELECT t FROM TbConducao t WHERE t.sysC006001 = :sysC006001")
    , @NamedQuery(name = "TbConducao.findBySysC007001", query = "SELECT t FROM TbConducao t WHERE t.sysC007001 = :sysC007001")
    , @NamedQuery(name = "TbConducao.findBySysC008001", query = "SELECT t FROM TbConducao t WHERE t.sysC008001 = :sysC008001")
    , @NamedQuery(name = "TbConducao.findByDateLastUpdate", query = "SELECT t FROM TbConducao t WHERE t.dateLastUpdate = :dateLastUpdate")})
public class TbConducao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "condu_seq")
    private Long conduSeq;
    @Size(max = 100)
    @Column(name = "sys_c_001_001")
    private String sysC001001;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "sys_c_002_001")
    private Double sysC002001;
    @Column(name = "sys_c_003_001")
    private Integer sysC003001;
    @Column(name = "sys_c_004_001")
    private BigDecimal sysC004001;
    @Column(name = "sys_c_005_001")
    private Integer sysC005001;
    @Column(name = "sys_c_006_001")
    private Boolean sysC006001;
    @Column(name = "sys_c_007_001")
    private Integer sysC007001;
    @Size(max = 2147483647)
    @Column(name = "sys_c_008_001")
    private String sysC008001;
    @Column(name = "date_last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdate;
    @JoinColumn(name = "siste_seq", referencedColumnName = "siste_seq")
    @ManyToOne
    private TbSistema sisteSeq;

    public TbConducao() {
    }

    public TbConducao(Long conduSeq) {
        this.conduSeq = conduSeq;
    }

    public Long getConduSeq() {
        return conduSeq;
    }

    public void setConduSeq(Long conduSeq) {
        this.conduSeq = conduSeq;
    }

    public String getSysC001001() {
        return sysC001001;
    }

    public void setSysC001001(String sysC001001) {
        this.sysC001001 = sysC001001;
    }

    public Double getSysC002001() {
        return sysC002001;
    }

    public void setSysC002001(Double sysC002001) {
        this.sysC002001 = sysC002001;
    }

    public Integer getSysC003001() {
        return sysC003001;
    }

    public void setSysC003001(Integer sysC003001) {
        this.sysC003001 = sysC003001;
    }

    public BigDecimal getSysC004001() {
        return sysC004001;
    }

    public void setSysC004001(BigDecimal sysC004001) {
        this.sysC004001 = sysC004001;
    }

    public Integer getSysC005001() {
        return sysC005001;
    }

    public void setSysC005001(Integer sysC005001) {
        this.sysC005001 = sysC005001;
    }

    public Boolean getSysC006001() {
        return sysC006001;
    }

    public void setSysC006001(Boolean sysC006001) {
        this.sysC006001 = sysC006001;
    }

    public Integer getSysC007001() {
        return sysC007001;
    }

    public void setSysC007001(Integer sysC007001) {
        this.sysC007001 = sysC007001;
    }

    public String getSysC008001() {
        return sysC008001;
    }

    public void setSysC008001(String sysC008001) {
        this.sysC008001 = sysC008001;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    public TbSistema getSisteSeq() {
        return sisteSeq;
    }

    public void setSisteSeq(TbSistema sisteSeq) {
        this.sisteSeq = sisteSeq;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (conduSeq != null ? conduSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbConducao)) {
            return false;
        }
        TbConducao other = (TbConducao) object;
        if ((this.conduSeq == null && other.conduSeq != null) || (this.conduSeq != null && !this.conduSeq.equals(other.conduSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbConducao[ conduSeq=" + conduSeq + " ]";
    }
    
}
