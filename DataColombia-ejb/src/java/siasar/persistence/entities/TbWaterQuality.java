/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_water_quality")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbWaterQuality.findAll", query = "SELECT t FROM TbWaterQuality t")
    , @NamedQuery(name = "TbWaterQuality.findByWaqId", query = "SELECT t FROM TbWaterQuality t WHERE t.waqId = :waqId")
    , @NamedQuery(name = "TbWaterQuality.findByWaqA001001", query = "SELECT t FROM TbWaterQuality t WHERE t.waqA001001 = :waqA001001")
    , @NamedQuery(name = "TbWaterQuality.findByWaqA002001", query = "SELECT t FROM TbWaterQuality t WHERE t.waqA002001 = :waqA002001")
    , @NamedQuery(name = "TbWaterQuality.findByWaqA003001", query = "SELECT t FROM TbWaterQuality t WHERE t.waqA003001 = :waqA003001")
    , @NamedQuery(name = "TbWaterQuality.findByWaqA004001", query = "SELECT t FROM TbWaterQuality t WHERE t.waqA004001 = :waqA004001")
    , @NamedQuery(name = "TbWaterQuality.findByWaqA005001", query = "SELECT t FROM TbWaterQuality t WHERE t.waqA005001 = :waqA005001")
    , @NamedQuery(name = "TbWaterQuality.findByWaqA006001", query = "SELECT t FROM TbWaterQuality t WHERE t.waqA006001 = :waqA006001")
    , @NamedQuery(name = "TbWaterQuality.findByWaqA007001", query = "SELECT t FROM TbWaterQuality t WHERE t.waqA007001 = :waqA007001")
    , @NamedQuery(name = "TbWaterQuality.findByWaqA008001", query = "SELECT t FROM TbWaterQuality t WHERE t.waqA008001 = :waqA008001")
    , @NamedQuery(name = "TbWaterQuality.findByWaqA009001", query = "SELECT t FROM TbWaterQuality t WHERE t.waqA009001 = :waqA009001")
    , @NamedQuery(name = "TbWaterQuality.findByWaqA010001", query = "SELECT t FROM TbWaterQuality t WHERE t.waqA010001 = :waqA010001")
    , @NamedQuery(name = "TbWaterQuality.findByWaqB001001", query = "SELECT t FROM TbWaterQuality t WHERE t.waqB001001 = :waqB001001")
    , @NamedQuery(name = "TbWaterQuality.findByWaqB002001Va", query = "SELECT t FROM TbWaterQuality t WHERE t.waqB002001Va = :waqB002001Va")
    , @NamedQuery(name = "TbWaterQuality.findByWaqB002001Un", query = "SELECT t FROM TbWaterQuality t WHERE t.waqB002001Un = :waqB002001Un")
    , @NamedQuery(name = "TbWaterQuality.findByWaqB003001Va", query = "SELECT t FROM TbWaterQuality t WHERE t.waqB003001Va = :waqB003001Va")
    , @NamedQuery(name = "TbWaterQuality.findByWaqB003001Un", query = "SELECT t FROM TbWaterQuality t WHERE t.waqB003001Un = :waqB003001Un")
    , @NamedQuery(name = "TbWaterQuality.findByWaqB004001Va", query = "SELECT t FROM TbWaterQuality t WHERE t.waqB004001Va = :waqB004001Va")
    , @NamedQuery(name = "TbWaterQuality.findByWaqB004001Un", query = "SELECT t FROM TbWaterQuality t WHERE t.waqB004001Un = :waqB004001Un")
    , @NamedQuery(name = "TbWaterQuality.findByWaqB005001Va", query = "SELECT t FROM TbWaterQuality t WHERE t.waqB005001Va = :waqB005001Va")
    , @NamedQuery(name = "TbWaterQuality.findByWaqB005001Un", query = "SELECT t FROM TbWaterQuality t WHERE t.waqB005001Un = :waqB005001Un")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC001001Va", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC001001Va = :waqC001001Va")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC001001Un", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC001001Un = :waqC001001Un")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC002001Va", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC002001Va = :waqC002001Va")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC002001Un", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC002001Un = :waqC002001Un")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC003001Va", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC003001Va = :waqC003001Va")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC003001Un", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC003001Un = :waqC003001Un")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC004001Va", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC004001Va = :waqC004001Va")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC004001Un", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC004001Un = :waqC004001Un")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC005001Va", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC005001Va = :waqC005001Va")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC005001Un", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC005001Un = :waqC005001Un")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC006001Va", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC006001Va = :waqC006001Va")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC006001Un", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC006001Un = :waqC006001Un")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC007001Va", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC007001Va = :waqC007001Va")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC007001Un", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC007001Un = :waqC007001Un")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC008001Va", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC008001Va = :waqC008001Va")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC008001Un", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC008001Un = :waqC008001Un")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC009001Va", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC009001Va = :waqC009001Va")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC009001Un", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC009001Un = :waqC009001Un")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC010001Va", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC010001Va = :waqC010001Va")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC010001Un", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC010001Un = :waqC010001Un")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC011001Va", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC011001Va = :waqC011001Va")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC011001Un", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC011001Un = :waqC011001Un")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC012001Va", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC012001Va = :waqC012001Va")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC012001Un", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC012001Un = :waqC012001Un")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC013001Va", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC013001Va = :waqC013001Va")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC013001Un", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC013001Un = :waqC013001Un")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC014001Va", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC014001Va = :waqC014001Va")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC014001Un", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC014001Un = :waqC014001Un")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC015001Va", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC015001Va = :waqC015001Va")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC015001Un", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC015001Un = :waqC015001Un")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC016001Va", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC016001Va = :waqC016001Va")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC016001Un", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC016001Un = :waqC016001Un")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC017001Va", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC017001Va = :waqC017001Va")
    , @NamedQuery(name = "TbWaterQuality.findByWaqC017001Un", query = "SELECT t FROM TbWaterQuality t WHERE t.waqC017001Un = :waqC017001Un")
    , @NamedQuery(name = "TbWaterQuality.findByWaqD001001Va", query = "SELECT t FROM TbWaterQuality t WHERE t.waqD001001Va = :waqD001001Va")
    , @NamedQuery(name = "TbWaterQuality.findByWaqD001001Un", query = "SELECT t FROM TbWaterQuality t WHERE t.waqD001001Un = :waqD001001Un")
    , @NamedQuery(name = "TbWaterQuality.findByWaqE001001Va", query = "SELECT t FROM TbWaterQuality t WHERE t.waqE001001Va = :waqE001001Va")
    , @NamedQuery(name = "TbWaterQuality.findByWaqE001001Un", query = "SELECT t FROM TbWaterQuality t WHERE t.waqE001001Un = :waqE001001Un")
    , @NamedQuery(name = "TbWaterQuality.findByWaqE002001Va", query = "SELECT t FROM TbWaterQuality t WHERE t.waqE002001Va = :waqE002001Va")
    , @NamedQuery(name = "TbWaterQuality.findByWaqE002001Un", query = "SELECT t FROM TbWaterQuality t WHERE t.waqE002001Un = :waqE002001Un")
    , @NamedQuery(name = "TbWaterQuality.findByWaqF001001", query = "SELECT t FROM TbWaterQuality t WHERE t.waqF001001 = :waqF001001")
    , @NamedQuery(name = "TbWaterQuality.findByDateLastUpdate", query = "SELECT t FROM TbWaterQuality t WHERE t.dateLastUpdate = :dateLastUpdate")})
public class TbWaterQuality implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "waq_id")
    private Long waqId;
    @Column(name = "waq_a_001_001")
    @Temporal(TemporalType.DATE)
    private Date waqA001001;
    @Column(name = "waq_a_002_001")
    private Integer waqA002001;
    @Column(name = "waq_a_003_001")
    private Integer waqA003001;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "waq_a_004_001")
    private Double waqA004001;
    @Column(name = "waq_a_005_001")
    private Double waqA005001;
    @Column(name = "waq_a_006_001")
    private Double waqA006001;
    @Size(max = 4000)
    @Column(name = "waq_a_007_001")
    private String waqA007001;
    @Size(max = 4000)
    @Column(name = "waq_a_008_001")
    private String waqA008001;
    @Size(max = 4000)
    @Column(name = "waq_a_009_001")
    private String waqA009001;
    @Size(max = 2147483647)
    @Column(name = "waq_a_010_001")
    private String waqA010001;
    @Column(name = "waq_b_001_001")
    private BigDecimal waqB001001;
    @Column(name = "waq_b_002_001_va")
    private BigDecimal waqB002001Va;
    @Column(name = "waq_b_002_001_un")
    private Integer waqB002001Un;
    @Column(name = "waq_b_003_001_va")
    private BigDecimal waqB003001Va;
    @Column(name = "waq_b_003_001_un")
    private Integer waqB003001Un;
    @Column(name = "waq_b_004_001_va")
    private BigDecimal waqB004001Va;
    @Column(name = "waq_b_004_001_un")
    private Integer waqB004001Un;
    @Column(name = "waq_b_005_001_va")
    private BigDecimal waqB005001Va;
    @Column(name = "waq_b_005_001_un")
    private Integer waqB005001Un;
    @Column(name = "waq_c_001_001_va")
    private BigDecimal waqC001001Va;
    @Column(name = "waq_c_001_001_un")
    private Integer waqC001001Un;
    @Column(name = "waq_c_002_001_va")
    private BigDecimal waqC002001Va;
    @Column(name = "waq_c_002_001_un")
    private Integer waqC002001Un;
    @Column(name = "waq_c_003_001_va")
    private BigDecimal waqC003001Va;
    @Column(name = "waq_c_003_001_un")
    private Integer waqC003001Un;
    @Column(name = "waq_c_004_001_va")
    private BigDecimal waqC004001Va;
    @Column(name = "waq_c_004_001_un")
    private Integer waqC004001Un;
    @Column(name = "waq_c_005_001_va")
    private BigDecimal waqC005001Va;
    @Column(name = "waq_c_005_001_un")
    private Integer waqC005001Un;
    @Column(name = "waq_c_006_001_va")
    private BigDecimal waqC006001Va;
    @Column(name = "waq_c_006_001_un")
    private Integer waqC006001Un;
    @Column(name = "waq_c_007_001_va")
    private BigDecimal waqC007001Va;
    @Column(name = "waq_c_007_001_un")
    private Integer waqC007001Un;
    @Column(name = "waq_c_008_001_va")
    private BigDecimal waqC008001Va;
    @Column(name = "waq_c_008_001_un")
    private Integer waqC008001Un;
    @Column(name = "waq_c_009_001_va")
    private BigDecimal waqC009001Va;
    @Column(name = "waq_c_009_001_un")
    private Integer waqC009001Un;
    @Column(name = "waq_c_010_001_va")
    private BigDecimal waqC010001Va;
    @Column(name = "waq_c_010_001_un")
    private Integer waqC010001Un;
    @Column(name = "waq_c_011_001_va")
    private BigDecimal waqC011001Va;
    @Column(name = "waq_c_011_001_un")
    private Integer waqC011001Un;
    @Column(name = "waq_c_012_001_va")
    private BigDecimal waqC012001Va;
    @Column(name = "waq_c_012_001_un")
    private Integer waqC012001Un;
    @Column(name = "waq_c_013_001_va")
    private BigDecimal waqC013001Va;
    @Column(name = "waq_c_013_001_un")
    private Integer waqC013001Un;
    @Column(name = "waq_c_014_001_va")
    private BigDecimal waqC014001Va;
    @Column(name = "waq_c_014_001_un")
    private Integer waqC014001Un;
    @Column(name = "waq_c_015_001_va")
    private BigDecimal waqC015001Va;
    @Column(name = "waq_c_015_001_un")
    private Integer waqC015001Un;
    @Column(name = "waq_c_016_001_va")
    private BigDecimal waqC016001Va;
    @Column(name = "waq_c_016_001_un")
    private Integer waqC016001Un;
    @Column(name = "waq_c_017_001_va")
    private BigDecimal waqC017001Va;
    @Column(name = "waq_c_017_001_un")
    private Integer waqC017001Un;
    @Column(name = "waq_d_001_001_va")
    private BigDecimal waqD001001Va;
    @Column(name = "waq_d_001_001_un")
    private Integer waqD001001Un;
    @Column(name = "waq_e_001_001_va")
    private BigDecimal waqE001001Va;
    @Column(name = "waq_e_001_001_un")
    private Integer waqE001001Un;
    @Column(name = "waq_e_002_001_va")
    private BigDecimal waqE002001Va;
    @Column(name = "waq_e_002_001_un")
    private Integer waqE002001Un;
    @Size(max = 2147483647)
    @Column(name = "waq_f_001_001")
    private String waqF001001;
    @Column(name = "date_last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdate;

    public TbWaterQuality() {
    }

    public TbWaterQuality(Long waqId) {
        this.waqId = waqId;
    }

    public Long getWaqId() {
        return waqId;
    }

    public void setWaqId(Long waqId) {
        this.waqId = waqId;
    }

    public Date getWaqA001001() {
        return waqA001001;
    }

    public void setWaqA001001(Date waqA001001) {
        this.waqA001001 = waqA001001;
    }

    public Integer getWaqA002001() {
        return waqA002001;
    }

    public void setWaqA002001(Integer waqA002001) {
        this.waqA002001 = waqA002001;
    }

    public Integer getWaqA003001() {
        return waqA003001;
    }

    public void setWaqA003001(Integer waqA003001) {
        this.waqA003001 = waqA003001;
    }

    public Double getWaqA004001() {
        return waqA004001;
    }

    public void setWaqA004001(Double waqA004001) {
        this.waqA004001 = waqA004001;
    }

    public Double getWaqA005001() {
        return waqA005001;
    }

    public void setWaqA005001(Double waqA005001) {
        this.waqA005001 = waqA005001;
    }

    public Double getWaqA006001() {
        return waqA006001;
    }

    public void setWaqA006001(Double waqA006001) {
        this.waqA006001 = waqA006001;
    }

    public String getWaqA007001() {
        return waqA007001;
    }

    public void setWaqA007001(String waqA007001) {
        this.waqA007001 = waqA007001;
    }

    public String getWaqA008001() {
        return waqA008001;
    }

    public void setWaqA008001(String waqA008001) {
        this.waqA008001 = waqA008001;
    }

    public String getWaqA009001() {
        return waqA009001;
    }

    public void setWaqA009001(String waqA009001) {
        this.waqA009001 = waqA009001;
    }

    public String getWaqA010001() {
        return waqA010001;
    }

    public void setWaqA010001(String waqA010001) {
        this.waqA010001 = waqA010001;
    }

    public BigDecimal getWaqB001001() {
        return waqB001001;
    }

    public void setWaqB001001(BigDecimal waqB001001) {
        this.waqB001001 = waqB001001;
    }

    public BigDecimal getWaqB002001Va() {
        return waqB002001Va;
    }

    public void setWaqB002001Va(BigDecimal waqB002001Va) {
        this.waqB002001Va = waqB002001Va;
    }

    public Integer getWaqB002001Un() {
        return waqB002001Un;
    }

    public void setWaqB002001Un(Integer waqB002001Un) {
        this.waqB002001Un = waqB002001Un;
    }

    public BigDecimal getWaqB003001Va() {
        return waqB003001Va;
    }

    public void setWaqB003001Va(BigDecimal waqB003001Va) {
        this.waqB003001Va = waqB003001Va;
    }

    public Integer getWaqB003001Un() {
        return waqB003001Un;
    }

    public void setWaqB003001Un(Integer waqB003001Un) {
        this.waqB003001Un = waqB003001Un;
    }

    public BigDecimal getWaqB004001Va() {
        return waqB004001Va;
    }

    public void setWaqB004001Va(BigDecimal waqB004001Va) {
        this.waqB004001Va = waqB004001Va;
    }

    public Integer getWaqB004001Un() {
        return waqB004001Un;
    }

    public void setWaqB004001Un(Integer waqB004001Un) {
        this.waqB004001Un = waqB004001Un;
    }

    public BigDecimal getWaqB005001Va() {
        return waqB005001Va;
    }

    public void setWaqB005001Va(BigDecimal waqB005001Va) {
        this.waqB005001Va = waqB005001Va;
    }

    public Integer getWaqB005001Un() {
        return waqB005001Un;
    }

    public void setWaqB005001Un(Integer waqB005001Un) {
        this.waqB005001Un = waqB005001Un;
    }

    public BigDecimal getWaqC001001Va() {
        return waqC001001Va;
    }

    public void setWaqC001001Va(BigDecimal waqC001001Va) {
        this.waqC001001Va = waqC001001Va;
    }

    public Integer getWaqC001001Un() {
        return waqC001001Un;
    }

    public void setWaqC001001Un(Integer waqC001001Un) {
        this.waqC001001Un = waqC001001Un;
    }

    public BigDecimal getWaqC002001Va() {
        return waqC002001Va;
    }

    public void setWaqC002001Va(BigDecimal waqC002001Va) {
        this.waqC002001Va = waqC002001Va;
    }

    public Integer getWaqC002001Un() {
        return waqC002001Un;
    }

    public void setWaqC002001Un(Integer waqC002001Un) {
        this.waqC002001Un = waqC002001Un;
    }

    public BigDecimal getWaqC003001Va() {
        return waqC003001Va;
    }

    public void setWaqC003001Va(BigDecimal waqC003001Va) {
        this.waqC003001Va = waqC003001Va;
    }

    public Integer getWaqC003001Un() {
        return waqC003001Un;
    }

    public void setWaqC003001Un(Integer waqC003001Un) {
        this.waqC003001Un = waqC003001Un;
    }

    public BigDecimal getWaqC004001Va() {
        return waqC004001Va;
    }

    public void setWaqC004001Va(BigDecimal waqC004001Va) {
        this.waqC004001Va = waqC004001Va;
    }

    public Integer getWaqC004001Un() {
        return waqC004001Un;
    }

    public void setWaqC004001Un(Integer waqC004001Un) {
        this.waqC004001Un = waqC004001Un;
    }

    public BigDecimal getWaqC005001Va() {
        return waqC005001Va;
    }

    public void setWaqC005001Va(BigDecimal waqC005001Va) {
        this.waqC005001Va = waqC005001Va;
    }

    public Integer getWaqC005001Un() {
        return waqC005001Un;
    }

    public void setWaqC005001Un(Integer waqC005001Un) {
        this.waqC005001Un = waqC005001Un;
    }

    public BigDecimal getWaqC006001Va() {
        return waqC006001Va;
    }

    public void setWaqC006001Va(BigDecimal waqC006001Va) {
        this.waqC006001Va = waqC006001Va;
    }

    public Integer getWaqC006001Un() {
        return waqC006001Un;
    }

    public void setWaqC006001Un(Integer waqC006001Un) {
        this.waqC006001Un = waqC006001Un;
    }

    public BigDecimal getWaqC007001Va() {
        return waqC007001Va;
    }

    public void setWaqC007001Va(BigDecimal waqC007001Va) {
        this.waqC007001Va = waqC007001Va;
    }

    public Integer getWaqC007001Un() {
        return waqC007001Un;
    }

    public void setWaqC007001Un(Integer waqC007001Un) {
        this.waqC007001Un = waqC007001Un;
    }

    public BigDecimal getWaqC008001Va() {
        return waqC008001Va;
    }

    public void setWaqC008001Va(BigDecimal waqC008001Va) {
        this.waqC008001Va = waqC008001Va;
    }

    public Integer getWaqC008001Un() {
        return waqC008001Un;
    }

    public void setWaqC008001Un(Integer waqC008001Un) {
        this.waqC008001Un = waqC008001Un;
    }

    public BigDecimal getWaqC009001Va() {
        return waqC009001Va;
    }

    public void setWaqC009001Va(BigDecimal waqC009001Va) {
        this.waqC009001Va = waqC009001Va;
    }

    public Integer getWaqC009001Un() {
        return waqC009001Un;
    }

    public void setWaqC009001Un(Integer waqC009001Un) {
        this.waqC009001Un = waqC009001Un;
    }

    public BigDecimal getWaqC010001Va() {
        return waqC010001Va;
    }

    public void setWaqC010001Va(BigDecimal waqC010001Va) {
        this.waqC010001Va = waqC010001Va;
    }

    public Integer getWaqC010001Un() {
        return waqC010001Un;
    }

    public void setWaqC010001Un(Integer waqC010001Un) {
        this.waqC010001Un = waqC010001Un;
    }

    public BigDecimal getWaqC011001Va() {
        return waqC011001Va;
    }

    public void setWaqC011001Va(BigDecimal waqC011001Va) {
        this.waqC011001Va = waqC011001Va;
    }

    public Integer getWaqC011001Un() {
        return waqC011001Un;
    }

    public void setWaqC011001Un(Integer waqC011001Un) {
        this.waqC011001Un = waqC011001Un;
    }

    public BigDecimal getWaqC012001Va() {
        return waqC012001Va;
    }

    public void setWaqC012001Va(BigDecimal waqC012001Va) {
        this.waqC012001Va = waqC012001Va;
    }

    public Integer getWaqC012001Un() {
        return waqC012001Un;
    }

    public void setWaqC012001Un(Integer waqC012001Un) {
        this.waqC012001Un = waqC012001Un;
    }

    public BigDecimal getWaqC013001Va() {
        return waqC013001Va;
    }

    public void setWaqC013001Va(BigDecimal waqC013001Va) {
        this.waqC013001Va = waqC013001Va;
    }

    public Integer getWaqC013001Un() {
        return waqC013001Un;
    }

    public void setWaqC013001Un(Integer waqC013001Un) {
        this.waqC013001Un = waqC013001Un;
    }

    public BigDecimal getWaqC014001Va() {
        return waqC014001Va;
    }

    public void setWaqC014001Va(BigDecimal waqC014001Va) {
        this.waqC014001Va = waqC014001Va;
    }

    public Integer getWaqC014001Un() {
        return waqC014001Un;
    }

    public void setWaqC014001Un(Integer waqC014001Un) {
        this.waqC014001Un = waqC014001Un;
    }

    public BigDecimal getWaqC015001Va() {
        return waqC015001Va;
    }

    public void setWaqC015001Va(BigDecimal waqC015001Va) {
        this.waqC015001Va = waqC015001Va;
    }

    public Integer getWaqC015001Un() {
        return waqC015001Un;
    }

    public void setWaqC015001Un(Integer waqC015001Un) {
        this.waqC015001Un = waqC015001Un;
    }

    public BigDecimal getWaqC016001Va() {
        return waqC016001Va;
    }

    public void setWaqC016001Va(BigDecimal waqC016001Va) {
        this.waqC016001Va = waqC016001Va;
    }

    public Integer getWaqC016001Un() {
        return waqC016001Un;
    }

    public void setWaqC016001Un(Integer waqC016001Un) {
        this.waqC016001Un = waqC016001Un;
    }

    public BigDecimal getWaqC017001Va() {
        return waqC017001Va;
    }

    public void setWaqC017001Va(BigDecimal waqC017001Va) {
        this.waqC017001Va = waqC017001Va;
    }

    public Integer getWaqC017001Un() {
        return waqC017001Un;
    }

    public void setWaqC017001Un(Integer waqC017001Un) {
        this.waqC017001Un = waqC017001Un;
    }

    public BigDecimal getWaqD001001Va() {
        return waqD001001Va;
    }

    public void setWaqD001001Va(BigDecimal waqD001001Va) {
        this.waqD001001Va = waqD001001Va;
    }

    public Integer getWaqD001001Un() {
        return waqD001001Un;
    }

    public void setWaqD001001Un(Integer waqD001001Un) {
        this.waqD001001Un = waqD001001Un;
    }

    public BigDecimal getWaqE001001Va() {
        return waqE001001Va;
    }

    public void setWaqE001001Va(BigDecimal waqE001001Va) {
        this.waqE001001Va = waqE001001Va;
    }

    public Integer getWaqE001001Un() {
        return waqE001001Un;
    }

    public void setWaqE001001Un(Integer waqE001001Un) {
        this.waqE001001Un = waqE001001Un;
    }

    public BigDecimal getWaqE002001Va() {
        return waqE002001Va;
    }

    public void setWaqE002001Va(BigDecimal waqE002001Va) {
        this.waqE002001Va = waqE002001Va;
    }

    public Integer getWaqE002001Un() {
        return waqE002001Un;
    }

    public void setWaqE002001Un(Integer waqE002001Un) {
        this.waqE002001Un = waqE002001Un;
    }

    public String getWaqF001001() {
        return waqF001001;
    }

    public void setWaqF001001(String waqF001001) {
        this.waqF001001 = waqF001001;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (waqId != null ? waqId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbWaterQuality)) {
            return false;
        }
        TbWaterQuality other = (TbWaterQuality) object;
        if ((this.waqId == null && other.waqId != null) || (this.waqId != null && !this.waqId.equals(other.waqId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbWaterQuality[ waqId=" + waqId + " ]";
    }
    
}
