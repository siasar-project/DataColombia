/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_interv_sist_sane_melhor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbIntervSistSaneMelhor.findAll", query = "SELECT t FROM TbIntervSistSaneMelhor t")
    , @NamedQuery(name = "TbIntervSistSaneMelhor.findByIntssmSeq", query = "SELECT t FROM TbIntervSistSaneMelhor t WHERE t.intssmSeq = :intssmSeq")
    , @NamedQuery(name = "TbIntervSistSaneMelhor.findByIntssmFonte", query = "SELECT t FROM TbIntervSistSaneMelhor t WHERE t.intssmFonte = :intssmFonte")
    , @NamedQuery(name = "TbIntervSistSaneMelhor.findByDateLastUpdate", query = "SELECT t FROM TbIntervSistSaneMelhor t WHERE t.dateLastUpdate = :dateLastUpdate")})
public class TbIntervSistSaneMelhor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "intssm_seq")
    private Long intssmSeq;
    @Size(max = 255)
    @Column(name = "intssm_fonte")
    private String intssmFonte;
    @Column(name = "date_last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdate;
    @JoinColumn(name = "comun_seq", referencedColumnName = "comun_seq")
    @ManyToOne
    private TbComunidade comunSeq;
    @JoinColumn(name = "taxt_seq", referencedColumnName = "taxt_seq")
    @ManyToOne
    private TbTaxoTermo taxtSeq;

    public TbIntervSistSaneMelhor() {
    }

    public TbIntervSistSaneMelhor(Long intssmSeq) {
        this.intssmSeq = intssmSeq;
    }

    public Long getIntssmSeq() {
        return intssmSeq;
    }

    public void setIntssmSeq(Long intssmSeq) {
        this.intssmSeq = intssmSeq;
    }

    public String getIntssmFonte() {
        return intssmFonte;
    }

    public void setIntssmFonte(String intssmFonte) {
        this.intssmFonte = intssmFonte;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    public TbComunidade getComunSeq() {
        return comunSeq;
    }

    public void setComunSeq(TbComunidade comunSeq) {
        this.comunSeq = comunSeq;
    }

    public TbTaxoTermo getTaxtSeq() {
        return taxtSeq;
    }

    public void setTaxtSeq(TbTaxoTermo taxtSeq) {
        this.taxtSeq = taxtSeq;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (intssmSeq != null ? intssmSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbIntervSistSaneMelhor)) {
            return false;
        }
        TbIntervSistSaneMelhor other = (TbIntervSistSaneMelhor) object;
        if ((this.intssmSeq == null && other.intssmSeq != null) || (this.intssmSeq != null && !this.intssmSeq.equals(other.intssmSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbIntervSistSaneMelhor[ intssmSeq=" + intssmSeq + " ]";
    }
    
}
