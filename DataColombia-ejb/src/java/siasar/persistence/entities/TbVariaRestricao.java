/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_varia_restricao")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbVariaRestricao.findAll", query = "SELECT t FROM TbVariaRestricao t")
    , @NamedQuery(name = "TbVariaRestricao.findByVaresSeq", query = "SELECT t FROM TbVariaRestricao t WHERE t.varesSeq = :varesSeq")
    , @NamedQuery(name = "TbVariaRestricao.findByVaresFormula", query = "SELECT t FROM TbVariaRestricao t WHERE t.varesFormula = :varesFormula")
    , @NamedQuery(name = "TbVariaRestricao.findByVaresValor", query = "SELECT t FROM TbVariaRestricao t WHERE t.varesValor = :varesValor")
    , @NamedQuery(name = "TbVariaRestricao.findByVaresErro", query = "SELECT t FROM TbVariaRestricao t WHERE t.varesErro = :varesErro")
    , @NamedQuery(name = "TbVariaRestricao.findByVaresAtivo", query = "SELECT t FROM TbVariaRestricao t WHERE t.varesAtivo = :varesAtivo")})
public class TbVariaRestricao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "vares_seq")
    private Long varesSeq;
    @Size(max = 1000)
    @Column(name = "vares_formula")
    private String varesFormula;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "vares_valor")
    private BigDecimal varesValor;
    @Size(max = 2147483647)
    @Column(name = "vares_erro")
    private String varesErro;
    @Column(name = "vares_ativo")
    private Boolean varesAtivo;
    @JoinColumn(name = "varia_seq", referencedColumnName = "varia_seq")
    @ManyToOne
    private TbVariavel variaSeq;

    public TbVariaRestricao() {
    }

    public TbVariaRestricao(Long varesSeq) {
        this.varesSeq = varesSeq;
    }

    public Long getVaresSeq() {
        return varesSeq;
    }

    public void setVaresSeq(Long varesSeq) {
        this.varesSeq = varesSeq;
    }

    public String getVaresFormula() {
        return varesFormula;
    }

    public void setVaresFormula(String varesFormula) {
        this.varesFormula = varesFormula;
    }

    public BigDecimal getVaresValor() {
        return varesValor;
    }

    public void setVaresValor(BigDecimal varesValor) {
        this.varesValor = varesValor;
    }

    public String getVaresErro() {
        return varesErro;
    }

    public void setVaresErro(String varesErro) {
        this.varesErro = varesErro;
    }

    public Boolean getVaresAtivo() {
        return varesAtivo;
    }

    public void setVaresAtivo(Boolean varesAtivo) {
        this.varesAtivo = varesAtivo;
    }

    public TbVariavel getVariaSeq() {
        return variaSeq;
    }

    public void setVariaSeq(TbVariavel variaSeq) {
        this.variaSeq = variaSeq;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (varesSeq != null ? varesSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbVariaRestricao)) {
            return false;
        }
        TbVariaRestricao other = (TbVariaRestricao) object;
        if ((this.varesSeq == null && other.varesSeq != null) || (this.varesSeq != null && !this.varesSeq.equals(other.varesSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbVariaRestricao[ varesSeq=" + varesSeq + " ]";
    }
    
}
