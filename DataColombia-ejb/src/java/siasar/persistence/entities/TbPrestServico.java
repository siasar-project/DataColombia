/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_prest_servico")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbPrestServico.findAll", query = "SELECT t FROM TbPrestServico t")
    , @NamedQuery(name = "TbPrestServico.findByPrserSeq", query = "SELECT t FROM TbPrestServico t WHERE t.prserSeq = :prserSeq")
    , @NamedQuery(name = "TbPrestServico.findByValidado", query = "SELECT t FROM TbPrestServico t WHERE t.validado = :validado")
    , @NamedQuery(name = "TbPrestServico.findByDataValidacao", query = "SELECT t FROM TbPrestServico t WHERE t.dataValidacao = :dataValidacao")
    , @NamedQuery(name = "TbPrestServico.findBySepA001001", query = "SELECT t FROM TbPrestServico t WHERE t.sepA001001 = :sepA001001")
    , @NamedQuery(name = "TbPrestServico.findBySepA002001", query = "SELECT t FROM TbPrestServico t WHERE t.sepA002001 = :sepA002001")
    , @NamedQuery(name = "TbPrestServico.findBySepA003001", query = "SELECT t FROM TbPrestServico t WHERE t.sepA003001 = :sepA003001")
    , @NamedQuery(name = "TbPrestServico.findBySepA004001", query = "SELECT t FROM TbPrestServico t WHERE t.sepA004001 = :sepA004001")
    , @NamedQuery(name = "TbPrestServico.findBySepA005001", query = "SELECT t FROM TbPrestServico t WHERE t.sepA005001 = :sepA005001")
    , @NamedQuery(name = "TbPrestServico.findBySepA006001", query = "SELECT t FROM TbPrestServico t WHERE t.sepA006001 = :sepA006001")
    , @NamedQuery(name = "TbPrestServico.findBySepA007001", query = "SELECT t FROM TbPrestServico t WHERE t.sepA007001 = :sepA007001")
    , @NamedQuery(name = "TbPrestServico.findBySepA008001", query = "SELECT t FROM TbPrestServico t WHERE t.sepA008001 = :sepA008001")
    , @NamedQuery(name = "TbPrestServico.findBySepA009001", query = "SELECT t FROM TbPrestServico t WHERE t.sepA009001 = :sepA009001")
    , @NamedQuery(name = "TbPrestServico.findBySepA010001", query = "SELECT t FROM TbPrestServico t WHERE t.sepA010001 = :sepA010001")
    , @NamedQuery(name = "TbPrestServico.findBySepA011001", query = "SELECT t FROM TbPrestServico t WHERE t.sepA011001 = :sepA011001")
    , @NamedQuery(name = "TbPrestServico.findBySepA012001", query = "SELECT t FROM TbPrestServico t WHERE t.sepA012001 = :sepA012001")
    , @NamedQuery(name = "TbPrestServico.findBySepB001001", query = "SELECT t FROM TbPrestServico t WHERE t.sepB001001 = :sepB001001")
    , @NamedQuery(name = "TbPrestServico.findBySepB002001", query = "SELECT t FROM TbPrestServico t WHERE t.sepB002001 = :sepB002001")
    , @NamedQuery(name = "TbPrestServico.findBySepB003001", query = "SELECT t FROM TbPrestServico t WHERE t.sepB003001 = :sepB003001")
    , @NamedQuery(name = "TbPrestServico.findBySepB004001", query = "SELECT t FROM TbPrestServico t WHERE t.sepB004001 = :sepB004001")
    , @NamedQuery(name = "TbPrestServico.findBySepB005001", query = "SELECT t FROM TbPrestServico t WHERE t.sepB005001 = :sepB005001")
    , @NamedQuery(name = "TbPrestServico.findBySepB014001", query = "SELECT t FROM TbPrestServico t WHERE t.sepB014001 = :sepB014001")
    , @NamedQuery(name = "TbPrestServico.findBySepB015001", query = "SELECT t FROM TbPrestServico t WHERE t.sepB015001 = :sepB015001")
    , @NamedQuery(name = "TbPrestServico.findBySepB016001", query = "SELECT t FROM TbPrestServico t WHERE t.sepB016001 = :sepB016001")
    , @NamedQuery(name = "TbPrestServico.findBySepC001001", query = "SELECT t FROM TbPrestServico t WHERE t.sepC001001 = :sepC001001")
    , @NamedQuery(name = "TbPrestServico.findBySepC002001", query = "SELECT t FROM TbPrestServico t WHERE t.sepC002001 = :sepC002001")
    , @NamedQuery(name = "TbPrestServico.findBySepC003001", query = "SELECT t FROM TbPrestServico t WHERE t.sepC003001 = :sepC003001")
    , @NamedQuery(name = "TbPrestServico.findBySepC004001", query = "SELECT t FROM TbPrestServico t WHERE t.sepC004001 = :sepC004001")
    , @NamedQuery(name = "TbPrestServico.findBySepC005001", query = "SELECT t FROM TbPrestServico t WHERE t.sepC005001 = :sepC005001")
    , @NamedQuery(name = "TbPrestServico.findBySepC006001", query = "SELECT t FROM TbPrestServico t WHERE t.sepC006001 = :sepC006001")
    , @NamedQuery(name = "TbPrestServico.findBySepC007001", query = "SELECT t FROM TbPrestServico t WHERE t.sepC007001 = :sepC007001")
    , @NamedQuery(name = "TbPrestServico.findBySepC008001", query = "SELECT t FROM TbPrestServico t WHERE t.sepC008001 = :sepC008001")
    , @NamedQuery(name = "TbPrestServico.findBySepC009001", query = "SELECT t FROM TbPrestServico t WHERE t.sepC009001 = :sepC009001")
    , @NamedQuery(name = "TbPrestServico.findBySepC010001", query = "SELECT t FROM TbPrestServico t WHERE t.sepC010001 = :sepC010001")
    , @NamedQuery(name = "TbPrestServico.findBySepC011001", query = "SELECT t FROM TbPrestServico t WHERE t.sepC011001 = :sepC011001")
    , @NamedQuery(name = "TbPrestServico.findBySepC012001", query = "SELECT t FROM TbPrestServico t WHERE t.sepC012001 = :sepC012001")
    , @NamedQuery(name = "TbPrestServico.findBySepC013001", query = "SELECT t FROM TbPrestServico t WHERE t.sepC013001 = :sepC013001")
    , @NamedQuery(name = "TbPrestServico.findBySepC014001", query = "SELECT t FROM TbPrestServico t WHERE t.sepC014001 = :sepC014001")
    , @NamedQuery(name = "TbPrestServico.findBySepC015001", query = "SELECT t FROM TbPrestServico t WHERE t.sepC015001 = :sepC015001")
    , @NamedQuery(name = "TbPrestServico.findBySepC016001", query = "SELECT t FROM TbPrestServico t WHERE t.sepC016001 = :sepC016001")
    , @NamedQuery(name = "TbPrestServico.findBySepC017001", query = "SELECT t FROM TbPrestServico t WHERE t.sepC017001 = :sepC017001")
    , @NamedQuery(name = "TbPrestServico.findBySepC018001", query = "SELECT t FROM TbPrestServico t WHERE t.sepC018001 = :sepC018001")
    , @NamedQuery(name = "TbPrestServico.findBySepC019001", query = "SELECT t FROM TbPrestServico t WHERE t.sepC019001 = :sepC019001")
    , @NamedQuery(name = "TbPrestServico.findBySepD001001", query = "SELECT t FROM TbPrestServico t WHERE t.sepD001001 = :sepD001001")
    , @NamedQuery(name = "TbPrestServico.findBySepD002001", query = "SELECT t FROM TbPrestServico t WHERE t.sepD002001 = :sepD002001")
    , @NamedQuery(name = "TbPrestServico.findBySepD003001", query = "SELECT t FROM TbPrestServico t WHERE t.sepD003001 = :sepD003001")
    , @NamedQuery(name = "TbPrestServico.findBySepD004001", query = "SELECT t FROM TbPrestServico t WHERE t.sepD004001 = :sepD004001")
    , @NamedQuery(name = "TbPrestServico.findBySepD005001", query = "SELECT t FROM TbPrestServico t WHERE t.sepD005001 = :sepD005001")
    , @NamedQuery(name = "TbPrestServico.findBySepD006001", query = "SELECT t FROM TbPrestServico t WHERE t.sepD006001 = :sepD006001")
    , @NamedQuery(name = "TbPrestServico.findBySepD007001", query = "SELECT t FROM TbPrestServico t WHERE t.sepD007001 = :sepD007001")
    , @NamedQuery(name = "TbPrestServico.findBySepD008001", query = "SELECT t FROM TbPrestServico t WHERE t.sepD008001 = :sepD008001")
    , @NamedQuery(name = "TbPrestServico.findBySepD009001", query = "SELECT t FROM TbPrestServico t WHERE t.sepD009001 = :sepD009001")
    , @NamedQuery(name = "TbPrestServico.findBySepD010001", query = "SELECT t FROM TbPrestServico t WHERE t.sepD010001 = :sepD010001")
    , @NamedQuery(name = "TbPrestServico.findBySepD011001", query = "SELECT t FROM TbPrestServico t WHERE t.sepD011001 = :sepD011001")
    , @NamedQuery(name = "TbPrestServico.findBySepD012001", query = "SELECT t FROM TbPrestServico t WHERE t.sepD012001 = :sepD012001")
    , @NamedQuery(name = "TbPrestServico.findBySepE001001", query = "SELECT t FROM TbPrestServico t WHERE t.sepE001001 = :sepE001001")
    , @NamedQuery(name = "TbPrestServico.findBySepE002001", query = "SELECT t FROM TbPrestServico t WHERE t.sepE002001 = :sepE002001")
    , @NamedQuery(name = "TbPrestServico.findBySepE003001", query = "SELECT t FROM TbPrestServico t WHERE t.sepE003001 = :sepE003001")
    , @NamedQuery(name = "TbPrestServico.findBySepE004001", query = "SELECT t FROM TbPrestServico t WHERE t.sepE004001 = :sepE004001")
    , @NamedQuery(name = "TbPrestServico.findBySepE005001", query = "SELECT t FROM TbPrestServico t WHERE t.sepE005001 = :sepE005001")
    , @NamedQuery(name = "TbPrestServico.findBySepE006001", query = "SELECT t FROM TbPrestServico t WHERE t.sepE006001 = :sepE006001")
    , @NamedQuery(name = "TbPrestServico.findBySepE007001", query = "SELECT t FROM TbPrestServico t WHERE t.sepE007001 = :sepE007001")
    , @NamedQuery(name = "TbPrestServico.findBySepE008001", query = "SELECT t FROM TbPrestServico t WHERE t.sepE008001 = :sepE008001")
    , @NamedQuery(name = "TbPrestServico.findBySepE009001", query = "SELECT t FROM TbPrestServico t WHERE t.sepE009001 = :sepE009001")
    , @NamedQuery(name = "TbPrestServico.findBySepE010001", query = "SELECT t FROM TbPrestServico t WHERE t.sepE010001 = :sepE010001")
    , @NamedQuery(name = "TbPrestServico.findBySepE011001", query = "SELECT t FROM TbPrestServico t WHERE t.sepE011001 = :sepE011001")
    , @NamedQuery(name = "TbPrestServico.findBySepE012001", query = "SELECT t FROM TbPrestServico t WHERE t.sepE012001 = :sepE012001")
    , @NamedQuery(name = "TbPrestServico.findBySepE013001", query = "SELECT t FROM TbPrestServico t WHERE t.sepE013001 = :sepE013001")
    , @NamedQuery(name = "TbPrestServico.findBySepE014001", query = "SELECT t FROM TbPrestServico t WHERE t.sepE014001 = :sepE014001")
    , @NamedQuery(name = "TbPrestServico.findBySepE015001", query = "SELECT t FROM TbPrestServico t WHERE t.sepE015001 = :sepE015001")
    , @NamedQuery(name = "TbPrestServico.findBySepE016001", query = "SELECT t FROM TbPrestServico t WHERE t.sepE016001 = :sepE016001")
    , @NamedQuery(name = "TbPrestServico.findBySepE018001", query = "SELECT t FROM TbPrestServico t WHERE t.sepE018001 = :sepE018001")
    , @NamedQuery(name = "TbPrestServico.findBySepE019001", query = "SELECT t FROM TbPrestServico t WHERE t.sepE019001 = :sepE019001")
    , @NamedQuery(name = "TbPrestServico.findBySepE020001", query = "SELECT t FROM TbPrestServico t WHERE t.sepE020001 = :sepE020001")
    , @NamedQuery(name = "TbPrestServico.findBySepE021001", query = "SELECT t FROM TbPrestServico t WHERE t.sepE021001 = :sepE021001")
    , @NamedQuery(name = "TbPrestServico.findBySepF001001", query = "SELECT t FROM TbPrestServico t WHERE t.sepF001001 = :sepF001001")
    , @NamedQuery(name = "TbPrestServico.findBySepF003001", query = "SELECT t FROM TbPrestServico t WHERE t.sepF003001 = :sepF003001")
    , @NamedQuery(name = "TbPrestServico.findBySepF004001", query = "SELECT t FROM TbPrestServico t WHERE t.sepF004001 = :sepF004001")
    , @NamedQuery(name = "TbPrestServico.findBySepF005001", query = "SELECT t FROM TbPrestServico t WHERE t.sepF005001 = :sepF005001")
    , @NamedQuery(name = "TbPrestServico.findBySepF006001", query = "SELECT t FROM TbPrestServico t WHERE t.sepF006001 = :sepF006001")
    , @NamedQuery(name = "TbPrestServico.findBySepF007001", query = "SELECT t FROM TbPrestServico t WHERE t.sepF007001 = :sepF007001")
    , @NamedQuery(name = "TbPrestServico.findBySepF008001", query = "SELECT t FROM TbPrestServico t WHERE t.sepF008001 = :sepF008001")
    , @NamedQuery(name = "TbPrestServico.findBySepF009001", query = "SELECT t FROM TbPrestServico t WHERE t.sepF009001 = :sepF009001")
    , @NamedQuery(name = "TbPrestServico.findBySepF010001", query = "SELECT t FROM TbPrestServico t WHERE t.sepF010001 = :sepF010001")
    , @NamedQuery(name = "TbPrestServico.findBySepF011001", query = "SELECT t FROM TbPrestServico t WHERE t.sepF011001 = :sepF011001")
    , @NamedQuery(name = "TbPrestServico.findBySepF012001", query = "SELECT t FROM TbPrestServico t WHERE t.sepF012001 = :sepF012001")
    , @NamedQuery(name = "TbPrestServico.findBySepF013001", query = "SELECT t FROM TbPrestServico t WHERE t.sepF013001 = :sepF013001")
    , @NamedQuery(name = "TbPrestServico.findBySepF014001", query = "SELECT t FROM TbPrestServico t WHERE t.sepF014001 = :sepF014001")
    , @NamedQuery(name = "TbPrestServico.findBySepF015001", query = "SELECT t FROM TbPrestServico t WHERE t.sepF015001 = :sepF015001")
    , @NamedQuery(name = "TbPrestServico.findBySepF016001", query = "SELECT t FROM TbPrestServico t WHERE t.sepF016001 = :sepF016001")
    , @NamedQuery(name = "TbPrestServico.findBySepF017001", query = "SELECT t FROM TbPrestServico t WHERE t.sepF017001 = :sepF017001")
    , @NamedQuery(name = "TbPrestServico.findBySepF018001", query = "SELECT t FROM TbPrestServico t WHERE t.sepF018001 = :sepF018001")
    , @NamedQuery(name = "TbPrestServico.findBySepG001001", query = "SELECT t FROM TbPrestServico t WHERE t.sepG001001 = :sepG001001")
    , @NamedQuery(name = "TbPrestServico.findBySepG002001", query = "SELECT t FROM TbPrestServico t WHERE t.sepG002001 = :sepG002001")
    , @NamedQuery(name = "TbPrestServico.findBySepG003001", query = "SELECT t FROM TbPrestServico t WHERE t.sepG003001 = :sepG003001")
    , @NamedQuery(name = "TbPrestServico.findBySepG004001", query = "SELECT t FROM TbPrestServico t WHERE t.sepG004001 = :sepG004001")
    , @NamedQuery(name = "TbPrestServico.findBySepH001001", query = "SELECT t FROM TbPrestServico t WHERE t.sepH001001 = :sepH001001")
    , @NamedQuery(name = "TbPrestServico.findBySepH002001", query = "SELECT t FROM TbPrestServico t WHERE t.sepH002001 = :sepH002001")
    , @NamedQuery(name = "TbPrestServico.findBySepH003001", query = "SELECT t FROM TbPrestServico t WHERE t.sepH003001 = :sepH003001")
    , @NamedQuery(name = "TbPrestServico.findBySepH004001", query = "SELECT t FROM TbPrestServico t WHERE t.sepH004001 = :sepH004001")
    , @NamedQuery(name = "TbPrestServico.findBySepH005001", query = "SELECT t FROM TbPrestServico t WHERE t.sepH005001 = :sepH005001")
    , @NamedQuery(name = "TbPrestServico.findBySepH006001", query = "SELECT t FROM TbPrestServico t WHERE t.sepH006001 = :sepH006001")
    , @NamedQuery(name = "TbPrestServico.findBySepH007001", query = "SELECT t FROM TbPrestServico t WHERE t.sepH007001 = :sepH007001")
    , @NamedQuery(name = "TbPrestServico.findBySepH008001", query = "SELECT t FROM TbPrestServico t WHERE t.sepH008001 = :sepH008001")
    , @NamedQuery(name = "TbPrestServico.findBySepH009001", query = "SELECT t FROM TbPrestServico t WHERE t.sepH009001 = :sepH009001")
    , @NamedQuery(name = "TbPrestServico.findBySepH010001", query = "SELECT t FROM TbPrestServico t WHERE t.sepH010001 = :sepH010001")
    , @NamedQuery(name = "TbPrestServico.findBySepH011001", query = "SELECT t FROM TbPrestServico t WHERE t.sepH011001 = :sepH011001")
    , @NamedQuery(name = "TbPrestServico.findBySepH012001", query = "SELECT t FROM TbPrestServico t WHERE t.sepH012001 = :sepH012001")
    , @NamedQuery(name = "TbPrestServico.findBySepH013001", query = "SELECT t FROM TbPrestServico t WHERE t.sepH013001 = :sepH013001")
    , @NamedQuery(name = "TbPrestServico.findBySepH014001", query = "SELECT t FROM TbPrestServico t WHERE t.sepH014001 = :sepH014001")
    , @NamedQuery(name = "TbPrestServico.findBySepH015001", query = "SELECT t FROM TbPrestServico t WHERE t.sepH015001 = :sepH015001")
    , @NamedQuery(name = "TbPrestServico.findBySepH016001", query = "SELECT t FROM TbPrestServico t WHERE t.sepH016001 = :sepH016001")
    , @NamedQuery(name = "TbPrestServico.findBySepH017001", query = "SELECT t FROM TbPrestServico t WHERE t.sepH017001 = :sepH017001")
    , @NamedQuery(name = "TbPrestServico.findBySepH018001", query = "SELECT t FROM TbPrestServico t WHERE t.sepH018001 = :sepH018001")
    , @NamedQuery(name = "TbPrestServico.findBySepI001001", query = "SELECT t FROM TbPrestServico t WHERE t.sepI001001 = :sepI001001")
    , @NamedQuery(name = "TbPrestServico.findByUrlImagem", query = "SELECT t FROM TbPrestServico t WHERE t.urlImagem = :urlImagem")
    , @NamedQuery(name = "TbPrestServico.findByClassificacao", query = "SELECT t FROM TbPrestServico t WHERE t.classificacao = :classificacao")
    , @NamedQuery(name = "TbPrestServico.findByPaisSigla", query = "SELECT t FROM TbPrestServico t WHERE t.paisSigla = :paisSigla")
    , @NamedQuery(name = "TbPrestServico.findByDateLastUpdate", query = "SELECT t FROM TbPrestServico t WHERE t.dateLastUpdate = :dateLastUpdate")})
public class TbPrestServico implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "prser_seq")
    private Long prserSeq;
    @Column(name = "validado")
    private Boolean validado;
    @Column(name = "data_validacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataValidacao;
    @Size(max = 2147483647)
    @Column(name = "sep_a_001_001")
    private String sepA001001;
    @Column(name = "sep_a_002_001")
    private Integer sepA002001;
    @Column(name = "sep_a_003_001")
    private Integer sepA003001;
    @Size(max = 2)
    @Column(name = "sep_a_004_001")
    private String sepA004001;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "sep_a_005_001")
    private Double sepA005001;
    @Column(name = "sep_a_006_001")
    private Double sepA006001;
    @Column(name = "sep_a_007_001")
    private Double sepA007001;
    @Size(max = 255)
    @Column(name = "sep_a_008_001")
    private String sepA008001;
    @Column(name = "sep_a_009_001")
    private Integer sepA009001;
    @Size(max = 2147483647)
    @Column(name = "sep_a_010_001")
    private String sepA010001;
    @Column(name = "sep_a_011_001")
    @Temporal(TemporalType.DATE)
    private Date sepA011001;
    @Size(max = 255)
    @Column(name = "sep_a_012_001")
    private String sepA012001;
    @Column(name = "sep_b_001_001")
    @Temporal(TemporalType.DATE)
    private Date sepB001001;
    @Column(name = "sep_b_002_001")
    private Integer sepB002001;
    @Column(name = "sep_b_003_001")
    @Temporal(TemporalType.DATE)
    private Date sepB003001;
    @Column(name = "sep_b_004_001")
    private Boolean sepB004001;
    @Column(name = "sep_b_005_001")
    private Integer sepB005001;
    @Column(name = "sep_b_014_001")
    private Boolean sepB014001;
    @Column(name = "sep_b_015_001")
    private Boolean sepB015001;
    @Column(name = "sep_b_016_001")
    private Boolean sepB016001;
    @Column(name = "sep_c_001_001")
    private Boolean sepC001001;
    @Column(name = "sep_c_002_001")
    private Integer sepC002001;
    @Column(name = "sep_c_003_001")
    private Integer sepC003001;
    @Column(name = "sep_c_004_001")
    private BigDecimal sepC004001;
    @Column(name = "sep_c_005_001")
    private Integer sepC005001;
    @Size(max = 2147483647)
    @Column(name = "sep_c_006_001")
    private String sepC006001;
    @Column(name = "sep_c_007_001")
    private Boolean sepC007001;
    @Column(name = "sep_c_008_001")
    private BigDecimal sepC008001;
    @Column(name = "sep_c_009_001")
    private Integer sepC009001;
    @Column(name = "sep_c_010_001")
    private BigDecimal sepC010001;
    @Column(name = "sep_c_011_001")
    private Integer sepC011001;
    @Column(name = "sep_c_012_001")
    private Integer sepC012001;
    @Column(name = "sep_c_013_001")
    private Integer sepC013001;
    @Column(name = "sep_c_014_001")
    private BigDecimal sepC014001;
    @Column(name = "sep_c_015_001")
    private Integer sepC015001;
    @Column(name = "sep_c_016_001")
    private Integer sepC016001;
    @Column(name = "sep_c_017_001")
    private BigDecimal sepC017001;
    @Column(name = "sep_c_018_001")
    private Integer sepC018001;
    @Size(max = 2147483647)
    @Column(name = "sep_c_019_001")
    private String sepC019001;
    @Column(name = "sep_d_001_001")
    private Boolean sepD001001;
    @Column(name = "sep_d_002_001")
    private Integer sepD002001;
    @Column(name = "sep_d_003_001")
    private BigDecimal sepD003001;
    @Column(name = "sep_d_004_001")
    private Integer sepD004001;
    @Column(name = "sep_d_005_001")
    private BigDecimal sepD005001;
    @Column(name = "sep_d_006_001")
    private Boolean sepD006001;
    @Column(name = "sep_d_007_001")
    private Integer sepD007001;
    @Column(name = "sep_d_008_001")
    private BigDecimal sepD008001;
    @Column(name = "sep_d_009_001")
    private Integer sepD009001;
    @Column(name = "sep_d_010_001")
    private BigDecimal sepD010001;
    @Column(name = "sep_d_011_001")
    private Integer sepD011001;
    @Column(name = "sep_d_012_001")
    private BigDecimal sepD012001;
    @Column(name = "sep_e_001_001")
    private Integer sepE001001;
    @Column(name = "sep_e_002_001")
    private BigDecimal sepE002001;
    @Column(name = "sep_e_003_001")
    private Integer sepE003001;
    @Column(name = "sep_e_004_001")
    private BigDecimal sepE004001;
    @Column(name = "sep_e_005_001")
    private Integer sepE005001;
    @Column(name = "sep_e_006_001")
    private BigDecimal sepE006001;
    @Column(name = "sep_e_007_001")
    private Integer sepE007001;
    @Column(name = "sep_e_008_001")
    private BigDecimal sepE008001;
    @Column(name = "sep_e_009_001")
    private Integer sepE009001;
    @Column(name = "sep_e_010_001")
    private BigDecimal sepE010001;
    @Column(name = "sep_e_011_001")
    private Integer sepE011001;
    @Column(name = "sep_e_012_001")
    private BigDecimal sepE012001;
    @Column(name = "sep_e_013_001")
    private Integer sepE013001;
    @Column(name = "sep_e_014_001")
    private BigDecimal sepE014001;
    @Column(name = "sep_e_015_001")
    private Integer sepE015001;
    @Column(name = "sep_e_016_001")
    private BigDecimal sepE016001;
    @Column(name = "sep_e_018_001")
    private BigDecimal sepE018001;
    @Column(name = "sep_e_019_001")
    private Integer sepE019001;
    @Column(name = "sep_e_020_001")
    private BigDecimal sepE020001;
    @Column(name = "sep_e_021_001")
    private Integer sepE021001;
    @Column(name = "sep_f_001_001")
    private Boolean sepF001001;
    @Column(name = "sep_f_003_001")
    private Integer sepF003001;
    @Column(name = "sep_f_004_001")
    private BigDecimal sepF004001;
    @Column(name = "sep_f_005_001")
    private Integer sepF005001;
    @Column(name = "sep_f_006_001")
    private BigDecimal sepF006001;
    @Column(name = "sep_f_007_001")
    private Boolean sepF007001;
    @Column(name = "sep_f_008_001")
    private Integer sepF008001;
    @Column(name = "sep_f_009_001")
    private BigDecimal sepF009001;
    @Column(name = "sep_f_010_001")
    private Boolean sepF010001;
    @Column(name = "sep_f_011_001")
    private Integer sepF011001;
    @Column(name = "sep_f_012_001")
    private BigDecimal sepF012001;
    @Column(name = "sep_f_013_001")
    private Integer sepF013001;
    @Column(name = "sep_f_014_001")
    private BigDecimal sepF014001;
    @Column(name = "sep_f_015_001")
    private Integer sepF015001;
    @Column(name = "sep_f_016_001")
    private BigDecimal sepF016001;
    @Column(name = "sep_f_017_001")
    private Integer sepF017001;
    @Column(name = "sep_f_018_001")
    private BigDecimal sepF018001;
    @Column(name = "sep_g_001_001")
    private Integer sepG001001;
    @Column(name = "sep_g_002_001")
    private Boolean sepG002001;
    @Column(name = "sep_g_003_001")
    private Boolean sepG003001;
    @Column(name = "sep_g_004_001")
    private Integer sepG004001;
    @Column(name = "sep_h_001_001")
    private Boolean sepH001001;
    @Size(max = 2147483647)
    @Column(name = "sep_h_002_001")
    private String sepH002001;
    @Column(name = "sep_h_003_001")
    private Boolean sepH003001;
    @Column(name = "sep_h_004_001")
    private Boolean sepH004001;
    @Column(name = "sep_h_005_001")
    private Integer sepH005001;
    @Column(name = "sep_h_006_001")
    private Integer sepH006001;
    @Column(name = "sep_h_007_001")
    private Integer sepH007001;
    @Column(name = "sep_h_008_001")
    private Integer sepH008001;
    @Column(name = "sep_h_009_001")
    private Integer sepH009001;
    @Column(name = "sep_h_010_001")
    private Integer sepH010001;
    @Column(name = "sep_h_011_001")
    private Integer sepH011001;
    @Column(name = "sep_h_012_001")
    private Integer sepH012001;
    @Column(name = "sep_h_013_001")
    private Integer sepH013001;
    @Column(name = "sep_h_014_001")
    private Integer sepH014001;
    @Column(name = "sep_h_015_001")
    private Integer sepH015001;
    @Column(name = "sep_h_016_001")
    private Integer sepH016001;
    @Column(name = "sep_h_017_001")
    private Integer sepH017001;
    @Column(name = "sep_h_018_001")
    private Integer sepH018001;
    @Size(max = 2147483647)
    @Column(name = "sep_i_001_001")
    private String sepI001001;
    @Size(max = 4000)
    @Column(name = "url_imagem")
    private String urlImagem;
    @Size(max = 1)
    @Column(name = "classificacao")
    private String classificacao;
    @Size(max = 60)
    @Column(name = "pais_sigla")
    private String paisSigla;
    @Column(name = "date_last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdate;
    @OneToMany(mappedBy = "prserSeq")
    private List<TbDirTecnico> tbDirTecnicoList;
    @OneToMany(mappedBy = "prserSeq")
    private List<TbDirRepresentante> tbDirRepresentanteList;

    public TbPrestServico() {
    }

    public TbPrestServico(Long prserSeq) {
        this.prserSeq = prserSeq;
    }

    public Long getPrserSeq() {
        return prserSeq;
    }

    public void setPrserSeq(Long prserSeq) {
        this.prserSeq = prserSeq;
    }

    public Boolean getValidado() {
        return validado;
    }

    public void setValidado(Boolean validado) {
        this.validado = validado;
    }

    public Date getDataValidacao() {
        return dataValidacao;
    }

    public void setDataValidacao(Date dataValidacao) {
        this.dataValidacao = dataValidacao;
    }

    public String getSepA001001() {
        return sepA001001;
    }

    public void setSepA001001(String sepA001001) {
        this.sepA001001 = sepA001001;
    }

    public Integer getSepA002001() {
        return sepA002001;
    }

    public void setSepA002001(Integer sepA002001) {
        this.sepA002001 = sepA002001;
    }

    public Integer getSepA003001() {
        return sepA003001;
    }

    public void setSepA003001(Integer sepA003001) {
        this.sepA003001 = sepA003001;
    }

    public String getSepA004001() {
        return sepA004001;
    }

    public void setSepA004001(String sepA004001) {
        this.sepA004001 = sepA004001;
    }

    public Double getSepA005001() {
        return sepA005001;
    }

    public void setSepA005001(Double sepA005001) {
        this.sepA005001 = sepA005001;
    }

    public Double getSepA006001() {
        return sepA006001;
    }

    public void setSepA006001(Double sepA006001) {
        this.sepA006001 = sepA006001;
    }

    public Double getSepA007001() {
        return sepA007001;
    }

    public void setSepA007001(Double sepA007001) {
        this.sepA007001 = sepA007001;
    }

    public String getSepA008001() {
        return sepA008001;
    }

    public void setSepA008001(String sepA008001) {
        this.sepA008001 = sepA008001;
    }

    public Integer getSepA009001() {
        return sepA009001;
    }

    public void setSepA009001(Integer sepA009001) {
        this.sepA009001 = sepA009001;
    }

    public String getSepA010001() {
        return sepA010001;
    }

    public void setSepA010001(String sepA010001) {
        this.sepA010001 = sepA010001;
    }

    public Date getSepA011001() {
        return sepA011001;
    }

    public void setSepA011001(Date sepA011001) {
        this.sepA011001 = sepA011001;
    }

    public String getSepA012001() {
        return sepA012001;
    }

    public void setSepA012001(String sepA012001) {
        this.sepA012001 = sepA012001;
    }

    public Date getSepB001001() {
        return sepB001001;
    }

    public void setSepB001001(Date sepB001001) {
        this.sepB001001 = sepB001001;
    }

    public Integer getSepB002001() {
        return sepB002001;
    }

    public void setSepB002001(Integer sepB002001) {
        this.sepB002001 = sepB002001;
    }

    public Date getSepB003001() {
        return sepB003001;
    }

    public void setSepB003001(Date sepB003001) {
        this.sepB003001 = sepB003001;
    }

    public Boolean getSepB004001() {
        return sepB004001;
    }

    public void setSepB004001(Boolean sepB004001) {
        this.sepB004001 = sepB004001;
    }

    public Integer getSepB005001() {
        return sepB005001;
    }

    public void setSepB005001(Integer sepB005001) {
        this.sepB005001 = sepB005001;
    }

    public Boolean getSepB014001() {
        return sepB014001;
    }

    public void setSepB014001(Boolean sepB014001) {
        this.sepB014001 = sepB014001;
    }

    public Boolean getSepB015001() {
        return sepB015001;
    }

    public void setSepB015001(Boolean sepB015001) {
        this.sepB015001 = sepB015001;
    }

    public Boolean getSepB016001() {
        return sepB016001;
    }

    public void setSepB016001(Boolean sepB016001) {
        this.sepB016001 = sepB016001;
    }

    public Boolean getSepC001001() {
        return sepC001001;
    }

    public void setSepC001001(Boolean sepC001001) {
        this.sepC001001 = sepC001001;
    }

    public Integer getSepC002001() {
        return sepC002001;
    }

    public void setSepC002001(Integer sepC002001) {
        this.sepC002001 = sepC002001;
    }

    public Integer getSepC003001() {
        return sepC003001;
    }

    public void setSepC003001(Integer sepC003001) {
        this.sepC003001 = sepC003001;
    }

    public BigDecimal getSepC004001() {
        return sepC004001;
    }

    public void setSepC004001(BigDecimal sepC004001) {
        this.sepC004001 = sepC004001;
    }

    public Integer getSepC005001() {
        return sepC005001;
    }

    public void setSepC005001(Integer sepC005001) {
        this.sepC005001 = sepC005001;
    }

    public String getSepC006001() {
        return sepC006001;
    }

    public void setSepC006001(String sepC006001) {
        this.sepC006001 = sepC006001;
    }

    public Boolean getSepC007001() {
        return sepC007001;
    }

    public void setSepC007001(Boolean sepC007001) {
        this.sepC007001 = sepC007001;
    }

    public BigDecimal getSepC008001() {
        return sepC008001;
    }

    public void setSepC008001(BigDecimal sepC008001) {
        this.sepC008001 = sepC008001;
    }

    public Integer getSepC009001() {
        return sepC009001;
    }

    public void setSepC009001(Integer sepC009001) {
        this.sepC009001 = sepC009001;
    }

    public BigDecimal getSepC010001() {
        return sepC010001;
    }

    public void setSepC010001(BigDecimal sepC010001) {
        this.sepC010001 = sepC010001;
    }

    public Integer getSepC011001() {
        return sepC011001;
    }

    public void setSepC011001(Integer sepC011001) {
        this.sepC011001 = sepC011001;
    }

    public Integer getSepC012001() {
        return sepC012001;
    }

    public void setSepC012001(Integer sepC012001) {
        this.sepC012001 = sepC012001;
    }

    public Integer getSepC013001() {
        return sepC013001;
    }

    public void setSepC013001(Integer sepC013001) {
        this.sepC013001 = sepC013001;
    }

    public BigDecimal getSepC014001() {
        return sepC014001;
    }

    public void setSepC014001(BigDecimal sepC014001) {
        this.sepC014001 = sepC014001;
    }

    public Integer getSepC015001() {
        return sepC015001;
    }

    public void setSepC015001(Integer sepC015001) {
        this.sepC015001 = sepC015001;
    }

    public Integer getSepC016001() {
        return sepC016001;
    }

    public void setSepC016001(Integer sepC016001) {
        this.sepC016001 = sepC016001;
    }

    public BigDecimal getSepC017001() {
        return sepC017001;
    }

    public void setSepC017001(BigDecimal sepC017001) {
        this.sepC017001 = sepC017001;
    }

    public Integer getSepC018001() {
        return sepC018001;
    }

    public void setSepC018001(Integer sepC018001) {
        this.sepC018001 = sepC018001;
    }

    public String getSepC019001() {
        return sepC019001;
    }

    public void setSepC019001(String sepC019001) {
        this.sepC019001 = sepC019001;
    }

    public Boolean getSepD001001() {
        return sepD001001;
    }

    public void setSepD001001(Boolean sepD001001) {
        this.sepD001001 = sepD001001;
    }

    public Integer getSepD002001() {
        return sepD002001;
    }

    public void setSepD002001(Integer sepD002001) {
        this.sepD002001 = sepD002001;
    }

    public BigDecimal getSepD003001() {
        return sepD003001;
    }

    public void setSepD003001(BigDecimal sepD003001) {
        this.sepD003001 = sepD003001;
    }

    public Integer getSepD004001() {
        return sepD004001;
    }

    public void setSepD004001(Integer sepD004001) {
        this.sepD004001 = sepD004001;
    }

    public BigDecimal getSepD005001() {
        return sepD005001;
    }

    public void setSepD005001(BigDecimal sepD005001) {
        this.sepD005001 = sepD005001;
    }

    public Boolean getSepD006001() {
        return sepD006001;
    }

    public void setSepD006001(Boolean sepD006001) {
        this.sepD006001 = sepD006001;
    }

    public Integer getSepD007001() {
        return sepD007001;
    }

    public void setSepD007001(Integer sepD007001) {
        this.sepD007001 = sepD007001;
    }

    public BigDecimal getSepD008001() {
        return sepD008001;
    }

    public void setSepD008001(BigDecimal sepD008001) {
        this.sepD008001 = sepD008001;
    }

    public Integer getSepD009001() {
        return sepD009001;
    }

    public void setSepD009001(Integer sepD009001) {
        this.sepD009001 = sepD009001;
    }

    public BigDecimal getSepD010001() {
        return sepD010001;
    }

    public void setSepD010001(BigDecimal sepD010001) {
        this.sepD010001 = sepD010001;
    }

    public Integer getSepD011001() {
        return sepD011001;
    }

    public void setSepD011001(Integer sepD011001) {
        this.sepD011001 = sepD011001;
    }

    public BigDecimal getSepD012001() {
        return sepD012001;
    }

    public void setSepD012001(BigDecimal sepD012001) {
        this.sepD012001 = sepD012001;
    }

    public Integer getSepE001001() {
        return sepE001001;
    }

    public void setSepE001001(Integer sepE001001) {
        this.sepE001001 = sepE001001;
    }

    public BigDecimal getSepE002001() {
        return sepE002001;
    }

    public void setSepE002001(BigDecimal sepE002001) {
        this.sepE002001 = sepE002001;
    }

    public Integer getSepE003001() {
        return sepE003001;
    }

    public void setSepE003001(Integer sepE003001) {
        this.sepE003001 = sepE003001;
    }

    public BigDecimal getSepE004001() {
        return sepE004001;
    }

    public void setSepE004001(BigDecimal sepE004001) {
        this.sepE004001 = sepE004001;
    }

    public Integer getSepE005001() {
        return sepE005001;
    }

    public void setSepE005001(Integer sepE005001) {
        this.sepE005001 = sepE005001;
    }

    public BigDecimal getSepE006001() {
        return sepE006001;
    }

    public void setSepE006001(BigDecimal sepE006001) {
        this.sepE006001 = sepE006001;
    }

    public Integer getSepE007001() {
        return sepE007001;
    }

    public void setSepE007001(Integer sepE007001) {
        this.sepE007001 = sepE007001;
    }

    public BigDecimal getSepE008001() {
        return sepE008001;
    }

    public void setSepE008001(BigDecimal sepE008001) {
        this.sepE008001 = sepE008001;
    }

    public Integer getSepE009001() {
        return sepE009001;
    }

    public void setSepE009001(Integer sepE009001) {
        this.sepE009001 = sepE009001;
    }

    public BigDecimal getSepE010001() {
        return sepE010001;
    }

    public void setSepE010001(BigDecimal sepE010001) {
        this.sepE010001 = sepE010001;
    }

    public Integer getSepE011001() {
        return sepE011001;
    }

    public void setSepE011001(Integer sepE011001) {
        this.sepE011001 = sepE011001;
    }

    public BigDecimal getSepE012001() {
        return sepE012001;
    }

    public void setSepE012001(BigDecimal sepE012001) {
        this.sepE012001 = sepE012001;
    }

    public Integer getSepE013001() {
        return sepE013001;
    }

    public void setSepE013001(Integer sepE013001) {
        this.sepE013001 = sepE013001;
    }

    public BigDecimal getSepE014001() {
        return sepE014001;
    }

    public void setSepE014001(BigDecimal sepE014001) {
        this.sepE014001 = sepE014001;
    }

    public Integer getSepE015001() {
        return sepE015001;
    }

    public void setSepE015001(Integer sepE015001) {
        this.sepE015001 = sepE015001;
    }

    public BigDecimal getSepE016001() {
        return sepE016001;
    }

    public void setSepE016001(BigDecimal sepE016001) {
        this.sepE016001 = sepE016001;
    }

    public BigDecimal getSepE018001() {
        return sepE018001;
    }

    public void setSepE018001(BigDecimal sepE018001) {
        this.sepE018001 = sepE018001;
    }

    public Integer getSepE019001() {
        return sepE019001;
    }

    public void setSepE019001(Integer sepE019001) {
        this.sepE019001 = sepE019001;
    }

    public BigDecimal getSepE020001() {
        return sepE020001;
    }

    public void setSepE020001(BigDecimal sepE020001) {
        this.sepE020001 = sepE020001;
    }

    public Integer getSepE021001() {
        return sepE021001;
    }

    public void setSepE021001(Integer sepE021001) {
        this.sepE021001 = sepE021001;
    }

    public Boolean getSepF001001() {
        return sepF001001;
    }

    public void setSepF001001(Boolean sepF001001) {
        this.sepF001001 = sepF001001;
    }

    public Integer getSepF003001() {
        return sepF003001;
    }

    public void setSepF003001(Integer sepF003001) {
        this.sepF003001 = sepF003001;
    }

    public BigDecimal getSepF004001() {
        return sepF004001;
    }

    public void setSepF004001(BigDecimal sepF004001) {
        this.sepF004001 = sepF004001;
    }

    public Integer getSepF005001() {
        return sepF005001;
    }

    public void setSepF005001(Integer sepF005001) {
        this.sepF005001 = sepF005001;
    }

    public BigDecimal getSepF006001() {
        return sepF006001;
    }

    public void setSepF006001(BigDecimal sepF006001) {
        this.sepF006001 = sepF006001;
    }

    public Boolean getSepF007001() {
        return sepF007001;
    }

    public void setSepF007001(Boolean sepF007001) {
        this.sepF007001 = sepF007001;
    }

    public Integer getSepF008001() {
        return sepF008001;
    }

    public void setSepF008001(Integer sepF008001) {
        this.sepF008001 = sepF008001;
    }

    public BigDecimal getSepF009001() {
        return sepF009001;
    }

    public void setSepF009001(BigDecimal sepF009001) {
        this.sepF009001 = sepF009001;
    }

    public Boolean getSepF010001() {
        return sepF010001;
    }

    public void setSepF010001(Boolean sepF010001) {
        this.sepF010001 = sepF010001;
    }

    public Integer getSepF011001() {
        return sepF011001;
    }

    public void setSepF011001(Integer sepF011001) {
        this.sepF011001 = sepF011001;
    }

    public BigDecimal getSepF012001() {
        return sepF012001;
    }

    public void setSepF012001(BigDecimal sepF012001) {
        this.sepF012001 = sepF012001;
    }

    public Integer getSepF013001() {
        return sepF013001;
    }

    public void setSepF013001(Integer sepF013001) {
        this.sepF013001 = sepF013001;
    }

    public BigDecimal getSepF014001() {
        return sepF014001;
    }

    public void setSepF014001(BigDecimal sepF014001) {
        this.sepF014001 = sepF014001;
    }

    public Integer getSepF015001() {
        return sepF015001;
    }

    public void setSepF015001(Integer sepF015001) {
        this.sepF015001 = sepF015001;
    }

    public BigDecimal getSepF016001() {
        return sepF016001;
    }

    public void setSepF016001(BigDecimal sepF016001) {
        this.sepF016001 = sepF016001;
    }

    public Integer getSepF017001() {
        return sepF017001;
    }

    public void setSepF017001(Integer sepF017001) {
        this.sepF017001 = sepF017001;
    }

    public BigDecimal getSepF018001() {
        return sepF018001;
    }

    public void setSepF018001(BigDecimal sepF018001) {
        this.sepF018001 = sepF018001;
    }

    public Integer getSepG001001() {
        return sepG001001;
    }

    public void setSepG001001(Integer sepG001001) {
        this.sepG001001 = sepG001001;
    }

    public Boolean getSepG002001() {
        return sepG002001;
    }

    public void setSepG002001(Boolean sepG002001) {
        this.sepG002001 = sepG002001;
    }

    public Boolean getSepG003001() {
        return sepG003001;
    }

    public void setSepG003001(Boolean sepG003001) {
        this.sepG003001 = sepG003001;
    }

    public Integer getSepG004001() {
        return sepG004001;
    }

    public void setSepG004001(Integer sepG004001) {
        this.sepG004001 = sepG004001;
    }

    public Boolean getSepH001001() {
        return sepH001001;
    }

    public void setSepH001001(Boolean sepH001001) {
        this.sepH001001 = sepH001001;
    }

    public String getSepH002001() {
        return sepH002001;
    }

    public void setSepH002001(String sepH002001) {
        this.sepH002001 = sepH002001;
    }

    public Boolean getSepH003001() {
        return sepH003001;
    }

    public void setSepH003001(Boolean sepH003001) {
        this.sepH003001 = sepH003001;
    }

    public Boolean getSepH004001() {
        return sepH004001;
    }

    public void setSepH004001(Boolean sepH004001) {
        this.sepH004001 = sepH004001;
    }

    public Integer getSepH005001() {
        return sepH005001;
    }

    public void setSepH005001(Integer sepH005001) {
        this.sepH005001 = sepH005001;
    }

    public Integer getSepH006001() {
        return sepH006001;
    }

    public void setSepH006001(Integer sepH006001) {
        this.sepH006001 = sepH006001;
    }

    public Integer getSepH007001() {
        return sepH007001;
    }

    public void setSepH007001(Integer sepH007001) {
        this.sepH007001 = sepH007001;
    }

    public Integer getSepH008001() {
        return sepH008001;
    }

    public void setSepH008001(Integer sepH008001) {
        this.sepH008001 = sepH008001;
    }

    public Integer getSepH009001() {
        return sepH009001;
    }

    public void setSepH009001(Integer sepH009001) {
        this.sepH009001 = sepH009001;
    }

    public Integer getSepH010001() {
        return sepH010001;
    }

    public void setSepH010001(Integer sepH010001) {
        this.sepH010001 = sepH010001;
    }

    public Integer getSepH011001() {
        return sepH011001;
    }

    public void setSepH011001(Integer sepH011001) {
        this.sepH011001 = sepH011001;
    }

    public Integer getSepH012001() {
        return sepH012001;
    }

    public void setSepH012001(Integer sepH012001) {
        this.sepH012001 = sepH012001;
    }

    public Integer getSepH013001() {
        return sepH013001;
    }

    public void setSepH013001(Integer sepH013001) {
        this.sepH013001 = sepH013001;
    }

    public Integer getSepH014001() {
        return sepH014001;
    }

    public void setSepH014001(Integer sepH014001) {
        this.sepH014001 = sepH014001;
    }

    public Integer getSepH015001() {
        return sepH015001;
    }

    public void setSepH015001(Integer sepH015001) {
        this.sepH015001 = sepH015001;
    }

    public Integer getSepH016001() {
        return sepH016001;
    }

    public void setSepH016001(Integer sepH016001) {
        this.sepH016001 = sepH016001;
    }

    public Integer getSepH017001() {
        return sepH017001;
    }

    public void setSepH017001(Integer sepH017001) {
        this.sepH017001 = sepH017001;
    }

    public Integer getSepH018001() {
        return sepH018001;
    }

    public void setSepH018001(Integer sepH018001) {
        this.sepH018001 = sepH018001;
    }

    public String getSepI001001() {
        return sepI001001;
    }

    public void setSepI001001(String sepI001001) {
        this.sepI001001 = sepI001001;
    }

    public String getUrlImagem() {
        return urlImagem;
    }

    public void setUrlImagem(String urlImagem) {
        this.urlImagem = urlImagem;
    }

    public String getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(String classificacao) {
        this.classificacao = classificacao;
    }

    public String getPaisSigla() {
        return paisSigla;
    }

    public void setPaisSigla(String paisSigla) {
        this.paisSigla = paisSigla;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    @XmlTransient
    public List<TbDirTecnico> getTbDirTecnicoList() {
        return tbDirTecnicoList;
    }

    public void setTbDirTecnicoList(List<TbDirTecnico> tbDirTecnicoList) {
        this.tbDirTecnicoList = tbDirTecnicoList;
    }

    @XmlTransient
    public List<TbDirRepresentante> getTbDirRepresentanteList() {
        return tbDirRepresentanteList;
    }

    public void setTbDirRepresentanteList(List<TbDirRepresentante> tbDirRepresentanteList) {
        this.tbDirRepresentanteList = tbDirRepresentanteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (prserSeq != null ? prserSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbPrestServico)) {
            return false;
        }
        TbPrestServico other = (TbPrestServico) object;
        if ((this.prserSeq == null && other.prserSeq != null) || (this.prserSeq != null && !this.prserSeq.equals(other.prserSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbPrestServico[ prserSeq=" + prserSeq + " ]";
    }
    
}
