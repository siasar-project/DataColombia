/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "explorer_query")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ExplorerQuery.findAll", query = "SELECT e FROM ExplorerQuery e")
    , @NamedQuery(name = "ExplorerQuery.findById", query = "SELECT e FROM ExplorerQuery e WHERE e.id = :id")
    , @NamedQuery(name = "ExplorerQuery.findByTitle", query = "SELECT e FROM ExplorerQuery e WHERE e.title = :title")
    , @NamedQuery(name = "ExplorerQuery.findBySql", query = "SELECT e FROM ExplorerQuery e WHERE e.sql = :sql")
    , @NamedQuery(name = "ExplorerQuery.findByDescription", query = "SELECT e FROM ExplorerQuery e WHERE e.description = :description")
    , @NamedQuery(name = "ExplorerQuery.findByCreatedAt", query = "SELECT e FROM ExplorerQuery e WHERE e.createdAt = :createdAt")
    , @NamedQuery(name = "ExplorerQuery.findByLastRunDate", query = "SELECT e FROM ExplorerQuery e WHERE e.lastRunDate = :lastRunDate")
    , @NamedQuery(name = "ExplorerQuery.findBySnapshot", query = "SELECT e FROM ExplorerQuery e WHERE e.snapshot = :snapshot")})
public class ExplorerQuery implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "sql")
    private String sql;
    @Size(max = 2147483647)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "last_run_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastRunDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "snapshot")
    private boolean snapshot;
    @JoinColumn(name = "created_by_user_id", referencedColumnName = "id")
    @ManyToOne
    private AuthUser createdByUserId;
    @OneToMany(mappedBy = "queryId")
    private List<ExplorerQuerylog> explorerQuerylogList;

    public ExplorerQuery() {
    }

    public ExplorerQuery(Integer id) {
        this.id = id;
    }

    public ExplorerQuery(Integer id, String title, String sql, Date createdAt, Date lastRunDate, boolean snapshot) {
        this.id = id;
        this.title = title;
        this.sql = sql;
        this.createdAt = createdAt;
        this.lastRunDate = lastRunDate;
        this.snapshot = snapshot;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getLastRunDate() {
        return lastRunDate;
    }

    public void setLastRunDate(Date lastRunDate) {
        this.lastRunDate = lastRunDate;
    }

    public boolean getSnapshot() {
        return snapshot;
    }

    public void setSnapshot(boolean snapshot) {
        this.snapshot = snapshot;
    }

    public AuthUser getCreatedByUserId() {
        return createdByUserId;
    }

    public void setCreatedByUserId(AuthUser createdByUserId) {
        this.createdByUserId = createdByUserId;
    }

    @XmlTransient
    public List<ExplorerQuerylog> getExplorerQuerylogList() {
        return explorerQuerylogList;
    }

    public void setExplorerQuerylogList(List<ExplorerQuerylog> explorerQuerylogList) {
        this.explorerQuerylogList = explorerQuerylogList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExplorerQuery)) {
            return false;
        }
        ExplorerQuery other = (ExplorerQuery) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.ExplorerQuery[ id=" + id + " ]";
    }
    
}
