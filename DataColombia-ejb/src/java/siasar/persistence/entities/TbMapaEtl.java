/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_mapa_etl")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbMapaEtl.findAll", query = "SELECT t FROM TbMapaEtl t")
    , @NamedQuery(name = "TbMapaEtl.findByMapetlSeq", query = "SELECT t FROM TbMapaEtl t WHERE t.mapetlSeq = :mapetlSeq")
    , @NamedQuery(name = "TbMapaEtl.findByMapetlNome", query = "SELECT t FROM TbMapaEtl t WHERE t.mapetlNome = :mapetlNome")
    , @NamedQuery(name = "TbMapaEtl.findByMapetlArquivo", query = "SELECT t FROM TbMapaEtl t WHERE t.mapetlArquivo = :mapetlArquivo")
    , @NamedQuery(name = "TbMapaEtl.findByMapetlCron", query = "SELECT t FROM TbMapaEtl t WHERE t.mapetlCron = :mapetlCron")
    , @NamedQuery(name = "TbMapaEtl.findByMapetlParametro", query = "SELECT t FROM TbMapaEtl t WHERE t.mapetlParametro = :mapetlParametro")
    , @NamedQuery(name = "TbMapaEtl.findByMapetlLog", query = "SELECT t FROM TbMapaEtl t WHERE t.mapetlLog = :mapetlLog")
    , @NamedQuery(name = "TbMapaEtl.findByMapetlAtivo", query = "SELECT t FROM TbMapaEtl t WHERE t.mapetlAtivo = :mapetlAtivo")
    , @NamedQuery(name = "TbMapaEtl.findByMapetlDatCriacao", query = "SELECT t FROM TbMapaEtl t WHERE t.mapetlDatCriacao = :mapetlDatCriacao")
    , @NamedQuery(name = "TbMapaEtl.findByMapetlDatUltExecucao", query = "SELECT t FROM TbMapaEtl t WHERE t.mapetlDatUltExecucao = :mapetlDatUltExecucao")
    , @NamedQuery(name = "TbMapaEtl.findByMapetlImagem", query = "SELECT t FROM TbMapaEtl t WHERE t.mapetlImagem = :mapetlImagem")
    , @NamedQuery(name = "TbMapaEtl.findByMapetlInstalado", query = "SELECT t FROM TbMapaEtl t WHERE t.mapetlInstalado = :mapetlInstalado")
    , @NamedQuery(name = "TbMapaEtl.findByMapetlPid", query = "SELECT t FROM TbMapaEtl t WHERE t.mapetlPid = :mapetlPid")
    , @NamedQuery(name = "TbMapaEtl.findByMapetlComando", query = "SELECT t FROM TbMapaEtl t WHERE t.mapetlComando = :mapetlComando")})
public class TbMapaEtl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "mapetl_seq")
    private Long mapetlSeq;
    @Size(max = 2000)
    @Column(name = "mapetl_nome")
    private String mapetlNome;
    @Size(max = 4000)
    @Column(name = "mapetl_arquivo")
    private String mapetlArquivo;
    @Size(max = 4000)
    @Column(name = "mapetl_cron")
    private String mapetlCron;
    @Size(max = 4000)
    @Column(name = "mapetl_parametro")
    private String mapetlParametro;
    @Size(max = 2147483647)
    @Column(name = "mapetl_log")
    private String mapetlLog;
    @Column(name = "mapetl_ativo")
    private Boolean mapetlAtivo;
    @Column(name = "mapetl_dat_criacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date mapetlDatCriacao;
    @Column(name = "mapetl_dat_ult_execucao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date mapetlDatUltExecucao;
    @Size(max = 4000)
    @Column(name = "mapetl_imagem")
    private String mapetlImagem;
    @Column(name = "mapetl_instalado")
    private Boolean mapetlInstalado;
    @Column(name = "mapetl_pid")
    private Integer mapetlPid;
    @Size(max = 4000)
    @Column(name = "mapetl_comando")
    private String mapetlComando;

    public TbMapaEtl() {
    }

    public TbMapaEtl(Long mapetlSeq) {
        this.mapetlSeq = mapetlSeq;
    }

    public Long getMapetlSeq() {
        return mapetlSeq;
    }

    public void setMapetlSeq(Long mapetlSeq) {
        this.mapetlSeq = mapetlSeq;
    }

    public String getMapetlNome() {
        return mapetlNome;
    }

    public void setMapetlNome(String mapetlNome) {
        this.mapetlNome = mapetlNome;
    }

    public String getMapetlArquivo() {
        return mapetlArquivo;
    }

    public void setMapetlArquivo(String mapetlArquivo) {
        this.mapetlArquivo = mapetlArquivo;
    }

    public String getMapetlCron() {
        return mapetlCron;
    }

    public void setMapetlCron(String mapetlCron) {
        this.mapetlCron = mapetlCron;
    }

    public String getMapetlParametro() {
        return mapetlParametro;
    }

    public void setMapetlParametro(String mapetlParametro) {
        this.mapetlParametro = mapetlParametro;
    }

    public String getMapetlLog() {
        return mapetlLog;
    }

    public void setMapetlLog(String mapetlLog) {
        this.mapetlLog = mapetlLog;
    }

    public Boolean getMapetlAtivo() {
        return mapetlAtivo;
    }

    public void setMapetlAtivo(Boolean mapetlAtivo) {
        this.mapetlAtivo = mapetlAtivo;
    }

    public Date getMapetlDatCriacao() {
        return mapetlDatCriacao;
    }

    public void setMapetlDatCriacao(Date mapetlDatCriacao) {
        this.mapetlDatCriacao = mapetlDatCriacao;
    }

    public Date getMapetlDatUltExecucao() {
        return mapetlDatUltExecucao;
    }

    public void setMapetlDatUltExecucao(Date mapetlDatUltExecucao) {
        this.mapetlDatUltExecucao = mapetlDatUltExecucao;
    }

    public String getMapetlImagem() {
        return mapetlImagem;
    }

    public void setMapetlImagem(String mapetlImagem) {
        this.mapetlImagem = mapetlImagem;
    }

    public Boolean getMapetlInstalado() {
        return mapetlInstalado;
    }

    public void setMapetlInstalado(Boolean mapetlInstalado) {
        this.mapetlInstalado = mapetlInstalado;
    }

    public Integer getMapetlPid() {
        return mapetlPid;
    }

    public void setMapetlPid(Integer mapetlPid) {
        this.mapetlPid = mapetlPid;
    }

    public String getMapetlComando() {
        return mapetlComando;
    }

    public void setMapetlComando(String mapetlComando) {
        this.mapetlComando = mapetlComando;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mapetlSeq != null ? mapetlSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbMapaEtl)) {
            return false;
        }
        TbMapaEtl other = (TbMapaEtl) object;
        if ((this.mapetlSeq == null && other.mapetlSeq != null) || (this.mapetlSeq != null && !this.mapetlSeq.equals(other.mapetlSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbMapaEtl[ mapetlSeq=" + mapetlSeq + " ]";
    }
    
}
