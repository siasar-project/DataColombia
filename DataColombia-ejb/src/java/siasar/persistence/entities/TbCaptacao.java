/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siasar.persistence.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielo
 */
@Entity
@Table(name = "tb_captacao")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbCaptacao.findAll", query = "SELECT t FROM TbCaptacao t")
    , @NamedQuery(name = "TbCaptacao.findByCaptaSeq", query = "SELECT t FROM TbCaptacao t WHERE t.captaSeq = :captaSeq")
    , @NamedQuery(name = "TbCaptacao.findBySysB001001", query = "SELECT t FROM TbCaptacao t WHERE t.sysB001001 = :sysB001001")
    , @NamedQuery(name = "TbCaptacao.findBySysB002001", query = "SELECT t FROM TbCaptacao t WHERE t.sysB002001 = :sysB002001")
    , @NamedQuery(name = "TbCaptacao.findBySysB003001", query = "SELECT t FROM TbCaptacao t WHERE t.sysB003001 = :sysB003001")
    , @NamedQuery(name = "TbCaptacao.findBySysB004001", query = "SELECT t FROM TbCaptacao t WHERE t.sysB004001 = :sysB004001")
    , @NamedQuery(name = "TbCaptacao.findBySysB005001", query = "SELECT t FROM TbCaptacao t WHERE t.sysB005001 = :sysB005001")
    , @NamedQuery(name = "TbCaptacao.findBySysB006001", query = "SELECT t FROM TbCaptacao t WHERE t.sysB006001 = :sysB006001")
    , @NamedQuery(name = "TbCaptacao.findBySysB007001", query = "SELECT t FROM TbCaptacao t WHERE t.sysB007001 = :sysB007001")
    , @NamedQuery(name = "TbCaptacao.findBySysB008001", query = "SELECT t FROM TbCaptacao t WHERE t.sysB008001 = :sysB008001")
    , @NamedQuery(name = "TbCaptacao.findBySysB009001", query = "SELECT t FROM TbCaptacao t WHERE t.sysB009001 = :sysB009001")
    , @NamedQuery(name = "TbCaptacao.findBySysB010001", query = "SELECT t FROM TbCaptacao t WHERE t.sysB010001 = :sysB010001")
    , @NamedQuery(name = "TbCaptacao.findBySysB011001", query = "SELECT t FROM TbCaptacao t WHERE t.sysB011001 = :sysB011001")
    , @NamedQuery(name = "TbCaptacao.findBySysB012001", query = "SELECT t FROM TbCaptacao t WHERE t.sysB012001 = :sysB012001")
    , @NamedQuery(name = "TbCaptacao.findBySysB013001", query = "SELECT t FROM TbCaptacao t WHERE t.sysB013001 = :sysB013001")
    , @NamedQuery(name = "TbCaptacao.findBySysB014001", query = "SELECT t FROM TbCaptacao t WHERE t.sysB014001 = :sysB014001")
    , @NamedQuery(name = "TbCaptacao.findBySysB015001", query = "SELECT t FROM TbCaptacao t WHERE t.sysB015001 = :sysB015001")
    , @NamedQuery(name = "TbCaptacao.findBySysB016001", query = "SELECT t FROM TbCaptacao t WHERE t.sysB016001 = :sysB016001")
    , @NamedQuery(name = "TbCaptacao.findBySysB017001", query = "SELECT t FROM TbCaptacao t WHERE t.sysB017001 = :sysB017001")
    , @NamedQuery(name = "TbCaptacao.findBySysB018001", query = "SELECT t FROM TbCaptacao t WHERE t.sysB018001 = :sysB018001")
    , @NamedQuery(name = "TbCaptacao.findBySysB019001", query = "SELECT t FROM TbCaptacao t WHERE t.sysB019001 = :sysB019001")
    , @NamedQuery(name = "TbCaptacao.findBySysB020001", query = "SELECT t FROM TbCaptacao t WHERE t.sysB020001 = :sysB020001")
    , @NamedQuery(name = "TbCaptacao.findBySysB021001", query = "SELECT t FROM TbCaptacao t WHERE t.sysB021001 = :sysB021001")
    , @NamedQuery(name = "TbCaptacao.findBySysB022001", query = "SELECT t FROM TbCaptacao t WHERE t.sysB022001 = :sysB022001")
    , @NamedQuery(name = "TbCaptacao.findByDateLastUpdate", query = "SELECT t FROM TbCaptacao t WHERE t.dateLastUpdate = :dateLastUpdate")})
public class TbCaptacao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "capta_seq")
    private Long captaSeq;
    @Size(max = 2147483647)
    @Column(name = "sys_b_001_001")
    private String sysB001001;
    @Size(max = 100)
    @Column(name = "sys_b_002_001")
    private String sysB002001;
    @Column(name = "sys_b_003_001")
    private Integer sysB003001;
    @Column(name = "sys_b_004_001")
    private Boolean sysB004001;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "sys_b_005_001")
    private BigDecimal sysB005001;
    @Column(name = "sys_b_006_001")
    private Integer sysB006001;
    @Column(name = "sys_b_007_001")
    @Temporal(TemporalType.DATE)
    private Date sysB007001;
    @Column(name = "sys_b_008_001")
    private BigDecimal sysB008001;
    @Column(name = "sys_b_009_001")
    private Integer sysB009001;
    @Column(name = "sys_b_010_001")
    @Temporal(TemporalType.DATE)
    private Date sysB010001;
    @Column(name = "sys_b_011_001")
    private Double sysB011001;
    @Column(name = "sys_b_012_001")
    private Double sysB012001;
    @Column(name = "sys_b_013_001")
    private Double sysB013001;
    @Column(name = "sys_b_014_001")
    private Boolean sysB014001;
    @Column(name = "sys_b_015_001")
    private Boolean sysB015001;
    @Column(name = "sys_b_016_001")
    private Boolean sysB016001;
    @Column(name = "sys_b_017_001")
    private Boolean sysB017001;
    @Column(name = "sys_b_018_001")
    private Boolean sysB018001;
    @Column(name = "sys_b_019_001")
    private Boolean sysB019001;
    @Column(name = "sys_b_020_001")
    private Boolean sysB020001;
    @Column(name = "sys_b_021_001")
    private Integer sysB021001;
    @Size(max = 2147483647)
    @Column(name = "sys_b_022_001")
    private String sysB022001;
    @Column(name = "date_last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdate;
    @JoinColumn(name = "siste_seq", referencedColumnName = "siste_seq")
    @ManyToOne
    private TbSistema sisteSeq;

    public TbCaptacao() {
    }

    public TbCaptacao(Long captaSeq) {
        this.captaSeq = captaSeq;
    }

    public Long getCaptaSeq() {
        return captaSeq;
    }

    public void setCaptaSeq(Long captaSeq) {
        this.captaSeq = captaSeq;
    }

    public String getSysB001001() {
        return sysB001001;
    }

    public void setSysB001001(String sysB001001) {
        this.sysB001001 = sysB001001;
    }

    public String getSysB002001() {
        return sysB002001;
    }

    public void setSysB002001(String sysB002001) {
        this.sysB002001 = sysB002001;
    }

    public Integer getSysB003001() {
        return sysB003001;
    }

    public void setSysB003001(Integer sysB003001) {
        this.sysB003001 = sysB003001;
    }

    public Boolean getSysB004001() {
        return sysB004001;
    }

    public void setSysB004001(Boolean sysB004001) {
        this.sysB004001 = sysB004001;
    }

    public BigDecimal getSysB005001() {
        return sysB005001;
    }

    public void setSysB005001(BigDecimal sysB005001) {
        this.sysB005001 = sysB005001;
    }

    public Integer getSysB006001() {
        return sysB006001;
    }

    public void setSysB006001(Integer sysB006001) {
        this.sysB006001 = sysB006001;
    }

    public Date getSysB007001() {
        return sysB007001;
    }

    public void setSysB007001(Date sysB007001) {
        this.sysB007001 = sysB007001;
    }

    public BigDecimal getSysB008001() {
        return sysB008001;
    }

    public void setSysB008001(BigDecimal sysB008001) {
        this.sysB008001 = sysB008001;
    }

    public Integer getSysB009001() {
        return sysB009001;
    }

    public void setSysB009001(Integer sysB009001) {
        this.sysB009001 = sysB009001;
    }

    public Date getSysB010001() {
        return sysB010001;
    }

    public void setSysB010001(Date sysB010001) {
        this.sysB010001 = sysB010001;
    }

    public Double getSysB011001() {
        return sysB011001;
    }

    public void setSysB011001(Double sysB011001) {
        this.sysB011001 = sysB011001;
    }

    public Double getSysB012001() {
        return sysB012001;
    }

    public void setSysB012001(Double sysB012001) {
        this.sysB012001 = sysB012001;
    }

    public Double getSysB013001() {
        return sysB013001;
    }

    public void setSysB013001(Double sysB013001) {
        this.sysB013001 = sysB013001;
    }

    public Boolean getSysB014001() {
        return sysB014001;
    }

    public void setSysB014001(Boolean sysB014001) {
        this.sysB014001 = sysB014001;
    }

    public Boolean getSysB015001() {
        return sysB015001;
    }

    public void setSysB015001(Boolean sysB015001) {
        this.sysB015001 = sysB015001;
    }

    public Boolean getSysB016001() {
        return sysB016001;
    }

    public void setSysB016001(Boolean sysB016001) {
        this.sysB016001 = sysB016001;
    }

    public Boolean getSysB017001() {
        return sysB017001;
    }

    public void setSysB017001(Boolean sysB017001) {
        this.sysB017001 = sysB017001;
    }

    public Boolean getSysB018001() {
        return sysB018001;
    }

    public void setSysB018001(Boolean sysB018001) {
        this.sysB018001 = sysB018001;
    }

    public Boolean getSysB019001() {
        return sysB019001;
    }

    public void setSysB019001(Boolean sysB019001) {
        this.sysB019001 = sysB019001;
    }

    public Boolean getSysB020001() {
        return sysB020001;
    }

    public void setSysB020001(Boolean sysB020001) {
        this.sysB020001 = sysB020001;
    }

    public Integer getSysB021001() {
        return sysB021001;
    }

    public void setSysB021001(Integer sysB021001) {
        this.sysB021001 = sysB021001;
    }

    public String getSysB022001() {
        return sysB022001;
    }

    public void setSysB022001(String sysB022001) {
        this.sysB022001 = sysB022001;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    public TbSistema getSisteSeq() {
        return sisteSeq;
    }

    public void setSisteSeq(TbSistema sisteSeq) {
        this.sisteSeq = sisteSeq;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (captaSeq != null ? captaSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbCaptacao)) {
            return false;
        }
        TbCaptacao other = (TbCaptacao) object;
        if ((this.captaSeq == null && other.captaSeq != null) || (this.captaSeq != null && !this.captaSeq.equals(other.captaSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siasar.persistence.entities.TbCaptacao[ captaSeq=" + captaSeq + " ]";
    }
    
}
