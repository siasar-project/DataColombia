package siasar.persistence.entities;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbWaterQuality.class)
public abstract class TbWaterQuality_ {

	public static volatile SingularAttribute<TbWaterQuality, String> waqA009001;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqE002001Va;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqE001001Un;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqC002001Va;
	public static volatile SingularAttribute<TbWaterQuality, Double> waqA005001;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqC007001Un;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqB005001Un;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqC010001Un;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqC016001Un;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqC017001Va;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqC003001Va;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqC008001Un;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqB004001Un;
	public static volatile SingularAttribute<TbWaterQuality, Date> waqA001001;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqC015001Un;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqC011001Va;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqC001001Un;
	public static volatile SingularAttribute<TbWaterQuality, String> waqA008001;
	public static volatile SingularAttribute<TbWaterQuality, String> waqA010001;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqC012001Va;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqC015001Va;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqC006001Un;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqC014001Un;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqC003001Un;
	public static volatile SingularAttribute<TbWaterQuality, Double> waqA004001;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqC011001Un;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqC007001Va;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqC001001Va;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqC009001Va;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqB001001;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqC004001Va;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqB005001Va;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqE001001Va;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqB002001Va;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqC017001Un;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqC009001Un;
	public static volatile SingularAttribute<TbWaterQuality, String> waqA007001;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqA003001;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqB002001Un;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqC005001Un;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqC013001Un;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqC006001Va;
	public static volatile SingularAttribute<TbWaterQuality, String> waqF001001;
	public static volatile SingularAttribute<TbWaterQuality, Date> dateLastUpdate;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqC012001Un;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqB003001Va;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqC014001Va;
	public static volatile SingularAttribute<TbWaterQuality, Long> waqId;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqC004001Un;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqD001001Un;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqB003001Un;
	public static volatile SingularAttribute<TbWaterQuality, Double> waqA006001;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqB004001Va;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqD001001Va;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqC002001Un;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqC010001Va;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqA002001;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqC016001Va;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqC005001Va;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqC008001Va;
	public static volatile SingularAttribute<TbWaterQuality, BigDecimal> waqC013001Va;
	public static volatile SingularAttribute<TbWaterQuality, Integer> waqE002001Un;

}

