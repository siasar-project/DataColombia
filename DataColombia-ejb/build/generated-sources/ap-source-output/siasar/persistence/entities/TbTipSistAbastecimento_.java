package siasar.persistence.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbTipSistAbastecimento.class)
public abstract class TbTipSistAbastecimento_ {

	public static volatile SingularAttribute<TbTipSistAbastecimento, Date> dateLastUpdate;
	public static volatile SingularAttribute<TbTipSistAbastecimento, Integer> sysA027001;
	public static volatile SingularAttribute<TbTipSistAbastecimento, TbSistema> tbSistema;
	public static volatile SingularAttribute<TbTipSistAbastecimento, TbTipSistAbastecimentoPK> tbTipSistAbastecimentoPK;
	public static volatile SingularAttribute<TbTipSistAbastecimento, TbTaxoTermo> tbTaxoTermo;

}

