package siasar.persistence.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DjangoAdminLog.class)
public abstract class DjangoAdminLog_ {

	public static volatile SingularAttribute<DjangoAdminLog, String> objectRepr;
	public static volatile SingularAttribute<DjangoAdminLog, Date> actionTime;
	public static volatile SingularAttribute<DjangoAdminLog, Short> actionFlag;
	public static volatile SingularAttribute<DjangoAdminLog, String> changeMessage;
	public static volatile SingularAttribute<DjangoAdminLog, DjangoContentType> contentTypeId;
	public static volatile SingularAttribute<DjangoAdminLog, Integer> id;
	public static volatile SingularAttribute<DjangoAdminLog, AuthUser> userId;
	public static volatile SingularAttribute<DjangoAdminLog, String> objectId;

}

