package siasar.persistence.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbTratamento.class)
public abstract class TbTratamento_ {

	public static volatile SingularAttribute<TbTratamento, Date> dateLastUpdate;
	public static volatile SingularAttribute<TbTratamento, TbSistema> sisteSeq;
	public static volatile SingularAttribute<TbTratamento, Double> sysD005001;
	public static volatile SingularAttribute<TbTratamento, Double> sysD006001;
	public static volatile SingularAttribute<TbTratamento, Integer> sysD007001;
	public static volatile SingularAttribute<TbTratamento, Long> trataSeq;
	public static volatile SingularAttribute<TbTratamento, String> sysD008001;
	public static volatile SingularAttribute<TbTratamento, String> sysD001001;
	public static volatile SingularAttribute<TbTratamento, Double> sysD004001;
	public static volatile SingularAttribute<TbTratamento, Integer> sysD002001;
	public static volatile SingularAttribute<TbTratamento, Boolean> sysD003001;

}

