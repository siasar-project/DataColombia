package siasar.persistence.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbIntervMelhoria.class)
public abstract class TbIntervMelhoria_ {

	public static volatile SingularAttribute<TbIntervMelhoria, String> intmeFonte;
	public static volatile SingularAttribute<TbIntervMelhoria, Date> dateLastUpdate;
	public static volatile SingularAttribute<TbIntervMelhoria, Long> intmeSeq;
	public static volatile SingularAttribute<TbIntervMelhoria, TbComunidade> comunSeq;
	public static volatile SingularAttribute<TbIntervMelhoria, TbTaxoTermo> taxtSeq;

}

