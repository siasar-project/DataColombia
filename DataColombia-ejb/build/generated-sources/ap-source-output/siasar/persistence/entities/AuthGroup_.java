package siasar.persistence.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AuthGroup.class)
public abstract class AuthGroup_ {

	public static volatile ListAttribute<AuthGroup, AuthGroupPermissions> authGroupPermissionsList;
	public static volatile SingularAttribute<AuthGroup, String> name;
	public static volatile ListAttribute<AuthGroup, AuthUserGroups> authUserGroupsList;
	public static volatile SingularAttribute<AuthGroup, Integer> id;

}

