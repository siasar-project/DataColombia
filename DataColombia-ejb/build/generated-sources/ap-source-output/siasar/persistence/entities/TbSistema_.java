package siasar.persistence.entities;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbSistema.class)
public abstract class TbSistema_ {

	public static volatile SingularAttribute<TbSistema, BigDecimal> sysG001001;
	public static volatile SingularAttribute<TbSistema, Integer> sysG003001;
	public static volatile SingularAttribute<TbSistema, Integer> sysA002001;
	public static volatile SingularAttribute<TbSistema, Integer> sysA004001;
	public static volatile ListAttribute<TbSistema, TbReabilAmpliacao> tbReabilAmpliacaoList;
	public static volatile SingularAttribute<TbSistema, Integer> sysG007001;
	public static volatile SingularAttribute<TbSistema, Date> sysG005001;
	public static volatile SingularAttribute<TbSistema, Boolean> sysG009001;
	public static volatile SingularAttribute<TbSistema, Integer> sysA011001;
	public static volatile SingularAttribute<TbSistema, Date> dateLastUpdate;
	public static volatile SingularAttribute<TbSistema, Double> sysA008001;
	public static volatile SingularAttribute<TbSistema, Long> sisteSeq;
	public static volatile SingularAttribute<TbSistema, Date> sysG010001;
	public static volatile SingularAttribute<TbSistema, Integer> sysA013001;
	public static volatile SingularAttribute<TbSistema, String> sysA034001;
	public static volatile SingularAttribute<TbSistema, String> sysA006001;
	public static volatile SingularAttribute<TbSistema, String> sysA032001;
	public static volatile SingularAttribute<TbSistema, Boolean> sysA029001;
	public static volatile SingularAttribute<TbSistema, String> sysA030001;
	public static volatile SingularAttribute<TbSistema, String> sysH001001;
	public static volatile ListAttribute<TbSistema, TbTratamento> tbTratamentoList;
	public static volatile SingularAttribute<TbSistema, String> classificacao;
	public static volatile SingularAttribute<TbSistema, String> paisSigla;
	public static volatile ListAttribute<TbSistema, TbFonteFinanc> tbFonteFinancList;
	public static volatile SingularAttribute<TbSistema, String> sysA001001;
	public static volatile SingularAttribute<TbSistema, BigDecimal> sysA026001;
	public static volatile SingularAttribute<TbSistema, Integer> sysG002001;
	public static volatile SingularAttribute<TbSistema, Integer> sysA005001;
	public static volatile SingularAttribute<TbSistema, BigDecimal> sysA018001;
	public static volatile SingularAttribute<TbSistema, Integer> sysG004001;
	public static volatile SingularAttribute<TbSistema, BigDecimal> sysG006001;
	public static volatile SingularAttribute<TbSistema, Date> sysG008001;
	public static volatile ListAttribute<TbSistema, TbArmazenamento> tbArmazenamentoList;
	public static volatile ListAttribute<TbSistema, TbTipSistAbastecimento> tbTipSistAbastecimentoList;
	public static volatile ListAttribute<TbSistema, TbCaptacao> tbCaptacaoList;
	public static volatile SingularAttribute<TbSistema, Integer> sysA012001;
	public static volatile SingularAttribute<TbSistema, String> sysA010001;
	public static volatile SingularAttribute<TbSistema, Boolean> sysG011001;
	public static volatile SingularAttribute<TbSistema, String> sysA033001;
	public static volatile ListAttribute<TbSistema, TbRedeDistribuicao> tbRedeDistribuicaoList;
	public static volatile SingularAttribute<TbSistema, Double> sysA009001;
	public static volatile SingularAttribute<TbSistema, Double> sysA007001;
	public static volatile SingularAttribute<TbSistema, Date> sysA031001;
	public static volatile ListAttribute<TbSistema, TbCentroSaude> tbCentroSaudeList;
	public static volatile ListAttribute<TbSistema, TbConducao> tbConducaoList;
	public static volatile SingularAttribute<TbSistema, Boolean> validado;
	public static volatile SingularAttribute<TbSistema, Boolean> sysA028001;
	public static volatile SingularAttribute<TbSistema, Date> dataValidacao;

}

