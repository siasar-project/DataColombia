package siasar.persistence.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbIntervSistSaneNaoMelhor.class)
public abstract class TbIntervSistSaneNaoMelhor_ {

	public static volatile SingularAttribute<TbIntervSistSaneNaoMelhor, Date> dateLastUpdate;
	public static volatile SingularAttribute<TbIntervSistSaneNaoMelhor, String> intssnmFonte;
	public static volatile SingularAttribute<TbIntervSistSaneNaoMelhor, TbComunidade> comunSeq;
	public static volatile SingularAttribute<TbIntervSistSaneNaoMelhor, Long> intssnmSeq;
	public static volatile SingularAttribute<TbIntervSistSaneNaoMelhor, TbTaxoTermo> taxtSeq;

}

