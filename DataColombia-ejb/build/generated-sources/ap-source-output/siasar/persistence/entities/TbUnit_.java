package siasar.persistence.entities;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbUnit.class)
public abstract class TbUnit_ {

	public static volatile SingularAttribute<TbUnit, Integer> unitSeq;
	public static volatile SingularAttribute<TbUnit, String> unitLabel;
	public static volatile SingularAttribute<TbUnit, String> unitModule;
	public static volatile SingularAttribute<TbUnit, String> unitSymbol;
	public static volatile ListAttribute<TbUnit, TbVariavel> tbVariavelList;
	public static volatile SingularAttribute<TbUnit, BigDecimal> unitFactor;
	public static volatile SingularAttribute<TbUnit, String> unitMeasure;

}

