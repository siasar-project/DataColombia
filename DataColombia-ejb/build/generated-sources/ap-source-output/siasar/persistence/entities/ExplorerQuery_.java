package siasar.persistence.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ExplorerQuery.class)
public abstract class ExplorerQuery_ {

	public static volatile SingularAttribute<ExplorerQuery, Date> createdAt;
	public static volatile SingularAttribute<ExplorerQuery, AuthUser> createdByUserId;
	public static volatile ListAttribute<ExplorerQuery, ExplorerQuerylog> explorerQuerylogList;
	public static volatile SingularAttribute<ExplorerQuery, String> description;
	public static volatile SingularAttribute<ExplorerQuery, Date> lastRunDate;
	public static volatile SingularAttribute<ExplorerQuery, Integer> id;
	public static volatile SingularAttribute<ExplorerQuery, String> title;
	public static volatile SingularAttribute<ExplorerQuery, Boolean> snapshot;
	public static volatile SingularAttribute<ExplorerQuery, String> sql;

}

