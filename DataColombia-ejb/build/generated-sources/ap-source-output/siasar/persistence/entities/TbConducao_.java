package siasar.persistence.entities;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbConducao.class)
public abstract class TbConducao_ {

	public static volatile SingularAttribute<TbConducao, Date> dateLastUpdate;
	public static volatile SingularAttribute<TbConducao, String> sysC001001;
	public static volatile SingularAttribute<TbConducao, Boolean> sysC006001;
	public static volatile SingularAttribute<TbConducao, Integer> sysC007001;
	public static volatile SingularAttribute<TbConducao, TbSistema> sisteSeq;
	public static volatile SingularAttribute<TbConducao, Double> sysC002001;
	public static volatile SingularAttribute<TbConducao, Integer> sysC005001;
	public static volatile SingularAttribute<TbConducao, Integer> sysC003001;
	public static volatile SingularAttribute<TbConducao, BigDecimal> sysC004001;
	public static volatile SingularAttribute<TbConducao, Long> conduSeq;
	public static volatile SingularAttribute<TbConducao, String> sysC008001;

}

