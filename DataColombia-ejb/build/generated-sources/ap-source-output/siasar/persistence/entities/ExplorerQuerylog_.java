package siasar.persistence.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ExplorerQuerylog.class)
public abstract class ExplorerQuerylog_ {

	public static volatile SingularAttribute<ExplorerQuerylog, Double> duration;
	public static volatile SingularAttribute<ExplorerQuerylog, Date> runAt;
	public static volatile SingularAttribute<ExplorerQuerylog, AuthUser> runByUserId;
	public static volatile SingularAttribute<ExplorerQuerylog, Integer> id;
	public static volatile SingularAttribute<ExplorerQuerylog, String> sql;
	public static volatile SingularAttribute<ExplorerQuerylog, ExplorerQuery> queryId;

}

