package siasar.persistence.entities;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbRedeDistribuicao.class)
public abstract class TbRedeDistribuicao_ {

	public static volatile SingularAttribute<TbRedeDistribuicao, Integer> sysF003001;
	public static volatile SingularAttribute<TbRedeDistribuicao, Integer> sysF004001;
	public static volatile SingularAttribute<TbRedeDistribuicao, Date> dateLastUpdate;
	public static volatile SingularAttribute<TbRedeDistribuicao, Integer> sysF005001;
	public static volatile SingularAttribute<TbRedeDistribuicao, Integer> sysF006001;
	public static volatile SingularAttribute<TbRedeDistribuicao, TbSistema> sisteSeq;
	public static volatile SingularAttribute<TbRedeDistribuicao, Long> redisSeq;
	public static volatile SingularAttribute<TbRedeDistribuicao, String> sysF008001;
	public static volatile SingularAttribute<TbRedeDistribuicao, Integer> sysF007001;
	public static volatile SingularAttribute<TbRedeDistribuicao, String> sysF001001;
	public static volatile SingularAttribute<TbRedeDistribuicao, BigDecimal> sysF002001;

}

