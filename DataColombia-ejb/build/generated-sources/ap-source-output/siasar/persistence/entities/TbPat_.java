package siasar.persistence.entities;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbPat.class)
public abstract class TbPat_ {

	public static volatile SingularAttribute<TbPat, BigDecimal> patC004001;
	public static volatile SingularAttribute<TbPat, Integer> patB001001;
	public static volatile SingularAttribute<TbPat, Integer> patC010001;
	public static volatile SingularAttribute<TbPat, Integer> patA006001;
	public static volatile SingularAttribute<TbPat, Date> patA002001;
	public static volatile SingularAttribute<TbPat, String> patD013001;
	public static volatile SingularAttribute<TbPat, Boolean> patC013001;
	public static volatile SingularAttribute<TbPat, Boolean> patC017001;
	public static volatile SingularAttribute<TbPat, Integer> patSeq;
	public static volatile SingularAttribute<TbPat, Integer> patD006001;
	public static volatile SingularAttribute<TbPat, Integer> patC007001;
	public static volatile SingularAttribute<TbPat, Integer> patC001001;
	public static volatile SingularAttribute<TbPat, Integer> patC005001;
	public static volatile SingularAttribute<TbPat, Integer> patA007001;
	public static volatile SingularAttribute<TbPat, String> patE001001;
	public static volatile SingularAttribute<TbPat, String> patA003001;
	public static volatile SingularAttribute<TbPat, Integer> patC016001;
	public static volatile SingularAttribute<TbPat, Integer> patD010001;
	public static volatile SingularAttribute<TbPat, Integer> patD003001;
	public static volatile SingularAttribute<TbPat, Integer> patD007001;
	public static volatile SingularAttribute<TbPat, Double> patA010001;
	public static volatile SingularAttribute<TbPat, Boolean> patC002001;
	public static volatile SingularAttribute<TbPat, Integer> patD001001;
	public static volatile SingularAttribute<TbPat, Integer> patA008001;
	public static volatile SingularAttribute<TbPat, Integer> patC006001;
	public static volatile SingularAttribute<TbPat, String> patA004001;
	public static volatile SingularAttribute<TbPat, Integer> patC012001;
	public static volatile SingularAttribute<TbPat, Integer> patD014001;
	public static volatile SingularAttribute<TbPat, Boolean> patC015001;
	public static volatile SingularAttribute<TbPat, Date> dateLastUpdate;
	public static volatile SingularAttribute<TbPat, Integer> patD011001;
	public static volatile SingularAttribute<TbPat, Integer> patC009001;
	public static volatile SingularAttribute<TbPat, String> paisSigla;
	public static volatile SingularAttribute<TbPat, String> classificacao;
	public static volatile SingularAttribute<TbPat, Integer> patD004001;
	public static volatile SingularAttribute<TbPat, Integer> patD008001;
	public static volatile SingularAttribute<TbPat, Integer> patC003001;
	public static volatile SingularAttribute<TbPat, Double> patA011001;
	public static volatile SingularAttribute<TbPat, String> patA009001;
	public static volatile SingularAttribute<TbPat, Integer> patD002001;
	public static volatile SingularAttribute<TbPat, String> patA005001;
	public static volatile SingularAttribute<TbPat, Integer> patB002001;
	public static volatile SingularAttribute<TbPat, Boolean> patC011001;
	public static volatile SingularAttribute<TbPat, String> patA001001;
	public static volatile SingularAttribute<TbPat, Integer> patC014001;
	public static volatile SingularAttribute<TbPat, Integer> patD012001;
	public static volatile SingularAttribute<TbPat, Integer> patC018001;
	public static volatile SingularAttribute<TbPat, Integer> patD005001;
	public static volatile SingularAttribute<TbPat, Boolean> validado;
	public static volatile SingularAttribute<TbPat, Integer> patC008001;
	public static volatile SingularAttribute<TbPat, Date> dataValidacao;
	public static volatile SingularAttribute<TbPat, Integer> patD009001;

}

