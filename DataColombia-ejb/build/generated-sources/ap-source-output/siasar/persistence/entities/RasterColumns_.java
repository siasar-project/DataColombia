package siasar.persistence.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RasterColumns.class)
public abstract class RasterColumns_ {

	public static volatile SingularAttribute<RasterColumns, String> RRasterColumn;
	public static volatile SingularAttribute<RasterColumns, Boolean> spatialIndex;
	public static volatile SingularAttribute<RasterColumns, String> RTableCatalog;
	public static volatile SingularAttribute<RasterColumns, String> RTableName;
	public static volatile SingularAttribute<RasterColumns, Boolean> sameAlignment;
	public static volatile SingularAttribute<RasterColumns, Integer> blocksizeX;
	public static volatile SingularAttribute<RasterColumns, String> RTableSchema;
	public static volatile SingularAttribute<RasterColumns, Integer> srid;
	public static volatile SingularAttribute<RasterColumns, Integer> blocksizeY;
	public static volatile SingularAttribute<RasterColumns, Double> scaleX;
	public static volatile SingularAttribute<RasterColumns, Double> scaleY;
	public static volatile SingularAttribute<RasterColumns, Integer> numBands;
	public static volatile SingularAttribute<RasterColumns, Boolean> regularBlocking;

}

