package siasar.persistence.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbTipSistAbastecimentoPK.class)
public abstract class TbTipSistAbastecimentoPK_ {

	public static volatile SingularAttribute<TbTipSistAbastecimentoPK, Long> sisteSeq;
	public static volatile SingularAttribute<TbTipSistAbastecimentoPK, Long> taxtSeq;

}

