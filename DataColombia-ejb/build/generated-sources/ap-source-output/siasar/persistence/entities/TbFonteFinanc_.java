package siasar.persistence.entities;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbFonteFinanc.class)
public abstract class TbFonteFinanc_ {

	public static volatile SingularAttribute<TbFonteFinanc, Date> dateLastUpdate;
	public static volatile SingularAttribute<TbFonteFinanc, TbSistema> sisteSeq;
	public static volatile SingularAttribute<TbFonteFinanc, BigDecimal> sysA016001;
	public static volatile SingularAttribute<TbFonteFinanc, Integer> sysA014001;
	public static volatile SingularAttribute<TbFonteFinanc, Integer> sysA015001;
	public static volatile SingularAttribute<TbFonteFinanc, Integer> sysA017001;
	public static volatile SingularAttribute<TbFonteFinanc, Long> fonteSeq;

}

