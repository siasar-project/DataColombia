package siasar.persistence.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DjangoSession.class)
public abstract class DjangoSession_ {

	public static volatile SingularAttribute<DjangoSession, String> sessionData;
	public static volatile SingularAttribute<DjangoSession, String> sessionKey;
	public static volatile SingularAttribute<DjangoSession, Date> expireDate;

}

