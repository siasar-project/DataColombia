package siasar.persistence.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RasterOverviews.class)
public abstract class RasterOverviews_ {

	public static volatile SingularAttribute<RasterOverviews, String> OTableCatalog;
	public static volatile SingularAttribute<RasterOverviews, String> RRasterColumn;
	public static volatile SingularAttribute<RasterOverviews, Integer> overviewFactor;
	public static volatile SingularAttribute<RasterOverviews, String> OTableName;
	public static volatile SingularAttribute<RasterOverviews, String> RTableCatalog;
	public static volatile SingularAttribute<RasterOverviews, String> RTableName;
	public static volatile SingularAttribute<RasterOverviews, String> ORasterColumn;
	public static volatile SingularAttribute<RasterOverviews, String> OTableSchema;
	public static volatile SingularAttribute<RasterOverviews, String> RTableSchema;

}

