package siasar.persistence.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbIntervNovoSistAgua.class)
public abstract class TbIntervNovoSistAgua_ {

	public static volatile SingularAttribute<TbIntervNovoSistAgua, Date> dateLastUpdate;
	public static volatile SingularAttribute<TbIntervNovoSistAgua, String> intnsaFonte;
	public static volatile SingularAttribute<TbIntervNovoSistAgua, TbComunidade> comunSeq;
	public static volatile SingularAttribute<TbIntervNovoSistAgua, Long> intnsaSeq;
	public static volatile SingularAttribute<TbIntervNovoSistAgua, TbTaxoTermo> taxtSeq;

}

