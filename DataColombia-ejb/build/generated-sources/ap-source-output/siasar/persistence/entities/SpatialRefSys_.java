package siasar.persistence.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SpatialRefSys.class)
public abstract class SpatialRefSys_ {

	public static volatile SingularAttribute<SpatialRefSys, String> srtext;
	public static volatile SingularAttribute<SpatialRefSys, String> authName;
	public static volatile SingularAttribute<SpatialRefSys, String> proj4text;
	public static volatile SingularAttribute<SpatialRefSys, Integer> authSrid;
	public static volatile SingularAttribute<SpatialRefSys, Integer> srid;

}

