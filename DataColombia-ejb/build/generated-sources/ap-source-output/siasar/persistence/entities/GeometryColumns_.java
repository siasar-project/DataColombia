package siasar.persistence.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(GeometryColumns.class)
public abstract class GeometryColumns_ {

	public static volatile SingularAttribute<GeometryColumns, String> FTableCatalog;
	public static volatile SingularAttribute<GeometryColumns, Integer> coordDimension;
	public static volatile SingularAttribute<GeometryColumns, String> FTableSchema;
	public static volatile SingularAttribute<GeometryColumns, String> FGeometryColumn;
	public static volatile SingularAttribute<GeometryColumns, String> type;
	public static volatile SingularAttribute<GeometryColumns, String> FTableName;
	public static volatile SingularAttribute<GeometryColumns, Integer> srid;

}

