package siasar.persistence.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AuthPermission.class)
public abstract class AuthPermission_ {

	public static volatile ListAttribute<AuthPermission, AuthGroupPermissions> authGroupPermissionsList;
	public static volatile SingularAttribute<AuthPermission, String> codename;
	public static volatile SingularAttribute<AuthPermission, String> name;
	public static volatile SingularAttribute<AuthPermission, DjangoContentType> contentTypeId;
	public static volatile ListAttribute<AuthPermission, AuthUserUserPermissions> authUserUserPermissionsList;
	public static volatile SingularAttribute<AuthPermission, Integer> id;

}

