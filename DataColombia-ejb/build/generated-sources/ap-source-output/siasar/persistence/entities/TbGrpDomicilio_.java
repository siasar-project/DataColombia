package siasar.persistence.entities;

import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbGrpDomicilio.class)
public abstract class TbGrpDomicilio_ {

	public static volatile SingularAttribute<TbGrpDomicilio, Date> dateLastUpdate;
	public static volatile SingularAttribute<TbGrpDomicilio, BigInteger> sisteSeq;
	public static volatile SingularAttribute<TbGrpDomicilio, Integer> grpdoNumDomicAtend;
	public static volatile SingularAttribute<TbGrpDomicilio, Integer> comA022001;
	public static volatile SingularAttribute<TbGrpDomicilio, TbComunidade> comunSeq;
	public static volatile SingularAttribute<TbGrpDomicilio, BigInteger> prserSeq;
	public static volatile SingularAttribute<TbGrpDomicilio, Long> grpdoSeq;

}

