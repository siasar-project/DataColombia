package siasar.persistence.entities;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbCaptacao.class)
public abstract class TbCaptacao_ {

	public static volatile SingularAttribute<TbCaptacao, Boolean> sysB020001;
	public static volatile SingularAttribute<TbCaptacao, Boolean> sysB018001;
	public static volatile SingularAttribute<TbCaptacao, String> sysB001001;
	public static volatile SingularAttribute<TbCaptacao, Integer> sysB003001;
	public static volatile SingularAttribute<TbCaptacao, BigDecimal> sysB005001;
	public static volatile SingularAttribute<TbCaptacao, String> sysB022001;
	public static volatile SingularAttribute<TbCaptacao, Date> sysB007001;
	public static volatile SingularAttribute<TbCaptacao, Date> dateLastUpdate;
	public static volatile SingularAttribute<TbCaptacao, TbSistema> sisteSeq;
	public static volatile SingularAttribute<TbCaptacao, Boolean> sysB016001;
	public static volatile SingularAttribute<TbCaptacao, Date> sysB010001;
	public static volatile SingularAttribute<TbCaptacao, Double> sysB012001;
	public static volatile SingularAttribute<TbCaptacao, Long> captaSeq;
	public static volatile SingularAttribute<TbCaptacao, Boolean> sysB014001;
	public static volatile SingularAttribute<TbCaptacao, Boolean> sysB019001;
	public static volatile SingularAttribute<TbCaptacao, String> sysB002001;
	public static volatile SingularAttribute<TbCaptacao, Boolean> sysB004001;
	public static volatile SingularAttribute<TbCaptacao, Integer> sysB009001;
	public static volatile SingularAttribute<TbCaptacao, Integer> sysB006001;
	public static volatile SingularAttribute<TbCaptacao, Integer> sysB021001;
	public static volatile SingularAttribute<TbCaptacao, BigDecimal> sysB008001;
	public static volatile SingularAttribute<TbCaptacao, Double> sysB011001;
	public static volatile SingularAttribute<TbCaptacao, Boolean> sysB017001;
	public static volatile SingularAttribute<TbCaptacao, Boolean> sysB015001;
	public static volatile SingularAttribute<TbCaptacao, Double> sysB013001;

}

