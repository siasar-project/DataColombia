package siasar.persistence.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DjangoSite.class)
public abstract class DjangoSite_ {

	public static volatile SingularAttribute<DjangoSite, String> domain;
	public static volatile SingularAttribute<DjangoSite, String> name;
	public static volatile SingularAttribute<DjangoSite, Integer> id;

}

