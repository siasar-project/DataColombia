package siasar.persistence.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbTaxoVocab.class)
public abstract class TbTaxoVocab_ {

	public static volatile ListAttribute<TbTaxoVocab, TbTaxoTermo> tbTaxoTermoList;
	public static volatile SingularAttribute<TbTaxoVocab, String> taxvNome;
	public static volatile SingularAttribute<TbTaxoVocab, String> taxvDescricao;
	public static volatile SingularAttribute<TbTaxoVocab, Long> taxvSeq;
	public static volatile SingularAttribute<TbTaxoVocab, String> taxvMachineName;

}

