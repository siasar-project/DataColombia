package siasar.persistence.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AuthUserUserPermissions.class)
public abstract class AuthUserUserPermissions_ {

	public static volatile SingularAttribute<AuthUserUserPermissions, AuthPermission> permissionId;
	public static volatile SingularAttribute<AuthUserUserPermissions, Integer> id;
	public static volatile SingularAttribute<AuthUserUserPermissions, AuthUser> userId;

}

