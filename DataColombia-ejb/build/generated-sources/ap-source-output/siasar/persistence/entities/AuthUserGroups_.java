package siasar.persistence.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AuthUserGroups.class)
public abstract class AuthUserGroups_ {

	public static volatile SingularAttribute<AuthUserGroups, AuthGroup> groupId;
	public static volatile SingularAttribute<AuthUserGroups, Integer> id;
	public static volatile SingularAttribute<AuthUserGroups, AuthUser> userId;

}

