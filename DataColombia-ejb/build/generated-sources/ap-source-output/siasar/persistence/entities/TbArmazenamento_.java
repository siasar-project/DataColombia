package siasar.persistence.entities;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbArmazenamento.class)
public abstract class TbArmazenamento_ {

	public static volatile SingularAttribute<TbArmazenamento, Double> sysE007001;
	public static volatile SingularAttribute<TbArmazenamento, Long> armazSeq;
	public static volatile SingularAttribute<TbArmazenamento, Double> sysE006001;
	public static volatile SingularAttribute<TbArmazenamento, Double> sysE008001;
	public static volatile SingularAttribute<TbArmazenamento, Integer> sysE009001;
	public static volatile SingularAttribute<TbArmazenamento, BigDecimal> sysE004001;
	public static volatile SingularAttribute<TbArmazenamento, Integer> sysE005001;
	public static volatile SingularAttribute<TbArmazenamento, Integer> sysE003001;
	public static volatile SingularAttribute<TbArmazenamento, String> sysE010001;
	public static volatile SingularAttribute<TbArmazenamento, Date> dateLastUpdate;
	public static volatile SingularAttribute<TbArmazenamento, TbSistema> sisteSeq;
	public static volatile SingularAttribute<TbArmazenamento, String> sysE001001;
	public static volatile SingularAttribute<TbArmazenamento, BigDecimal> sysE002001;

}

