package siasar.persistence.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbCentroSaude.class)
public abstract class TbCentroSaude_ {

	public static volatile SingularAttribute<TbCentroSaude, Integer> comD049001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD036001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD032001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD013001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD016001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD042001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD039001;
	public static volatile SingularAttribute<TbCentroSaude, TbSistema> sisteSeq;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD023001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD046001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD006001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD029001;
	public static volatile SingularAttribute<TbCentroSaude, TbComunidade> comunSeq;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD037001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD019001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD033001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD050001;
	public static volatile SingularAttribute<TbCentroSaude, String> comD012001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD047001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD009001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD026001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD043001;
	public static volatile SingularAttribute<TbCentroSaude, String> comD005001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD022001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD028001;
	public static volatile SingularAttribute<TbCentroSaude, Long> censaSeq;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD011001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD034001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD018001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD015001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD030001;
	public static volatile SingularAttribute<TbCentroSaude, Date> dateLastUpdate;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD048001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD044001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD008001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD025001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD040001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD021001;
	public static volatile SingularAttribute<TbCentroSaude, String> comD004001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD027001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD035001;
	public static volatile SingularAttribute<TbCentroSaude, String> comD010001;
	public static volatile SingularAttribute<TbCentroSaude, String> comD014001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD031001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD041001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD017001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD038001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD045001;
	public static volatile SingularAttribute<TbCentroSaude, String> comD007001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD020001;
	public static volatile SingularAttribute<TbCentroSaude, Integer> comD024001;

}

