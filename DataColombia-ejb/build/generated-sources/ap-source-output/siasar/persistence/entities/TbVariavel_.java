package siasar.persistence.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbVariavel.class)
public abstract class TbVariavel_ {

	public static volatile SingularAttribute<TbVariavel, Long> variaSeq;
	public static volatile SingularAttribute<TbVariavel, TbTipoContexto> tipoConSeq;
	public static volatile ListAttribute<TbVariavel, TbVariavelDw> tbVariavelDwList;
	public static volatile SingularAttribute<TbVariavel, String> variaConsulta;
	public static volatile ListAttribute<TbVariavel, TbVariavaria> tbVariavariaList;
	public static volatile ListAttribute<TbVariavel, TbVariavaria> tbVariavariaList1;
	public static volatile SingularAttribute<TbVariavel, TbUnit> unitSeq;
	public static volatile ListAttribute<TbVariavel, TbVariaRestricao> tbVariaRestricaoList;
	public static volatile SingularAttribute<TbVariavel, String> variaNome;
	public static volatile SingularAttribute<TbVariavel, String> variaCodigo;
	public static volatile SingularAttribute<TbVariavel, Boolean> variaAtivo;
	public static volatile SingularAttribute<TbVariavel, Integer> variaNivel;
	public static volatile SingularAttribute<TbVariavel, String> variaTipo;
	public static volatile SingularAttribute<TbVariavel, String> variaFormula;
	public static volatile SingularAttribute<TbVariavel, String> variaDescricao;

}

