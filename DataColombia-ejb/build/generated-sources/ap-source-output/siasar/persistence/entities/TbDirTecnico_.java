package siasar.persistence.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbDirTecnico.class)
public abstract class TbDirTecnico_ {

	public static volatile SingularAttribute<TbDirTecnico, Date> dateLastUpdate;
	public static volatile SingularAttribute<TbDirTecnico, TbTaxoTermo> sepB010001;
	public static volatile SingularAttribute<TbDirTecnico, TbPrestServico> prserSeq;
	public static volatile SingularAttribute<TbDirTecnico, String> sepB011001;
	public static volatile SingularAttribute<TbDirTecnico, TbTaxoTermo> sepB012001;
	public static volatile SingularAttribute<TbDirTecnico, String> sepB013001;
	public static volatile SingularAttribute<TbDirTecnico, Long> dtecSeq;

}

