package siasar.persistence.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TbLogCalculo.class)
public abstract class TbLogCalculo_ {

	public static volatile SingularAttribute<TbLogCalculo, String> logcalNomeVar;
	public static volatile SingularAttribute<TbLogCalculo, String> logcalErroResult;
	public static volatile SingularAttribute<TbLogCalculo, Boolean> logcalErroReparado;
	public static volatile SingularAttribute<TbLogCalculo, Date> logcalData;
	public static volatile SingularAttribute<TbLogCalculo, String> logcalCmdExec;
	public static volatile SingularAttribute<TbLogCalculo, Long> logcalSeq;
	public static volatile SingularAttribute<TbLogCalculo, String> logcalErroValor;

}

